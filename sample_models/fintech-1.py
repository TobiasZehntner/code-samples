# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

import itertools

from odoo import _, api, exceptions, fields, models

from ..tools import tools

########################################################################################################################
#                                                                                                                      #
# Markup Model Structure                                                                                               #
#                                                                                                                      #
# Sender      > Set of Markups > Currency pairs      > Markup values                                                   #
# res.partner   hom.markup.set   hom.markup.set.line   hom.markup                                                      #
#                                                                                                                      #
# Corridor        > Corridor Markup (copied readonly values from Markup Set, Markup Line and Markup values)            #
# sale.order.line   hom.markup.corridor                                                                                #
#                                                                                                                      #
# hom.markup: applicable values and fields                                                                             #
# Applicable Markup for different business model types and currency differences                                        #
# Markup field names in table if applicable (equal/unequal refers to OI <> Payout Currency)                            #
#                                                                                                                      #
#        Currencies             Markup Case    | OI  | Base | Regional | HS                                            #
#        MMSP Retail                           |-----|------|----------|-----                                          #
# Case 1 Sender == OI != Payout retail_unequal | Yes | Yes  | Yes      | No                                            #
# Case 2 Sender != OI != Payout retail_unequal | Yes | Yes  | Yes      | No                                            #
# Case 3 Sender != OI == Payout retail_equal   | Yes | No   | No       | Yes                                           #
#        Sender == Payout                      | Not allowed                                                           #
#                                                                                                                      #
#        CMSP Wholesale                                                                                                #
# Case 4 OI != Payout           wholesale      | No  | Yes  | Yes      | No                                            #
#        OI == Payout                          | Not allowed                                                           #
#                                                                                                                      #
########################################################################################################################


class HomMarkupSet(models.Model):
    _name = "hom.markup.set"
    _description = "Markup Set"

    name = fields.Char(required=True)

    sender_id = fields.Many2one(
        "res.partner", "Sender", ondelete="cascade", readonly=True
    )
    sender_applied_id = fields.Many2one(
        related="sender_id",
        store=True,
        help="Technical field to allow two O2Ms pointing at same record",
    )
    line_ids = fields.One2many(
        "hom.markup.set.line", inverse_name="set_id", string="Currency Pairs"
    )
    business_model_type = fields.Selection(
        [("wholesale", "Wholesale/CMSP"), ("retail", "Retail/MMSP")],
        string="Business Model Type",
        required=True,
    )
    has_volume_based_markup = fields.Boolean(
        "Uses Volume Based Markup", compute="_compute_has_volume_based"
    )
    allow_volume_based_markup = fields.Boolean(
        related="sender_id.allow_volume_based_markup", readonly=True
    )

    is_all_routing_tags = fields.Boolean()
    routing_tag_ids = fields.Many2many(
        "hom.instrument.type.routing.tag", string="Destination Routing Tags"
    )
    routing_tag_display = fields.Char(
        "Routing Tags", compute="_compute_routing_tag_display", store=True
    )
    is_all_payer_segments = fields.Boolean()
    payer_segment_ids = fields.Many2many("hom.payer.segment", string="Payer Segments")
    payer_segment_display = fields.Char(
        "Payer Segments", compute="_compute_payer_segment_display", store=True
    )
    corridor_ids = fields.Many2many(
        "sale.order.line",
        compute="_compute_corridor_ids",
        string="Affected Corridors",
        help="Applicable Corridors with the corresponding Payer Segment and Instrument Type",
    )

    _sql_constraints = [
        (
            "name_sender_unique",
            "unique(name, sender_id)",
            "Markup Set Name must be unique",
        ),
    ]

    @api.depends(
        "payer_segment_ids",
        "is_all_payer_segments",
        "routing_tag_ids",
        "is_all_routing_tags",
    )
    def _compute_corridor_ids(self):
        """
        Compute active corridors that use the Markup Set's Payer Segment / Destination Routing tag combination
        """
        for applied_markup in self:
            if (
                not applied_markup.is_all_routing_tags
                and not applied_markup.routing_tag_ids
                and not applied_markup.is_all_payer_segments
                and not applied_markup.payer_segment_ids
            ):
                applied_markup.corridor_ids = False
                continue

            domain = [
                ("sender_id", "=", applied_markup.sender_applied_id.id),
                ("dyn_state_id.is_terminal", "=", False),
                ("has_at_least_one_path_fx", "=", True),
            ]
            if not applied_markup.is_all_payer_segments:
                domain.append(
                    ("payer_segment_id", "in", applied_markup.payer_segment_ids.ids)
                )
            if not applied_markup.is_all_routing_tags:
                instrument_types = self.env["hom.instrument.type"].search(
                    [("routing_tag_id", "in", applied_markup.routing_tag_ids.ids)]
                )
                domain.append(
                    ("destination_instrument_type_id", "in", instrument_types.ids)
                )
            applied_markup.corridor_ids = self.env["sale.order.line"].search(domain)

    @api.depends("routing_tag_ids", "is_all_routing_tags")
    def _compute_routing_tag_display(self):
        """
        Compute char field for tree-view info
        """
        for markup_set in self:
            if markup_set.is_all_routing_tags:
                markup_set.routing_tag_display = "ALL"
                markup_set.routing_tag_ids = False
            elif markup_set.routing_tag_ids:
                markup_set.routing_tag_display = ", ".join(
                    [tag.name for tag in markup_set.routing_tag_ids]
                )
            else:
                markup_set.routing_tag_display = ""

    @api.depends("payer_segment_ids", "is_all_payer_segments")
    def _compute_payer_segment_display(self):
        """
        Compute char field for tree-view info
        """
        for markup_set in self:
            if markup_set.is_all_payer_segments:
                markup_set.payer_segment_display = "ALL"
                markup_set.payer_segment_ids = False
            elif markup_set.payer_segment_ids:
                markup_set.payer_segment_display = ", ".join(
                    [p.name for p in markup_set.payer_segment_ids]
                )
            else:
                markup_set.payer_segment_display = ""

    @api.depends("line_ids.is_volume_based_markup")
    def _compute_has_volume_based(self):
        for markup_set in self:
            markup_set.has_volume_based_markup = any(
                markup_set.line_ids.mapped("is_volume_based_markup")
            )

    @api.constrains("name", "sender_id")
    def _constrains_name(self):
        MarkupSet = self.env["hom.markup.set"]
        for markup_set in self:
            if MarkupSet.search_count(
                [
                    ("id", "!=", markup_set.id),
                    ("sender_id", "=", markup_set.sender_id.id),
                    ("name", "=", markup_set.name),
                ]
            ):
                raise exceptions.ValidationError(
                    _(f"Markup Set name must be unique ({markup_set.name})")
                )

    def get_covered_combos(self):
        """
        Return combinations of Routing Tags - Payer Segment combinations that are covered by this Markup Set
        :return: List of tuples: [(routing_tag, payer_segment)]
        :rtype: list
        """
        self.ensure_one()
        routing_tags = (
            self.sender_id.available_destination_routing_tag_ids
            if self.is_all_routing_tags
            else self.routing_tag_ids
        )
        payer_segments = (
            self.sender_id.sender_payer_segment_ids
            if self.is_all_payer_segments
            else self.payer_segment_ids
        )
        return list(itertools.product(routing_tags, payer_segments))

    def get_covered_currency_pairs(self):
        """
        Return list of currency pair IDs that are covered by this Markup Set
        :return: List of tuples [(from_currency.id, to_currency.id]
        :rtype: list
        """
        self.ensure_one()
        covered_pairs = []
        for line in self.line_ids:
            covered_pairs.append((line.from_currency_id.id, line.to_currency_id.id))
        return covered_pairs

    @api.constrains(
        "payer_segment_ids",
        "is_all_payer_segments",
        "routing_tag_ids",
        "is_all_routing_tags",
    )
    def _constrains_apply_parameters(self):
        """
        Each payer segment - instrument tag combination can only occur once per Sender
        """
        if self.env.context.get("skip_constrains_apply_parameters"):
            return
        for sender in self.mapped("sender_id"):
            covered_combos = sender.get_covered_markup_combos()
            covered = set()
            duplicates = {x for x in covered_combos if x in covered or covered.add(x)}
            if duplicates:
                duplicate_strings = [
                    f"\n- {dup[0].name} - {dup[1].name}" for dup in duplicates
                ]
                raise exceptions.ValidationError(
                    f"The following combinations are covered multiple times: {''.join(duplicate_strings)}"
                )

    def button_remove_apply_parameters(self):
        """
        Move a markup set from 'applied Markup Sets' to 'available markup sets'
        by removing to which routing tags / payer segments it applies
        Do not run checks when we remove paramaters (to allow removal of faulty applied markup sets)
        """
        self.ensure_one()
        self_context = self.with_context(skip_constrains_apply_parameters=True)
        self_context.payer_segment_ids = False
        self_context.is_all_payer_segments = False
        self_context.routing_tag_ids = False
        self_context.is_all_routing_tags = False

    def add_markup_line(self, currency_pair):
        """
        Add markup line with default 0 values for currency pair to current markup set
        :param currency_pair: (from_currency_id, to_currency_id)
        :type currency_pair: tuple
        :return: Markup Set Line
        :rtype: hom.markup.set.line
        """
        self.ensure_one()
        markup_set_line = self.env["hom.markup.set.line"].create(
            {
                "set_id": self.id,
                "from_currency_id": currency_pair[0],
                "to_currency_id": currency_pair[1],
            }
        )
        # Create default markup with value 0
        self.env["hom.markup"].create({"set_line_id": markup_set_line.id})
        return markup_set_line


class HomMarkupSetLine(models.Model):
    _name = "hom.markup.set.line"
    _description = "Markup Set Line"

    name = fields.Char(compute="_compute_name", store=True)
    is_active = fields.Boolean(
        "Active",
        compute="_compute_is_active",
        help="There are active corridors with this Currency Pair",
    )
    set_id = fields.Many2one(
        "hom.markup.set", "Markup Set", required=True, ondelete="cascade", readonly=True
    )
    sender_id = fields.Many2one(related="set_id.sender_id", store=True)
    business_model_type = fields.Selection(
        related="set_id.business_model_type", readonly=True
    )

    from_currency_id = fields.Many2one("res.currency", "From Currency", required=True)
    to_currency_id = fields.Many2one("res.currency", "To Currency", required=True)

    allow_volume_based_markup = fields.Boolean(
        related="set_id.sender_id.allow_volume_based_markup", readonly=True
    )
    is_volume_based_markup = fields.Boolean("Volume Based")

    markup_type = fields.Selection(
        [("linear", "Linear"), ("tiered", "Tiered")], required=True, default="linear"
    )
    tier_currency_id = fields.Many2one("res.currency", "Tier Currency", readonly=True)

    markup_ids = fields.One2many(
        "hom.markup", inverse_name="set_line_id", string="Markups"
    )

    _sql_constraints = [
        (
            "currency_pair_unique",
            "unique(from_currency_id, to_currency_id, set_id)",
            "Markup Set Currency Pair must be unique",
        ),
    ]

    @api.onchange("is_volume_based_markup")
    def _onchange_is_volume_based_markup(self):
        """
        Volume based markup requires tiered and uses the volume currency defined on the Sender
        """
        self.ensure_one()
        if self.is_volume_based_markup:
            self.markup_type = "tiered"
            self.tier_currency_id = self.set_id.sender_id.markup_volume_currency_id

    @api.onwrite("is_volume_based_markup")
    def _onwrite_is_volume_based_markup(self):
        """
        Volume based markup uses the volume currency defined on the Sender
        """
        for line in self:
            if line.is_volume_based_markup:
                line.tier_currency_id = line.set_id.sender_id.markup_volume_currency_id

    @api.depends("from_currency_id", "to_currency_id")
    def _compute_is_active(self):
        """
        is_active: there is an active corridor using this line's currency pair
        """
        for line in self:
            is_active = False
            for corridor in line.set_id.corridor_ids:
                valid_pairs = corridor.get_required_markup_currency_pairs()
                if (line.from_currency_id.id, line.to_currency_id.id) in valid_pairs:
                    is_active = True
                    break
            line.is_active = is_active

    @api.onchange("markup_ids", "markup_type")
    def _onchange_markup_ids(self):
        """
        Only allow adding markups if there can be multiple (tiered) and if they have a unique tier
        """
        self.ensure_one()
        warning = False
        if self.markup_type == "linear" and len(self.markup_ids) > 1:
            warning = "Only one Markup is allowed for Linear Markups"
            self.markup_ids = self.markup_ids[0]
        elif self.markup_type == "tiered" and len(self.markup_ids) != len(
            set(self.markup_ids.mapped("tier"))
        ):
            warning = "Tier value is required to be unique"

        if warning:
            return {"warning": {"title": "Invalid Markup Lines", "message": warning}}

    @api.onchange("markup_type", "from_currency_id", "is_volume_based_markup")
    def _onchange_from_currency_id(self):
        """
        If mode tiered, set tier currency to from-currency
        """
        self.ensure_one()
        if (
            not self.is_volume_based_markup
            and self.markup_type == "tiered"
            and self.from_currency_id
        ):
            self.tier_currency_id = self.from_currency_id

    @api.constrains("markup_type", "markup_ids")
    def _constrains_tier(self):
        """
        Make sure there's a 0 tier if line is in tiered mode
        """
        for line in self:
            if (
                line.markup_type == "tiered"
                and line.markup_ids
                and 0 not in line.markup_ids.mapped("tier")
            ):
                raise exceptions.ValidationError(
                    f"{line.name} in Markup Set '{line.set_id.name}' requires a markup with tier 0"
                )

    @api.depends("from_currency_id", "to_currency_id")
    def _compute_name(self):
        """
        Name: 'from_currency > to_currency'
        """
        for line in self:
            if line.from_currency_id and line.to_currency_id:
                line.name = (
                    f"{line.from_currency_id.name} > " f"{line.to_currency_id.name} "
                )
            else:
                line.name = False

    @api.constrains("from_currency_id", "to_currency_id", "set_id")
    def check_unique_currency(self):
        """
        Currency pair lines have to be unique per set. Like2Like paths not allowed.
        """
        for line in self:
            if line.from_currency_id == line.to_currency_id:
                # L2L path: not allowed
                raise exceptions.ValidationError(
                    _(
                        "No Markup can be created for Like2Like paths (equal from/to currency)"
                    )
                )
            if line.set_id.line_ids.filtered(
                lambda l: l != line
                and l.from_currency_id == line.from_currency_id
                and l.to_currency_id == line.to_currency_id
            ):
                raise exceptions.ValidationError(
                    _(
                        "A line with the same currency pair already exists (%s > %s)"
                        % (line.from_currency_id.name, line.to_currency_id.name)
                    )
                )

    def unlink(self):
        if not self.env.context.get("skip_unlink_check"):
            for line in self:
                if line.is_active:
                    raise exceptions.UserError(
                        _(
                            "You cannot remove an active currency pair that is used by Corridors"
                        )
                    )
        return super(HomMarkupSetLine, self).unlink()


# Avoiding duplicate code due to a lot of similar fields with the same constraints, computes and onchanges
# Repetitive Field parameters
MARKUP_FIELD_PARAM = {
    "mode": {
        "selection": [("percentage", "Percentage"), ("share", "Share")],
        "string": "Mode",
        "default": "percentage",
        "required": True,
        "help": "* Percentage: Set a percentage for each party. The End-to-End total will be the sum of all "
        "percentages. * Share: Set the End-to-End total percentage, then set the share of each party.",
    },
    "e2e_total": {
        "string": "E2E Total",
        "help": "The end-to-end total markup percentage of the amount",
    },
    "share_unassigned": {
        "string": "Left to Assign",
        "compute": "_compute_share_unassigned",
        "help": "The share of all applicable markup shares is required to be 100%",
    },
    "oi": {"string": "OI", "help": "Sender Customer Markup. Percentage of amount."},
    "oi_share": {
        "string": "OI Share",
        "help": "Sender Customer Markup share. Percentage of E2E total.",
    },
    "oi_display": {"string": "OI", "compute": "_compute_display"},
    "base": {
        "string": "Base",
        "help": "Base Markup covers HomeSend benefits. Percentage of amount.",
    },
    "base_share": {
        "string": "Base Share",
        "help": "Base Markup share covers HomeSend benefits. Percentage of E2E total.",
    },
    "base_display": {"string": "Base", "compute": "_compute_display"},
    "regional": {
        "string": "Regional",
        "help": "HomeSend Regional Markup covers MC region benefits. Percentage of amount.",
    },
    "regional_share": {
        "string": "Regional Share",
        "help": "HomeSend Regional Markup share covers MC region benefits. Percentage of E2E total.",
    },
    "regional_display": {"string": "Regional", "compute": "_compute_display"},
    "hs": {
        "string": "HS Share",
        "help": "HS Markup collected by HomeSend. Percentage of amount.",
    },
    "hs_share": {
        "string": "HS",
        "help": "HS Markup share collected by HomeSend. Percentage of E2E total.",
    },
    "hs_display": {"string": "HS", "compute": "_compute_display"},
}
MARKUP_FIELD_MAP = {
    "retail_equal": ["oi", "hs"],
    "retail_unequal": ["oi", "base", "regional"],
    "wholesale": ["base", "regional"],
}
ROUNDING_DIGITS = 10


# Functions to get specific fields
def get_counterpart_field(field_name):
    """
    Get counterpart field: for normal percentage field, return share-percentage field and vice versa
    """
    return (
        field_name.replace("_share", "")
        if "_share" in field_name
        else f"{field_name}_share"
    )


def get_specific_fields(specific_fields):
    """
    Get fields for all markup cases (retail equal, retail unequal, wholesale) for a list of fields
    :param specific_fields: list of field names
    :type specific_fields: list
    :return: list of field names
    :rtype: list
    """
    field_list = []
    for markup_case in MARKUP_FIELD_MAP.keys():
        for specific_field in specific_fields:
            field_list.append(f"{markup_case}_{specific_field}")
    return field_list


def get_all_markup_fields(include_totals=False):
    """
    Get all fields
    :param include_totals: Also include computed total fields
    :type include_totals: bool
    :return: List of field names
    :rtype: list
    """
    field_list = []
    for markup_case in MARKUP_FIELD_MAP.keys():
        field_list += get_markup_fields(markup_case, "both", include_totals)
    return field_list


def get_markup_fields(markup_case, markup_mode, include_totals=False):
    """
    Get list of markup fields
    field_name can be: oi, base, regional, hs
    Markup case can be: retail_equal, retail_unequal, wholesale

    For each field there is
    - {markup_case}_{field_name}
    - {markup_case}_{field_name}_share

    For each case there is also:
    - {markup_case}_e2e_total

    :param markup_case: retail_unequal, retail_equal, wholesale
    :type markup_case: str
    :param markup_type: percentage, share, both
    :type markup_type: str
    :param include_totals: Add total fields (e2e_total)
    :type include_totals: bool
    :return: list of technical field names
    :rtype: list
    """
    field_list = []
    markup_fields = MARKUP_FIELD_MAP[markup_case]
    if markup_mode in ["both", "percentage"]:
        field_list += [f"{markup_case}_{ms}" for ms in markup_fields]
    if markup_mode in ["both", "share"]:
        field_list += [f"{markup_case}_{ms}_share" for ms in markup_fields]
    if include_totals:
        field_list.append(f"{markup_case}_e2e_total")
    return field_list


class HomMarkup(models.Model):
    _name = "hom.markup"
    _description = "Markup"
    _order = "tier"

    name = fields.Char(compute="_compute_name")
    set_line_id = fields.Many2one(
        "hom.markup.set.line", required=True, ondelete="cascade"
    )
    sender_id = fields.Many2one(related="set_line_id.set_id.sender_id")
    business_model_type = fields.Selection(
        related="set_line_id.set_id.business_model_type", readonly=True
    )
    from_currency_id = fields.Many2one(
        related="set_line_id.from_currency_id", readonly=True
    )
    to_currency_id = fields.Many2one(
        related="set_line_id.to_currency_id", readonly=True
    )
    markup_type = fields.Selection(
        [("linear", "Linear"), ("tiered", "Tiered")], readonly=True
    )

    tier = fields.Integer("Tier From")
    tier_currency_id = fields.Many2one(
        related="set_line_id.tier_currency_id", readonly=True
    )
    is_volume_based_markup = fields.Boolean(
        related="set_line_id.is_volume_based_markup", readonly=True
    )
    is_multi_corridor = fields.Boolean(
        "Used by Multiple Corridors", compute="_compute_is_multi_corridor"
    )

    # Markup fields
    # Retail equal: OI == Payout Currency: OI and HS
    retail_equal_mode = fields.Selection(**MARKUP_FIELD_PARAM["mode"])
    retail_equal_e2e_total = fields.Float(**MARKUP_FIELD_PARAM["e2e_total"])
    retail_equal_share_unassigned = fields.Float(
        **MARKUP_FIELD_PARAM["share_unassigned"]
    )
    retail_equal_oi = fields.Float(**MARKUP_FIELD_PARAM["oi"])
    retail_equal_oi_share = fields.Float(**MARKUP_FIELD_PARAM["oi_share"])
    retail_equal_oi_display = fields.Float(**MARKUP_FIELD_PARAM["oi_display"])
    retail_equal_hs = fields.Float(**MARKUP_FIELD_PARAM["hs"])
    retail_equal_hs_share = fields.Float(**MARKUP_FIELD_PARAM["hs_share"])
    retail_equal_hs_display = fields.Float(**MARKUP_FIELD_PARAM["hs_display"])

    # Retail unequal: OI <> Payout Currency: OI, Base, HS
    retail_unequal_mode = fields.Selection(**MARKUP_FIELD_PARAM["mode"])
    retail_unequal_e2e_total = fields.Float(**MARKUP_FIELD_PARAM["e2e_total"])
    retail_unequal_share_unassigned = fields.Float(
        **MARKUP_FIELD_PARAM["share_unassigned"]
    )
    retail_unequal_oi = fields.Float(**MARKUP_FIELD_PARAM["oi"])
    retail_unequal_oi_share = fields.Float(**MARKUP_FIELD_PARAM["oi_share"])
    retail_unequal_oi_display = fields.Float(**MARKUP_FIELD_PARAM["oi_display"])
    retail_unequal_base = fields.Float(**MARKUP_FIELD_PARAM["base"])
    retail_unequal_base_share = fields.Float(**MARKUP_FIELD_PARAM["base_share"])
    retail_unequal_base_display = fields.Float(**MARKUP_FIELD_PARAM["base_display"])
    retail_unequal_regional = fields.Float(**MARKUP_FIELD_PARAM["regional"])
    retail_unequal_regional_share = fields.Float(**MARKUP_FIELD_PARAM["regional_share"])
    retail_unequal_regional_display = fields.Float(
        **MARKUP_FIELD_PARAM["regional_display"]
    )

    # Wholesale: Base, Regional
    wholesale_mode = fields.Selection(**MARKUP_FIELD_PARAM["mode"])
    wholesale_e2e_total = fields.Float(**MARKUP_FIELD_PARAM["e2e_total"])
    wholesale_share_unassigned = fields.Float(**MARKUP_FIELD_PARAM["share_unassigned"])
    wholesale_base = fields.Float(**MARKUP_FIELD_PARAM["base"])
    wholesale_base_share = fields.Float(**MARKUP_FIELD_PARAM["base_share"])
    wholesale_base_display = fields.Float(**MARKUP_FIELD_PARAM["base_display"])
    wholesale_regional = fields.Float(**MARKUP_FIELD_PARAM["regional"])
    wholesale_regional_share = fields.Float(**MARKUP_FIELD_PARAM["regional_share"])
    wholesale_regional_display = fields.Float(**MARKUP_FIELD_PARAM["regional_display"])

    def get_copy_to_corridor_values(self, fx_path):
        """
        Return values to copy a markup to a corridor markup
        :param fx_path: Settlement/MTS Path
        :type fx_path: hom.settlement.path OR hom.mts.path
        :return: {field_name: value}
        :rtype: dict
        """
        self.ensure_one()
        markup_line = self.set_line_id
        markup_case = fx_path.corridor_id.get_markup_case(fx_path)
        path_field = (
            "settlement_path_id"
            if fx_path._name == "hom.settlement.path"
            else "mts_path_id"
        )
        values = {
            "from_currency_id": markup_line.from_currency_id.id,
            "to_currency_id": markup_line.to_currency_id.id,
            "markup_type": markup_line.markup_type,
            "is_volume_based_markup": markup_line.is_volume_based_markup,
            "tier": self.tier,
            "tier_currency_id": markup_line.tier_currency_id.id,
            "mode": self[f"{markup_case}_mode"],
            "e2e_total": self[f"{markup_case}_e2e_total"],
            path_field: fx_path.id,
        }
        for ms in MARKUP_FIELD_MAP[markup_case]:
            values[f"{ms}_display"] = self[f"{markup_case}_{ms}_display"]

        return values

    def copy_to_corridor(self, fx_path):
        """
        Create a corridor markup from a markup and a specific path
        :param fx_path: FX settlement path
        :type fx_path: hom.settlement.path OR hom.mts.path
        """
        self.ensure_one()
        vals = self.get_copy_to_corridor_values(fx_path)
        vals.update(
            {
                "corridor_id": fx_path.corridor_id.id,
                "set_line_id": self.set_line_id.id,
                "markup_id": self.id,
            }
        )
        self.env["hom.markup.corridor"].create(vals)

    def _compute_is_multi_corridor(self):
        """
        is_multi_corridor: there is more than one active corridor using these markup values
        Used to make values readonly if opened from a corridor, so user manages markup from Sender
        """
        for markup in self:
            active_corridors = (
                markup.set_line_id.set_id.sender_id.get_active_corridors()
            )
            corridor_markups = active_corridors.mapped("corridor_markup_ids")
            markup.is_multi_corridor = (
                len(corridor_markups.filtered(lambda cm: cm.markup_id == markup)) > 1
            )

    def get_share_unassigned(self, markup_case):
        """
        Get percentage that hasn't been assigned yet (100 - assigned percentages)
        :param markup_case: retail_equal, retail_unequal, wholesale
        :type markup_case: str
        :return: Unassigned percentage
        :rtype: float
        """
        self.ensure_one()
        if (
            self[f"{markup_case}_e2e_total"] == 0
            or self[f"{markup_case}_mode"] == "percentage"
        ):
            # If E2E total is 0 or mode is percentage, no share needs to be assigned
            return 0
        share_fields = get_markup_fields(markup_case, "share")
        total = sum([self[share_field] for share_field in share_fields])
        return round(100 - total, ROUNDING_DIGITS)

    def set_zero(self, markup_fields):
        """
        Set non-applicable fields to zero
        :param markup_fields: list of field names
        :type markup_fields: list
        """
        self.ensure_one()
        for field in markup_fields:
            if self[field] != 0:
                self[field] = 0

    def compute_counterpart_markup(self, markup_case):
        """
        Depending on markup mode percentage/share, we compute the counterpart field
        so it stays in sync
        """
        self.ensure_one()
        if self.env.context.get("skip_check"):
            return
        self_write = self.with_context(skip_check=True)
        e2e_total_field = f"{markup_case}_e2e_total"
        percentage_fields = get_markup_fields(markup_case, "percentage")
        if self[f"{markup_case}_mode"] == "percentage":
            # Recompute total percentage before new individual values
            e2e_total_value = sum(
                [self[percentage_field] for percentage_field in percentage_fields]
            )
            rounded_e2e_total_value = round(e2e_total_value, ROUNDING_DIGITS)
            if self[e2e_total_field] != rounded_e2e_total_value:
                self_write[e2e_total_field] = rounded_e2e_total_value
            for percentage_field in percentage_fields:
                # Counterpart fields
                share_value = (
                    100 / self[e2e_total_field] * self[percentage_field]
                    if not self[e2e_total_field] == 0
                    else 0
                )
                rounded_share_value = round(share_value, ROUNDING_DIGITS)
                counterpart_field = get_counterpart_field(percentage_field)
                if self[counterpart_field] != rounded_share_value:
                    self_write[counterpart_field] = rounded_share_value
        else:
            for percentage_field in percentage_fields:
                share_counterpart_field = get_counterpart_field(percentage_field)
                percentage_value = (
                    self[e2e_total_field] / 100 * self[share_counterpart_field]
                    if not self[share_counterpart_field] == 0
                    else 0
                )
                rounded_percentage_value = round(percentage_value, ROUNDING_DIGITS)
                if self[percentage_field] != rounded_percentage_value:
                    self_write[percentage_field] = rounded_percentage_value

    def set_display_fields(self, markup_case):
        """
        Display correct field value in tree-view depending on type (percentage/share)
        """
        self.ensure_one()
        for ms in MARKUP_FIELD_MAP[markup_case]:
            applicable_field = f"{markup_case}_{ms}{'_share' if self[f'{markup_case}_mode'] == 'share' else ''}"
            if self[f"{markup_case}_{ms}_display"] != self[applicable_field]:
                self[f"{markup_case}_{ms}_display"] = self[applicable_field]

    @api.depends("set_line_id.markup_type")
    def _compute_name(self):
        """
        Name of markup is either "Linear" or its Tier value
        """
        for markup in self:
            if markup.set_line_id.markup_type == "linear":
                markup.name = "Linear Markup"
            else:
                markup.name = markup.tier

    @api.depends(*(get_all_markup_fields(include_totals=True)))
    def _compute_share_unassigned(self):
        for markup in self:
            for markup_case in MARKUP_FIELD_MAP.keys():
                share_unassigned = markup.get_share_unassigned(markup_case)
                if markup[f"{markup_case}_share_unassigned"] != share_unassigned:
                    markup[f"{markup_case}_share_unassigned"] = share_unassigned

    @api.onchange(*(get_specific_fields(["mode", "e2e_total"])))
    def _onchange_e2e_total(self):
        """
        Set share values to 0 if e2e total is 0
        """
        self.ensure_one()
        for markup_case in MARKUP_FIELD_MAP.keys():
            if (
                self[f"{markup_case}_mode"] == "share"
                and self[f"{markup_case}_e2e_total"] == 0
            ):
                self.set_zero(get_markup_fields(markup_case, "share"))

    @api.onchange(*(get_all_markup_fields(include_totals=True)))
    def _onchange_markup(self):
        """
        Depending on percentage/share mode, we adjust the counterpart and total
        Percentage check is done before re-computing new values.
        """
        self.ensure_one()
        for markup_case in MARKUP_FIELD_MAP.keys():
            self.compute_counterpart_markup(markup_case)

    @api.constrains(*(get_all_markup_fields(include_totals=True)))
    def _constrains_percentage_fields(self):
        for markup in self:
            for markup_case in MARKUP_FIELD_MAP.keys():
                unassigned = markup[f"{markup_case}_share_unassigned"]
                if round(unassigned, ROUNDING_DIGITS) != 0:
                    raise exceptions.ValidationError(
                        f"The Sum of Share Percentages is required to be 100 (assign remaining {unassigned})"
                    )
                e2e_total = markup[f"{markup_case}_e2e_total"]
                if not e2e_total <= 100:
                    raise exceptions.ValidationError(
                        f"The E2E total cannot be above 100% ({e2e_total})"
                    )
                if e2e_total == 0:
                    for markup_field in get_markup_fields(
                        markup_case, markup[f"{markup_case}_mode"]
                    ):
                        if markup[markup_field] != 0:
                            raise exceptions.ValidationError(
                                f"The E2E Total cannot be 0 if the markup values are not 0 "
                                f"({markup.set_line_id.set_id.name} - {markup.set_line_id.name} - {markup.name})"
                            )

    @api.oncreatewrite(*(get_all_markup_fields(include_totals=True)))
    def recompute_counterpart(self):
        for markup in self:
            for markup_case in MARKUP_FIELD_MAP.keys():
                markup.compute_counterpart_markup(markup_case)

    @api.multi
    @api.depends(*(get_all_markup_fields()))
    def _compute_display(self):
        for markup in self:
            for markup_case in MARKUP_FIELD_MAP.keys():
                markup.set_display_fields(markup_case)


class HomMarkupCorridor(models.Model):
    _name = "hom.markup.corridor"
    _description = "Corridor Markup"
    _order = "from_currency_id, to_currency_id"

    name = fields.Char(compute="_compute_name")

    # Relational fields to corridor, currency pair, markup: all required. Corridor Markup should not exist without
    corridor_id = fields.Many2one(
        "sale.order.line", "Corridor", required=True, ondelete="cascade"
    )
    set_line_id = fields.Many2one(
        "hom.markup.set.line", "Currency Pair Origin", required=True, ondelete="cascade"
    )
    markup_id = fields.Many2one(
        "hom.markup", "Markup Origin", required=True, ondelete="cascade"
    )

    set_id = fields.Many2one(
        related="set_line_id.set_id", readonly=True, string="Markup Set"
    )
    business_model_type = fields.Selection(
        related="corridor_id.business_model_type", readonly=True
    )

    from_currency_id = fields.Many2one("res.currency", "From")
    to_currency_id = fields.Many2one("res.currency", "To")
    is_volume_based_markup = fields.Boolean("Volume Based")
    markup_type = fields.Selection([("linear", "Linear"), ("tiered", "Tiered")])
    tier = fields.Monetary("Tier From", currency_field="tier_currency_id")
    mode = fields.Selection([("percentage", "Percentage"), ("share", "Share")], "Mode")
    tier_currency_id = fields.Many2one("res.currency", "Tier Currency")

    oi_display = fields.Float("OI")
    base_display = fields.Float("Base")
    regional_display = fields.Float("Regional")
    hs_display = fields.Float("HS")

    e2e_total = fields.Float("E2E Total")

    settlement_path_id = fields.Many2one(
        "hom.settlement.path",
        readonly=True,
        help="The path that requires this Markup",
    )
    mts_path_id = fields.Many2one(
        "hom.mts.path", readonly=True, help="The path that requires this Markup"
    )

    @api.depends("from_currency_id", "to_currency_id")
    def _compute_name(self):
        """
        Name: 'from_currency > to_currency'
        """
        for corridor_markup in self:
            if corridor_markup.from_currency_id and corridor_markup.to_currency_id:
                corridor_markup.name = (
                    f"{corridor_markup.from_currency_id.name} > "
                    f"{corridor_markup.to_currency_id.name} "
                )
            else:
                corridor_markup.name = False

    def get_path(self):
        """
        A corridor markup is linked to a path. If legacy or MTS depends on corrior OI Settlement Mode.
        If Markup doesn't have a path, the corridor has been migrated: find correct path through origin
        """
        self.ensure_one()
        if self.corridor_id.oi_settlement_mode == "legacy":
            return self.settlement_path_id or self.env["hom.settlement.path"].search(
                [("origin_mts_path_id", "=", self.mts_path_id.id)], limit=1
            )
        else:
            return self.mts_path_id or self.env["hom.mts.path"].search(
                [("origin_settlement_path_id", "=", self.settlement_path_id.id)],
                limit=1,
            )

    def open_markup_from_corridor(self):
        """
        Open the Markup value directly from the Corridor Markup. Readonly if other corridors use same markup
        """
        self.ensure_one()
        if self.markup_id.is_multi_corridor:
            view = self.env.ref("hom_customer.hom_markup_form_from_corridor_readonly")
        else:
            view = self.env.ref("hom_customer.hom_markup_form")
        return {
            "name": f"{self.markup_id.set_line_id.set_id.name}: {self.markup_id.set_line_id.name}",
            "view_mode": "form",
            "res_model": "hom.markup",
            "view_id": view.id,
            "type": "ir.actions.act_window",
            "res_id": self.markup_id.id,
            "target": "new",
            "context": {
                "markup_case": self.corridor_id.get_markup_case(self.get_path())
            },
            "flags": {"initial_mode": "readonly"},
        }

    def update_markup_values(self):
        """
        Sync data with origin
        """
        for corridor_markup in self:
            path = corridor_markup.get_path()
            if not path:
                # Path has been deleted. Remove markup.
                corridor_markup.unlink()
                continue
            values = corridor_markup.markup_id.get_copy_to_corridor_values(path)
            for field, value in values.items():
                if tools.get_field_read_value(field, corridor_markup) != value:
                    corridor_markup[field] = value

    def is_outdated(self):
        """
        Check if data is outdated
        """
        self.ensure_one()
        path = self.get_path()
        if not path:
            return True
        values = self.markup_id.get_copy_to_corridor_values(path)
        for field, value in values.items():
            if tools.get_field_read_value(field, self) != value:
                return True
        return False
