# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models
from odoo.addons.tz_canonical.constants import CANONICAL_TZ

from ..tools import tools  # pylint: disable=W7935
from .hom_fdd_delivery import FDD_DELIVERY_COMMON_FIELDS

###############################################################################
#                        FDD Model Structure                                  #
#                                                                             #
#     Receiver                    ->             Product                      #
#    res.partner                             product.template                 #
#        |                                          |                         #
# Receiver FDD Policies           ->        Product FDD Policy                #
# hom.fdd.policy.receiver                   hom.fdd.policy.product            #
#        |                                          |                         #
#  Delivery Models                ->         Delivery Models                  #
# hom.fdd.delivery.receiver                hom.fdd.delivery.product           #
#        |             |                            |          |              #
#      Days          Lists                      Days         Lists            #
# hom.holiday.day   hom.holiday.list     hom.holiday.day   hom.holiday.list   #
#                                                                             #
###############################################################################

FDD_POLICY_RECEIVER_MAIN_FIELDS = [
    "fdd_type",
    "active",
    "sequence",
    "is_all_destination_countries",
    "is_all_destination_instruments",
    "is_all_payout_currencies",
    "is_all_payment_types",
    "destination_country_ids",
    "destination_instrument_ids",
    "payout_currency_ids",
    "payment_type_ids",
]
FDD_POLICY_COMMON_FIELDS = ["name", "fdd_type", "time_zero", "timezone"]


class HomFddPolicy(models.AbstractModel):
    _name = "hom.fdd.policy"
    _description = "Funds Delivery Duration Policy Abstract"

    name = fields.Char(required=True)
    fdd_type = fields.Selection(
        [("na", "Not Applicable"), ("rsp", "Provided by RSP"), ("active", "Active")],
        "Type",
        default="na",
        required=True,
    )
    time_zero = fields.Selection(
        [
            ("acknowledge_request", "Acknowledges HoS Request"),
            ("confirm_transaction", "Confirms Transaction"),
            ("confirm_paid_next", "Confirms Funds Paid to Next Actor"),
            ("confirm_paid_beneficiary", "Confirms Funds Paid to Beneficiary"),
        ],
        "T0",
        help="* Acknowledges HoS Request: RSP acknowledges Homesend request without "
        "providing further details on delivery confirmation or delivery duration. "
        "The funds are assumed delivered if not reject or refund message received "
        "during the configured delivery period."
        "* RSP Confirms Transaction: RSP confirms Homesend transaction (after "
        "previous acknowledge message or not) without further delivery "
        "confirmation or delivery duration."
        "* RSP Confirms Funds Paid to Next Actor: RSP confirms funds paid to the "
        "next actor (ACH, bank, etc) in the transaction processing chain."
        "* RSP Confirms Funds Paid to Beneficiary: RSP confirms funds have been "
        "paid to beneficiary account.",
    )
    timezone = fields.Selection(
        CANONICAL_TZ,
        string="Timezone",
        default=lambda self: self._context.get("tz", self.env.user.tz or "Etc/UTC"),
    )
    is_timezone_required = fields.Boolean(
        "Timezone Required",
        compute="_compute_is_timezone_required",
        help="Helper field for view",
    )

    delivery_info = fields.Char("Delivery Models", compute="_compute_delivery_info")
    delivery_warning = fields.Html(
        "Delivery Status Warning", compute="_compute_delivery_info"
    )

    @api.model
    def _get_delivery_info(self, delivery_ids):
        """
        Concatenate delivery model info into a string (displayed on policy tree)
        """
        delivery_types = dict(
            self.env["hom.fdd.delivery"]._fields["delivery_type"].selection
        )
        delivery_names = []
        for delivery in delivery_ids:
            if delivery.mode == "non_stop":
                duration = f"Non-Stop ({delivery.duration_min}min)"
            elif delivery.mode == "t_days":
                duration = f"T+{delivery.duration_day}D"
            else:
                duration = (
                    f"Opening-Closing "
                    f"({delivery.duration_hour}h{delivery.duration_min:02d})"
                )

            delivery_names.append(
                f"{delivery_types.get(delivery.delivery_type)}/{duration}"
            )
        delivery_info = "; ".join(delivery_names)
        return delivery_info

    def get_delivery_warning(self, delivery_ids):
        self.ensure_one()
        if self.fdd_type == "active":
            delivery_types = delivery_ids.mapped("delivery_type")
            if not delivery_ids:
                return _("Missing delivery model")
            if self.has_bank_instrument:
                # Required: one 3rd party, allowed: one extra direct
                if "3rd_party" not in delivery_types:
                    return _("3rd Party delivery model required")
                elif len(delivery_ids) > 2 or (
                    len(delivery_ids) == 2
                    and (
                        "3rd_party" not in delivery_types
                        or "direct" not in delivery_types
                    )
                ):
                    return _(
                        "Only 2 delivery models allowed: one 3rd Party (required) and one Direct (optional)"
                    )
            else:
                # Required: one direct delivery model
                if "3rd_party" in delivery_types or len(delivery_ids) > 1:
                    return _("Only one Direct delivery model is allowed")
        return ""

    def assign_delivery_info(self, deliveries):
        """
        Shared compute function that works for both Receiver and Product policies
        """
        self.ensure_one()
        warnings = self.get_delivery_warning(deliveries)
        self.delivery_warning = warnings

        if not self.fdd_type == "active":
            self.delivery_info = ""
        elif not deliveries:
            self.delivery_info = "Missing"
        elif warnings:
            self.delivery_info = "Error"
        else:
            self.delivery_info = self._get_delivery_info(deliveries)


class HomFddPolicyReceiver(models.Model):
    _name = "hom.fdd.policy.receiver"
    _description = "Funds Delivery Duration Policy Receiver"
    _inherit = "hom.fdd.policy"
    _order = "is_default, sequence, id DESC"

    # Main fields
    receiver_id = fields.Many2one("res.partner", "Receiver", required=True)
    sequence = fields.Integer(default=50)
    is_default = fields.Boolean(
        "Default",
        readonly=True,
        help="Default FDD Policy is added on RSP creation. The main parameters cannot "
        "be edited nor can it be removed.",
    )
    active = fields.Boolean(default=True)

    # Relations
    child_product_policy_ids = fields.One2many(
        "hom.fdd.policy.product",
        inverse_name="parent_receiver_policy_id",
        string="Product Policy Children",
    )
    product_ids = fields.Many2many(
        "product.template",
        string="Products",
        compute="_compute_product_ids",
        help="Products that use this Policy",
    )
    receiver_delivery_ids = fields.One2many(
        "hom.fdd.delivery.receiver",
        inverse_name="receiver_policy_id",
        string="Delivery Models",
    )

    # Receiver Policy specific
    is_all_destination_countries = fields.Boolean(default=True)
    is_all_destination_instruments = fields.Boolean(default=True)
    is_all_payout_currencies = fields.Boolean(default=True)
    is_all_payment_types = fields.Boolean(default=True)

    destination_country_ids = fields.Many2many(
        "res.country", string="Destination Countries"
    )
    destination_instrument_ids = fields.Many2many(
        "hom.instrument.type",
        string="Instrument Types",
        domain=[("is_terminating_type", "=", True)],
    )
    payout_currency_ids = fields.Many2many("res.currency", string="Payout Currencies")
    payment_type_ids = fields.Many2many("hom.payment.type", string="Payment Types")

    is_holiday_outdated = fields.Boolean(
        "Outdated Holiday Lists", compute="_compute_is_holiday_outdated"
    )

    country_domain_ids = fields.Many2many(
        "res.country",
        relation="fdd_policy_receiver_domain_country_rel",
        compute="_compute_country_domain_ids",
    )
    has_bank_instrument = fields.Boolean(
        compute="_compute_has_bank_instrument", help="Helper field for Delivery Policy"
    )
    default_delivery_type = fields.Selection(
        [("direct", "Direct"), ("3rd_party", "3rd Party")],
        compute="_compute_has_bank_instrument",
        help="Helper field for Delivery Policy",
    )

    @api.depends("receiver_delivery_ids", "receiver_delivery_ids.mode")
    def _compute_is_timezone_required(self):
        for receiver_policy in self:
            delivery_modes = receiver_policy.receiver_delivery_ids.mapped("mode")
            receiver_policy.is_timezone_required = any(
                [m in ["t_days", "open_close"] for m in delivery_modes]
            )

    @api.depends("is_all_destination_instruments", "destination_instrument_ids")
    def _compute_has_bank_instrument(self):
        """
        has_bank_instrument is true if any of the instruments are banks or
        if 'all instruments' is selected
        """
        bank_instruments = (
            self.env.ref("hom_customer.hom_instrument_type_bank_account")
            + self.env.ref("hom_customer.hom_instrument_type_bank_account_own")
            + self.env.ref("hom_customer.hom_instrument_type_bank_account_other")
        )
        for receiver_policy in self:
            has_bank_instrument = receiver_policy.is_all_destination_instruments or any(
                [
                    inst in bank_instruments
                    for inst in receiver_policy.destination_instrument_ids
                ]
            )
            receiver_policy.has_bank_instrument = has_bank_instrument
            receiver_policy.default_delivery_type = (
                "3rd_party" if has_bank_instrument else "direct"
            )

    @api.depends("is_all_destination_countries", "destination_country_ids")
    def _compute_country_domain_ids(self):
        """
        To be used on delivery (has to be computed on policy so it can be handed to delivery at creation, since
        NewID records otherwise don't get the correct countries)
        """
        for receiver_policy in self:
            if receiver_policy.is_all_destination_countries:
                receiver_policy.country_domain_ids = self.env["res.country"].search([])
            else:
                receiver_policy.country_domain_ids = (
                    receiver_policy.destination_country_ids
                )

    def _compute_is_holiday_outdated(self):
        for policy in self:
            policy.is_holiday_outdated = any(
                policy.receiver_delivery_ids.mapped("is_holiday_outdated")
            )

    @api.depends("receiver_delivery_ids")
    def _compute_delivery_info(self):
        for receiver_policy in self:
            receiver_policy.assign_delivery_info(receiver_policy.receiver_delivery_ids)

    def name_get(self):
        result = []
        for receiver_policy in self:
            name = f"{receiver_policy.name} ({receiver_policy.receiver_id.name})"
            result.append((receiver_policy.id, name))
        return result

    def unlink(self):
        if not self.env.context.get("skip_unlink_check"):
            for policy in self:
                if policy.is_default:
                    raise exceptions.UserError(
                        _("You cannot remove the default FDD Policy")
                    )
        return super(HomFddPolicyReceiver, self).unlink()

    @api.multi
    @api.constrains("name")
    def _constrains_name(self):
        for policy in self:
            if self.search(
                [
                    ("id", "!=", policy.id),
                    ("receiver_id", "=", policy.receiver_id.id),
                    ("name", "=", policy.name),
                ]
            ):
                raise exceptions.ValidationError(
                    _(
                        f"A Policy with the name '{policy.name}' already exists for "
                        f"this Receiver"
                    )
                )

    @api.constrains("active")
    def _constrains_active(self):
        for policy in self:
            if policy.is_default and not policy.active:
                raise exceptions.ValidationError(
                    _("You cannot archive the default FDD Policy")
                )

    @api.onwrite("sequence")
    def cap_sequence(self):
        """
        Do not allow a sequence higher than default policy: default should always be
        last
        """
        for policy in self:
            if policy.sequence >= 999:
                policy.sequence = 998

    @api.constrains("receiver_id", "is_default")
    def _constrains_is_default(self):
        """
        Only one default policy is allowed
        """
        for policy in self:
            if policy.is_default and self.search(
                [
                    ("id", "!=", policy.id),
                    ("receiver_id", "=", policy.receiver_id.id),
                    ("is_default", "=", True),
                ]
            ):
                raise exceptions.UserError(
                    _("A default policy already exists for this Receiver")
                )

    @api.onchange("destination_instrument_ids")
    def _onchange_destination_instrument_ids(self):
        """
        If instruments don't fit the delivery models return warning to user. If they
        don't change them we raise an error in the constrains
        """
        self.ensure_one()
        if not self.has_bank_instrument and (
            len(self.receiver_delivery_ids) > 1
            or "3rd_party" in self.receiver_delivery_ids.mapped("delivery_type")
        ):
            return {
                "warning": {
                    "title": "Invalid Delivery Models",
                    "message": "Please make sure your delivery models correspond to "
                    "the selected instrument types (3rd Party and/or Direct "
                    "for Banks, one direct for other instruments)",
                }
            }

    @api.multi
    def _compute_product_ids(self):
        Product = self.env["product.template"]
        for policy in self:
            policy.product_ids = Product.search(
                [
                    ("dyn_state_id.is_terminal", "=", False),
                    ("fdd_policy_id.parent_receiver_policy_id", "=", policy.id),
                    ("fdd_policy_id.is_standard", "=", True),
                ]
            )

    @api.onchange("is_all_destination_countries")
    def _onchange_is_all_destination_countries(self):
        self.ensure_one()
        if self.is_all_destination_countries:
            self.destination_country_ids = False

    @api.constrains("is_all_destination_countries", "destination_country_ids")
    def _constrains_destination_country_ids(self):
        for policy in self:
            if (
                not policy.is_all_destination_countries
                and not policy.destination_country_ids
            ):
                raise exceptions.UserError(_("Please select at least one country"))

    @api.onchange("is_all_destination_instruments")
    def _onchange_is_all_destination_instruments(self):
        self.ensure_one()
        if self.is_all_destination_instruments:
            self.destination_instrument_ids = False

    @api.constrains("is_all_destination_instruments", "destination_instrument_ids")
    def _constrains_destination_instrument_ids(self):
        for policy in self:
            if (
                not policy.is_all_destination_instruments
                and not policy.destination_instrument_ids
            ):
                raise exceptions.UserError(_("Please select at least one instrument"))

    @api.onchange("is_all_payout_currencies")
    def _onchange_is_all_payout_currencies(self):
        self.ensure_one()
        if self.is_all_payout_currencies:
            self.payout_currency_ids = False

    @api.constrains("is_all_payout_currencies", "payout_currency_ids")
    def _constrains_payout_currency_ids(self):
        for policy in self:
            if not policy.is_all_payout_currencies and not policy.payout_currency_ids:
                raise exceptions.UserError(_("Please select at least one currency"))

    @api.onchange("is_all_payment_types")
    def _onchange_is_all_payment_types(self):
        self.ensure_one()
        if self.is_all_payment_types:
            self.payment_type_ids = False

    @api.constrains("is_all_payment_types", "payment_type_ids")
    def _constrains_payment_type_ids(self):
        for policy in self:
            if not policy.is_all_payment_types and not policy.payment_type_ids:
                raise exceptions.UserError(_("Please select at least one payment type"))


class HomFddPolicyProduct(models.Model):
    _name = "hom.fdd.policy.product"
    _description = "Funds Delivery Duration Policy Product"
    _inherit = "hom.fdd.policy"

    is_standard = fields.Boolean(readonly=True)
    country_id = fields.Many2one(
        "res.country", related="product_id.country_id", readonly=True
    )

    # Relational fields
    parent_receiver_policy_id = fields.Many2one(
        "hom.fdd.policy.receiver", "Receiver Parent Policy", readonly=True
    )
    product_id = fields.Many2one(
        "product.template", "Product", required=True, ondelete="cascade"
    )
    product_delivery_ids = fields.One2many(
        "hom.fdd.delivery.product",
        inverse_name="product_policy_id",
        string="Delivery Models",
    )

    # Related for search view
    receiver_id = fields.Many2one(
        related="product_id.partner_id", readonly=True, store=True
    )
    product_dyn_state_id = fields.Many2one(
        related="product_id.dyn_state_id", readonly=True, store=True
    )
    payout_currency_id = fields.Many2one(
        related="product_id.payout_currency_id", readonly=True, store=True
    )
    instrument_type_id = fields.Many2one(
        related="product_id.instrument_type_id", readonly=True, store=True
    )
    receiver_name_technical = fields.Char(
        related="product_id.partner_id.name_technical", readonly=True, store=True
    )

    is_holiday_outdated = fields.Boolean(
        "Outdated Holiday Lists", compute="_compute_is_holiday_outdated"
    )

    has_bank_instrument = fields.Boolean(
        related="product_id.has_bank_instrument",
        help="Helper field for Delivery Policy",
    )
    default_delivery_type = fields.Selection(
        [("direct", "Direct"), ("3rd_party", "3rd Party")],
        compute="_compute_default_delivery_type",
        help="Helper field for Delivery Policy",
    )
    is_policy_applied = fields.Boolean(
        "Applied on Product", compute="_compute_is_policy_applied"
    )

    @api.depends("product_delivery_ids", "product_delivery_ids.mode")
    def _compute_is_timezone_required(self):
        for product_policy in self:
            delivery_modes = product_policy.product_delivery_ids.mapped("mode")
            product_policy.is_timezone_required = any(
                [m in ["t_days", "open_close"] for m in delivery_modes]
            )

    @api.depends("product_id.fdd_policy_id")
    def _compute_is_policy_applied(self):
        for product_policy in self:
            product_policy.is_policy_applied = (
                product_policy.product_id.fdd_policy_id == product_policy
            )

    @api.depends("has_bank_instrument")
    def _compute_default_delivery_type(self):
        for product_policy in self:
            product_policy.default_delivery_type = (
                "3rd_party" if product_policy.has_bank_instrument else "direct"
            )

    @api.depends("product_delivery_ids.is_holiday_outdated")
    def _compute_is_holiday_outdated(self):
        for product_policy in self:
            product_policy.is_holiday_outdated = any(
                product_policy.product_delivery_ids.mapped("is_holiday_outdated")
            )

    @api.model
    def read_group(
        self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True
    ):
        """
        Add string to groups which search on a boolean (instead of true/false)
        """
        res = super(HomFddPolicyProduct, self).read_group(
            domain,
            fields,
            groupby,
            offset=offset,
            limit=limit,
            orderby=orderby,
            lazy=lazy,
        )
        for group in res:
            if "is_standard" in group.keys():
                if group["is_standard"]:
                    group["is_standard"] = "Standard"
                else:
                    group["is_standard"] = "Not Standard"
        return res

    @api.depends("product_delivery_ids")
    def _compute_delivery_info(self):
        """
        Create string with data from associated delivery models
        """
        for product_policy in self:
            product_policy.assign_delivery_info(product_policy.product_delivery_ids)

    @api.onchange(*(FDD_POLICY_COMMON_FIELDS + ["product_delivery_ids"]))
    def _onchange_all_fields(self):
        """
        If a product policy is changed, the user is required to change its name to
        a different one than the origin RSP policy
        """
        self.ensure_one()
        if self.parent_receiver_policy_id.name == self.name:
            self.name = False

    def name_get(self):
        if not self.env.context.get("short_name"):
            result = []
            for product_policy in self:
                name = f"{product_policy.name} ({product_policy.product_id.name})"
                result.append((product_policy.id, name))
            return result
        return super().name_get()

    @api.model
    def create(self, vals):
        """
        Hardcode country at creation for domain on holiday_list_ids to work
        """
        res = super(HomFddPolicyProduct, self).create(vals)
        if not res.country_id:
            res.country_id = res.product_id.country_id
        return res

    def copy_receiver_fdd_delivery(self, receiver_delivery):
        """
        Copy a hom.fdd.delivery.receiver to hom.fdd.delivery.product and mark it as
        standard
        """
        product_delivery_vals = tools.get_value_dict(
            FDD_DELIVERY_COMMON_FIELDS, receiver_delivery
        )
        product_delivery_vals.update(
            {
                "product_policy_id": self.id,
                "parent_receiver_delivery_id": receiver_delivery.id,
                "is_standard": True,
            }
        )
        self.env["hom.fdd.delivery.product"].with_context(is_copy=True).create(
            product_delivery_vals
        )

    def is_outdated(self):
        if self.is_standard:
            if not self.parent_receiver_policy_id:
                # Specific case if policy was deleted on Receiver: the product policy is marked standard but has no
                # parent. It will have to be removed and the product reassigned a new policy.
                return True
            if (
                self.parent_receiver_policy_id
                != self.product_id.get_receiver_fdd_policy()
            ):
                # The current policy no longer applies
                return True
            for field in FDD_POLICY_COMMON_FIELDS:
                if self[field] != self.parent_receiver_policy_id[field]:
                    return True
            for product_delivery in self.product_delivery_ids:
                if product_delivery.is_outdated():
                    return True
            for (
                receiver_delivery
            ) in self.parent_receiver_policy_id.receiver_delivery_ids:
                if receiver_delivery not in self.product_delivery_ids.mapped(
                    "parent_receiver_delivery_id"
                ):
                    # A new delivery model on Receiver policy is not yet present on product
                    return True
        return False

    def button_reset_standard(self):
        """
        Reset product policy values to same as parent_receiver_policy_id on receiver
        Write all values in one so the standard flag is in sync and checks on write
        aren't triggered unnecessarily
        """
        self.ensure_one()
        if self.parent_receiver_policy_id:
            write_vals = {}
            for field in FDD_POLICY_COMMON_FIELDS:
                if self[field] != self.parent_receiver_policy_id[field]:
                    write_vals[field] = tools.get_field_write_value(
                        field, self.parent_receiver_policy_id
                    )
            for product_delivery in self.product_delivery_ids:
                # Sync delivery models
                if product_delivery.parent_receiver_delivery_id:
                    # If they have an origin, reset them to origin values
                    product_delivery.button_reset_standard()
                else:
                    # If they were manually added, remove them
                    product_delivery.unlink()
            for (
                receiver_delivery
            ) in self.parent_receiver_policy_id.receiver_delivery_ids:
                if receiver_delivery not in self.product_delivery_ids.mapped(
                    "parent_receiver_delivery_id"
                ):
                    # If they don't exist on the product, copy them
                    self.copy_receiver_fdd_delivery(receiver_delivery)

            if not self.is_standard:
                write_vals["is_standard"] = True
            self.with_context(skip_standard_check=True).write(write_vals)

    def write(self, vals):
        """
        If a value on a main field differs from the origin, we remove standard flag
        """
        if not self.env.context.get("skip_standard_check"):
            for product_policy in self:
                if product_policy.parent_receiver_policy_id:
                    for field in FDD_POLICY_COMMON_FIELDS:
                        if field in vals and vals[field] != tools.get_field_write_value(
                            field, product_policy.parent_receiver_policy_id
                        ):
                            vals["is_standard"] = False
                            break

        res = super(HomFddPolicyProduct, self).write(vals)

        for product_policy in self:
            if (
                not product_policy.is_standard
                and product_policy.name == product_policy.parent_receiver_policy_id.name
            ):
                if "is_standard" in vals or self.env.context.get("auto_edit_product"):
                    # For automated changes, do not check on the name to avoid errors
                    product_policy.name = f"{product_policy.name} (edited)"
                else:
                    raise exceptions.UserError(
                        _(
                            "The policy name needs to differ from its origin if it is not standard"
                        )
                    )

        return res

    def unlink(self):
        if not self.env.context.get("skip_unlink_check"):
            for product_policy in self:
                if product_policy.is_policy_applied:
                    raise exceptions.ValidationError(
                        _(
                            "You cannot delete a policy that is currently applied. Choose another or apply the "
                            "Standard Receiver Policy first."
                        )
                    )
        return super().unlink()
