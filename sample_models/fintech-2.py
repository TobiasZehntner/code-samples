# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models

from ..tools import tools  # pylint: disable=W7935

WEEKEND_FIELDS = [
    "weekend_mon",
    "weekend_tue",
    "weekend_wed",
    "weekend_thu",
    "weekend_fri",
    "weekend_sat",
    "weekend_sun",
]
FDD_DELIVERY_COMMON_FIELDS = WEEKEND_FIELDS + [
    "delivery_type",
    "mode",
    "bank_prefixes",
    "duration_min",
    "duration_hour",
    "duration_day",
    "time_open_float",
    "time_close_everyday_float",
    "time_close_holiday_float",
    "holiday_day_ids",
    "holiday_list_ids",
]


class HomFddDelivery(models.AbstractModel):
    _name = "hom.fdd.delivery"
    _description = "FDD Delivery Abstract"

    delivery_type = fields.Selection(
        [("direct", "Direct"), ("3rd_party", "3rd Party")],
        "Delivery Type",
        required=True,
    )
    mode = fields.Selection(
        [
            ("non_stop", "Non-Stop"),
            ("t_days", "T+X Days"),
            ("open_close", "Opening/Closing"),
        ],
        "Mode",
        default="non_stop",
        required=True,
    )
    country_id = fields.Many2one("res.country", "Country")
    bank_prefixes = fields.Char("Bank Prefixes")

    duration_min = fields.Integer(
        "Delivery Duration (minutes)",
        help="Delivery Duration in minutes for modes 'Non-Stop' and 'Opening/Closing'",
    )
    duration_hour = fields.Integer(
        "Delivery Duration (hours)",
        help="Delivery Duration in hours for mode 'Opening/Closing'",
    )
    duration_day = fields.Integer(
        "Delivery Duration (days)",
        help="Delivery Duration in days for mode 'T+X Days'. 0 = Same day",
    )
    duration_total_min = fields.Integer(
        "Total Delivery Duration (minutes)",
        compute="_compute_duration_total_min",
        store=True,
    )

    time_open = fields.Char(
        compute="_compute_time_open", store=True, help="Technical field for Rubiko"
    )
    time_open_float = fields.Float(
        "Opening", default=9, help="Time when the bank starts business every day"
    )
    time_close_everyday = fields.Char(
        compute="_compute_time_close_everyday",
        store=True,
        help="Technical field for Rubiko",
    )
    time_close_everyday_float = fields.Float(
        "Closing Time Everyday",
        default=17,
        help="Time when the bank stops processing transactions every day",
    )
    time_close_holiday = fields.Char(
        compute="_compute_time_close_holiday",
        store=True,
        help="Technical field for Rubiko",
    )
    time_close_holiday_float = fields.Float(
        "Closing Time before Holiday",
        default=17,
        help="Time when the bank stops processing transactions before weekends and "
        "holidays",
    )

    weekend_mon = fields.Boolean(
        "Monday", help="Select 0-3 consecutive days that count as weekend"
    )
    weekend_tue = fields.Boolean(
        "Tuesday", help="Select 0-3 consecutive days that count as weekend"
    )
    weekend_wed = fields.Boolean(
        "Wednesday", help="Select 0-3 consecutive days that count as weekend"
    )
    weekend_thu = fields.Boolean(
        "Thursday", help="Select 0-3 consecutive days that count as weekend"
    )
    weekend_fri = fields.Boolean(
        "Friday", help="Select 0-3 consecutive days that count as weekend"
    )
    weekend_sat = fields.Boolean(
        "Saturday",
        default=True,
        help="Select 0-3 consecutive days that count as weekend",
    )
    weekend_sun = fields.Boolean(
        "Sunday", default=True, help="Select 0-3 consecutive days that count as weekend"
    )

    is_holiday_outdated = fields.Boolean(
        "Outdated Holiday Lists", compute="_compute_is_holiday_outdated"
    )

    @api.depends("time_open_float")
    def _compute_time_open(self):
        for delivery in self:
            delivery.time_open = tools.float_time_to_string(delivery.time_open_float)

    @api.depends("time_close_everyday_float")
    def _compute_time_close_everyday(self):
        for delivery in self:
            delivery.time_close_everyday = tools.float_time_to_string(
                delivery.time_close_everyday_float
            )

    @api.depends("time_close_holiday_float")
    def _compute_time_close_holiday(self):
        for delivery in self:
            delivery.time_close_holiday = tools.float_time_to_string(
                delivery.time_close_holiday_float
            )

    def get_orphan_days(self):
        """
        Get days which lists are not present and that weren't added manually
        """
        self.ensure_one()
        orphan_days = self.env["hom.holiday.day"]
        for day in self.holiday_day_ids:
            if day not in self.manual_holiday_day_ids and all(
                [day_list not in self.holiday_list_ids for day_list in day.list_ids]
            ):
                orphan_days += day
        return orphan_days

    @api.depends("holiday_list_ids", "holiday_day_ids")
    def _compute_is_holiday_outdated(self):
        """
        Set flag if the days differ to the ones configured in the system
        (i.e. the lists were updated after they were added to the delivery model)
        """
        for delivery in self:
            missing_days = (
                delivery.holiday_list_ids.mapped("day_ids") - delivery.holiday_day_ids
            )
            delivery.is_holiday_outdated = missing_days or delivery.get_orphan_days()

    def write(self, vals):
        """
        Compare before/after values and keep trace of manually added holiday days.
        """

        def _compare(old, new):
            return new - old, old - new

        holidays = {}
        for delivery in self:
            holidays[delivery] = {
                "prev_days": delivery.holiday_day_ids,
                "prev_lists": delivery.holiday_list_ids,
            }

        res = super(HomFddDelivery, self).write(vals)

        if not self.env.context.get("skip_compare"):
            for delivery in self:
                delivery_write = delivery.with_context(skip_compare=True)
                added_days, removed_days = _compare(
                    holidays[delivery]["prev_days"], delivery.holiday_day_ids
                )
                added_lists, removed_lists = _compare(
                    holidays[delivery]["prev_lists"], delivery.holiday_list_ids
                )
                added_list_days = added_lists.mapped("day_ids")
                manually_added_days = self.env["hom.holiday.day"]
                for added_day in added_days:
                    if added_day not in added_list_days:
                        manually_added_days |= added_day
                if manually_added_days:
                    delivery_write.manual_holiday_day_ids |= manually_added_days
                if removed_days:
                    delivery_write.manual_holiday_day_ids -= removed_days

        return res

    @api.constrains(
        "time_open",
        "time_close_everyday",
        "time_close_holiday",
        "time_open_float",
        "time_close_everyday_float",
        "time_close_holiday_float",
    )
    def _constrains_time(self):
        """
        Check time format on Char fields
        """
        time_char_fields = ["time_open", "time_close_everyday", "time_close_holiday"]
        time_float_fields = [
            "time_open_float",
            "time_close_everyday_float",
            "time_close_holiday_float",
        ]
        time_char_field_dict = {
            time_field: self._fields.get(time_field).string
            for time_field in time_char_fields
        }
        for delivery in self:
            # Check float fields
            for float_field in time_float_fields:
                if not 0 <= delivery[float_field] < 24:
                    raise exceptions.ValidationError(
                        _("Please set a time between 00:00 and 23:59")
                    )
            # Check char fields
            for char_field, name in time_char_field_dict.items():
                tools.check_string_is_time(delivery[char_field], name)

    @api.constrains(*WEEKEND_FIELDS)
    def _constrains_weekend_days(self):
        """
        The weekend has to be 0-3 consecutive days
        """
        for delivery in self:
            # Create a list of chosen days with Monday=0 to Sunday=6
            chosen_days = [
                i for i, field in enumerate(WEEKEND_FIELDS) if delivery[field]
            ]
            if len(chosen_days) > 3:
                # Not longer than 3 days
                raise exceptions.ValidationError(
                    _("The weekend cannot be longer than 3 days")
                )
            elif len(chosen_days) > 1:
                for i, ind in enumerate(chosen_days):
                    if i > 0 and not ind - 1 == chosen_days[i - 1]:
                        # Days are not consecutive
                        if chosen_days in [[0, 6], [0, 5, 6], [0, 1, 6]]:
                            # Sunday-Monday rollover: allowed
                            continue
                        raise exceptions.ValidationError(
                            _("The weekend has to be defined as 0-3 consecutive days")
                        )

    def button_update_holidays(self):
        """
        Sync days with the present lists as configured in config
        """
        self.ensure_one()
        self.invalidate_cache(
            ["holiday_day_ids"]
        )  # Cache needs update if days were removed
        self.with_context(skip_compare=True).holiday_day_ids = (
            self.manual_holiday_day_ids
            | self.holiday_list_ids.mapped("day_ids") - self.get_orphan_days()
        )

    @api.onchange("holiday_list_ids")
    def _onchange_holiday_list_ids(self):
        """
        Adding a list: add all its days
        Removing a list: if a list is complete (all its days are present) but the list
        is no longer there -> a list has been removed and its days should be removed
        too (unless they belong also to another list that is present)
        """
        self.ensure_one()
        self.holiday_day_ids |= self.holiday_list_ids.mapped("day_ids")

        days_to_remove = self.env["hom.holiday.day"]
        for holiday_list in self.holiday_day_ids.mapped("list_ids"):
            # All lists the days are linked to
            if holiday_list not in self.holiday_list_ids:
                # List should be present but isn't
                if all(day in self.holiday_day_ids for day in holiday_list.day_ids):
                    # all days of a non-present list are present
                    for day in holiday_list.day_ids:
                        other_lists = day.list_ids.filtered(lambda l: l != holiday_list)
                        if all(li not in self.holiday_list_ids for li in other_lists):
                            # Day is not part of other lists: remove
                            days_to_remove += day

        self.holiday_day_ids -= days_to_remove

    @api.onchange("holiday_day_ids")
    def _onchange_holiday_day_ids(self):
        """
        If a day from a list is removed, the list is no longer complete and should be removed. The remaining days of
        that list have to be added to manually_added_days if they're not also part of another list.
        """
        self.ensure_one()
        lists_to_remove = self.env["hom.holiday.list"]
        for holiday_list in self.holiday_list_ids:
            if not all(day in self.holiday_day_ids for day in holiday_list.day_ids):
                lists_to_remove += holiday_list

        self.holiday_list_ids -= lists_to_remove
        self.manual_holiday_day_ids += self.get_orphan_days()

    @api.multi
    @api.depends("duration_min", "duration_hour", "duration_day")
    def _compute_duration_total_min(self):
        """
        Compute total minutes duration for Rubiko
        """
        for delivery in self:
            total_minutes = (
                (delivery.duration_day * 1440)
                + (delivery.duration_hour * 60)
                + delivery.duration_min
            )
            delivery.duration_total_min = total_minutes

    @api.onchange("mode")
    def _onchange_mode(self):
        """
        Reset duration fields on mode change
        """
        self.ensure_one()
        self.duration_min = 0
        self.duration_day = 0
        self.duration_hour = 0

    @api.constrains("delivery_type", "bank_prefixes")
    def _constrains_delivery_type(self):
        """
        Check for bank details
        """
        for receiver_delivery in self:
            if (
                receiver_delivery.delivery_type == "direct"
                and receiver_delivery.has_bank_instrument
                and not receiver_delivery.bank_prefixes
            ):
                raise exceptions.ValidationError(
                    _("Missing bank details on direct Delivery Model")
                )


class HomFddDeliveryReceiver(models.Model):
    _name = "hom.fdd.delivery.receiver"
    _description = "FDD Delivery Receiver"
    _inherit = "hom.fdd.delivery"

    receiver_policy_id = fields.Many2one(
        "hom.fdd.policy.receiver",
        "Receiver Policy",
        required=True,
        ondelete="cascade",
        copy=False,
    )
    country_domain_ids = fields.Many2many(
        related="receiver_policy_id.country_domain_ids"
    )
    holiday_day_ids = fields.Many2many(
        comodel_name="hom.holiday.day",
        relation="receiver_delivery_holiday_day_rel",
        column1="receiver_delivery_id",
        column2="day_id",
        string="Holiday Days",
    )
    holiday_list_ids = fields.Many2many(
        comodel_name="hom.holiday.list",
        relation="receiver_delivery_holiday_list_rel",
        column1="receiver_delivery_id",
        column2="list_id",
        string="Holiday Lists",
    )
    manual_holiday_day_ids = fields.Many2many(
        comodel_name="hom.holiday.day",
        relation="receiver_delivery_manual_holiday_day_rel",
        column1="receiver_delivery_id",
        column2="day_id",
        string="Manually added Holiday Days",
        help="Table to keep track on which days were added manually without a list",
    )

    has_bank_instrument = fields.Boolean(
        related="receiver_policy_id.has_bank_instrument"
    )

    def name_get(self):
        """
        [policy name] ([type] - [mode])
        """
        result = []
        for receiver_delivery in self:
            delivery_type_str = dict(
                receiver_delivery._fields["delivery_type"].selection
            ).get(receiver_delivery.delivery_type)
            mode_str = dict(receiver_delivery._fields["mode"].selection).get(
                receiver_delivery.mode
            )
            name = (
                f"{receiver_delivery.receiver_policy_id.name} "
                f"({delivery_type_str} - {mode_str})"
            )
            result.append((receiver_delivery.id, name))
        return result


class HomFddDeliveryProduct(models.Model):
    _name = "hom.fdd.delivery.product"
    _description = "FDD Delivery Product"
    _inherit = "hom.fdd.delivery"

    product_policy_id = fields.Many2one(
        "hom.fdd.policy.product", "Product Policy", ondelete="cascade", copy=False
    )
    parent_receiver_delivery_id = fields.Many2one(
        "hom.fdd.delivery.receiver", "Origin Delivery Model", readonly=True
    )
    is_standard = fields.Boolean(readonly=True)

    country_id = fields.Many2one("res.country", related="product_policy_id.country_id")

    holiday_day_ids = fields.Many2many(
        comodel_name="hom.holiday.day",
        relation="product_delivery_holiday_day_rel",
        column1="product_delivery_id",
        column2="day_id",
        string="Holiday Days",
    )
    holiday_list_ids = fields.Many2many(
        comodel_name="hom.holiday.list",
        relation="product_delivery_holiday_list_rel",
        column1="product_delivery_id",
        column2="list_id",
        string="Holiday Lists",
    )
    manual_holiday_day_ids = fields.Many2many(
        comodel_name="hom.holiday.day",
        relation="product_delivery_manual_holiday_day_rel",
        column1="product_delivery_id",
        column2="day_id",
        string="Manually added Holiday Days",
        help="Table to keep track on which days were added manually without a list",
    )
    has_bank_instrument = fields.Boolean(
        related="product_policy_id.has_bank_instrument"
    )

    @api.model
    def create(self, vals):
        """
        Set product policy to non-standard after a delivery is added
        """
        res = super(HomFddDeliveryProduct, self).create(vals)
        if not self.env.context.get("is_copy") and res.product_policy_id.is_standard:
            res.product_policy_id.is_standard = False
        return res

    def write(self, vals):
        """
        If a value on a main field differs from the origin, we remove standard flag
        on the delivery and the parent policy
        """
        if not self.env.context.get("skip_standard_check"):
            for product_delivery in self:
                if product_delivery.parent_receiver_delivery_id:
                    for field in FDD_DELIVERY_COMMON_FIELDS:
                        if field in vals and vals[field] != tools.get_field_write_value(
                            field, product_delivery.parent_receiver_delivery_id
                        ):
                            vals["is_standard"] = False
                            product_delivery.product_policy_id.is_standard = False
                            break
        return super(HomFddDeliveryProduct, self).write(vals)

    def unlink(self):
        """
        If we remove a delivery that was linked to origin receiver policy,
         we remove the standard flag on the policy

        """
        for product_delivery in self:
            if product_delivery.parent_receiver_delivery_id:
                product_delivery.product_policy_id.is_standard = False
        return super().unlink()

    def is_outdated(self):
        if self.is_standard and self.parent_receiver_delivery_id:
            for field in FDD_DELIVERY_COMMON_FIELDS:
                if self[field] != self.parent_receiver_delivery_id[field]:
                    return True
        return False

    def button_reset_standard(self):
        """
        Reset policy values to same as parent_receiver_delivery_id
        """
        self.ensure_one()
        if self.parent_receiver_delivery_id:
            write_vals = {}
            for field in FDD_DELIVERY_COMMON_FIELDS:
                if self[field] != self.parent_receiver_delivery_id[field]:
                    write_vals[field] = tools.get_field_write_value(
                        field, self.parent_receiver_delivery_id
                    )
            if not self.is_standard:
                write_vals["is_standard"] = True
            self.with_context(skip_standard_check=True).write(write_vals)

    def button_update_holidays(self):
        """
        If we change a standard product to non-standard automatically we need to edit its name so it differs from the
        original
        """
        super(
            HomFddDeliveryProduct, self.with_context(auto_edit_product=True)
        ).button_update_holidays()
