# Code Samples

Repository with code samples. ©2024 Tobias Zehntner

### Professional (Odoo: Python, XML, JS, PostgreSQL)

- [Complex Fintech Models](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_models)
- [Sample Modules](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules)
  - [Highlight modifications](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/highlight_modifications)  with complex SQL queries and JavaScript. High value to the customer with a combination of improved user interface and handling of large data in short amounts of time.
  - [API onwrite Decorator](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/api_onwrite_decorator) extends the ORM with a useful tool
  - [Custom API connector](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/custom_api_connector) uses an external database to enrich contacts
  - [Custom MRP Workflow](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/custom_mrp_workflow) adapts MRP for a customer
  - [Dynamic State](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/dynamic_state) allows to define workflows with statuses, set which statuses can be followed, which are reachable manually (by click on Statusbar) and which aren’t.
  - [Dynanic State Constraints](https://gitlab.com/TobiasZehntner/code-samples/-/tree/main/sample_modules/dynamic_state_constraint) allows constraints with conditions to other workflows (model A cannot go to status X, if linked model B is in status Y)


### Personal (Python, C++)
See visual project documentation on [tobiaszehntner.studio](https://tobiaszehntner.studio/).

#### Flash Field (2024, prototype)
For 50 programmable flash units, synced via GPS. &rarr; [Animation](https://www.instagram.com/p/CvJ5vU0oL35/)

- [Firmware (C++ on Arduino)](https://gitlab.com/TobiasZehntner/2023-flash-field/-/tree/develop/flash_field)

#### Halo (2022)
Async Python code with server/client communication over Wifi (to main controller) and Serial communication over cable (to Arduino). C++ to control motor, light and read sensors. &rarr; [Video](https://tobiaszehntner.studio/work/halo)

- [Controller (Python on RPi)](https://gitlab.com/TobiasZehntner/2021-halo/-/tree/develop/halo_controller)
- [Unit (Python on RPi)](https://gitlab.com/TobiasZehntner/2021-halo/-/tree/develop/halo_unit)
- [Firmware (C++ on Arduino)](https://gitlab.com/TobiasZehntner/2021-halo/-/blob/develop/halo_unit_arduino/driver/driver.ino)
