# © 2019 Niboo SPRL (https://www.niboo.com/)
# © 2019 Okia SPRL (https://okia.be)
# License Other proprietary.

{
    "name": "Dynamic State Constraints",
    "category": "Other",
    "summary": "Cross-workflow constraints",
    "website": "https://www.niboo.com",
    "version": "11.0.1.0.0",
    "license": "Other proprietary",
    "author": "Niboo",
    "description": """
        Adds cross-workflow constraints between different models and states: A
        record in one state is only allowed to have links to other records in
        certain states.
    """,
    "depends": ["dynamic_state", "api_onwrite_decorator"],
    "data": [
        "security/ir.model.access.csv",
        "views/dynamic_state.xml",
        "views/dynamic_state_constraint.xml",
    ],
    "installable": True,
    "application": False,
}
