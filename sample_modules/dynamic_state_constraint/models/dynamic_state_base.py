# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, models


class DynamicStateBase(models.Model):
    _inherit = "dynamic.state.base"

    @api.onwrite("dyn_state_id")
    def constrains_dyn_state_id(self):
        """
        1. Check constraints on the state and apply action
            - raise error or
            - force state on linked record
        2. If necessary, reverse any previously forced states on linked records
        """
        for record in self:
            record.check_state_constraints()
            record.handle_reverse_state()

    def check_state_constraints(self):
        """
        Check constraints for the cross-workflow records
        """
        if self._context.get("disable_dynamic_state_constrains"):
            return
        self.ensure_one()
        for constraint in self.dyn_state_id.constraint_ids:

            # Skip if constraint is not active
            if not constraint.active:
                continue

            # Skip if constraint doesn't apply to this record
            if not constraint.is_constrain_all_records:
                constraint_records = constraint.get_constraint_records(self)
                if self not in constraint_records:
                    continue

            # Get cross-workflow records where this constraint applies to
            to_apply_records = constraint.get_to_apply_records(self)

            # If constraint is not forcing a state and has no allowed states,
            # no linked records are allowed in the current state
            if (
                to_apply_records
                and constraint.action_type != "force_state"
                and not constraint.allowed_state_ids
            ):
                raise exceptions.ValidationError(
                    _('A %s cannot be in status "%s" while it has a linked %s')
                    % (
                        self._description,
                        self.dyn_state_id.name,
                        to_apply_records[0]._description,
                    )
                )

            self.apply_constraints(constraint, to_apply_records)

    def apply_constraints(self, constraint, to_apply_records):
        """
        Apply constraint on records
        """
        if self._context.get("disable_dynamic_state_constrains"):
            return
        for to_apply_record in to_apply_records:
            # Check if cross-workflow record is in a non-allowed state
            if to_apply_record.dyn_state_id not in constraint.allowed_state_ids:

                # Raise if constraint of type raise
                if constraint.action_type == "raise":
                    raise exceptions.ValidationError(
                        _(
                            'A %s cannot move to status "%s" while a linked %s '
                            'has status "%s"'
                        )
                        % (
                            self._description,
                            self.dyn_state_id.name,
                            to_apply_record._description,
                            to_apply_record.dyn_state_id.name,
                        )
                    )

                # if force_state, change state on cross-record
                elif constraint.action_type == "force_state":
                    force_state = constraint.force_state_id
                    comment = _(
                        "Automatic Forced Status Change: Linked %s %s " 'moved to "%s"'
                    ) % (self._description, self.display_name, self.dyn_state_id.name)
                    # TODO only force if not to_apply_record.dyn_state_id.is_terminal?
                    #  -> if a sender is suspended, rejected corridors don't need to be
                    #  suspended. Confirm with client
                    if not to_apply_record.dyn_state_id.is_terminal:
                        to_apply_record.go_to_state(
                            force_state,
                            comment=comment,
                            auto_force_record=self,
                            disable_state_check=True,
                        )

    def handle_reverse_state(self):
        """
        If this status change is a move back to the previous state and
        the state we're leaving has any force_state constraints,
        we check if there are any linked records that should have their
        status reversed as well
        """
        self.ensure_one()

        # Get the 2 last history entries:
        # latest_histories[0]: this status change
        # latest_histories[1]: the previous status change
        latest_histories = self.env["dynamic.state.history"].search(
            [("model", "=", self._name), ("res_id", "=", self.id)],
            order="date desc",
            limit=2,
        )

        # Check if it's a return to the previous state
        if (
            len(latest_histories) == 2
            and latest_histories[0].from_state_id == latest_histories[1].to_state_id
        ):

            # Look for force_state constraints on the status we're leaving
            previous_force_constraints = latest_histories[
                0
            ].from_state_id.constraint_ids.filtered(
                lambda c: c.action_type == "force_state"
            )

            for prev_constraint in previous_force_constraints:
                # Get records that might have been forced
                forced_records = prev_constraint.get_to_apply_records(self)

                for forced_record in forced_records:
                    # Is the forced record still in the status that would
                    # have been forced?
                    if forced_record.dyn_state_id == prev_constraint.force_state_id:

                        # Check for latest history of forced record
                        forced_record_history = self.env[
                            "dynamic.state.history"
                        ].search(
                            [
                                ("model", "=", forced_record._name),
                                ("res_id", "=", forced_record.id),
                            ],
                            order="date desc",
                            limit=1,
                        )

                        # Was last change forced by the current record?
                        if (
                            self._name == forced_record_history.auto_force_model
                            and self.id == forced_record_history.auto_force_res_id
                        ):
                            # Reverse status on cross record
                            comment = _(
                                "Automated return to previous status. "
                                'Linked %s %s returned to "%s"'
                            ) % (
                                self._description,
                                self.display_name,
                                self.dyn_state_id.name,
                            )

                            forced_record.with_context(
                                disable_dynamic_state_constrains=True
                            ).go_to_state(
                                forced_record_history.from_state_id,
                                comment,
                                disable_state_check=True,
                            )
