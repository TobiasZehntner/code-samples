# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, fields, models
from odoo.tools.safe_eval import safe_eval, test_python_expr


class DynamicStateConstraint(models.Model):
    _name = "dynamic.state.constraint"
    _description = "Status Constraint"
    _order = "sequence"

    active = fields.Boolean("Active", default=True)
    model_id = fields.Many2one(
        related="constrain_state_id.workflow_id.model_id", readonly=True
    )
    target_model_id = fields.Many2one(
        related="apply_workflow_id.model_id", readonly=True
    )

    constrain_state_id = fields.Many2one(
        "dynamic.state", "Constrain Status", required=True
    )
    constrain_workflow_id = fields.Many2one(
        "dynamic.state.workflow",
        related="constrain_state_id.workflow_id",
        store=True,
        readonly=True,
    )
    is_constrain_all_records = fields.Boolean(
        "Constrain All Records",
        default=True,
        help="Constrain all records with this Status",
    )
    apply_workflow_id = fields.Many2one(
        "dynamic.state.workflow",
        "Apply on Workflow",
        required=True,
        help="The workflow that should be cross-checked.",
    )
    allowed_state_ids = fields.Many2many(
        "dynamic.state",
        string="Allowed Statuses",
        help="The Statuses that records from the other workflow are allowed "
        "to be in. If none are defined, no linked record is allowed.",
    )

    action_type = fields.Selection(
        [("raise", "Raise Error"), ("force_state", "Force Status")],
        "Action",
        default="raise",
        required=True,
        help="Action applied to records that do not comply with the allowed "
        "Statuses: - Raise an error, - Force the other records into "
        "a certain status.",
    )
    force_state_id = fields.Many2one(
        "dynamic.state",
        "Force into",
        help="The Status the non-complying " "records should be forced into.",
    )

    code_get_constraint_records = fields.Text(
        string="Code to retrieve constraint records"
    )
    code_get_apply_records = fields.Text(
        string="Code to retrieve to-apply-records", required=True
    )

    sequence = fields.Integer("Sequence", default=9999)

    @api.multi
    def name_get(self):
        """
        Create display_name from chosen workflow and status
        """
        result = []
        for constraint in self:
            if (
                constraint.constrain_state_id.workflow_id
                and constraint.constrain_state_id
            ):
                name = _('%s: "%s" > %s') % (
                    constraint.constrain_state_id.workflow_id.display_name,
                    constraint.constrain_state_id.name,
                    constraint.apply_workflow_id.display_name,
                )

            else:
                name = _("New Constraint")

            result.append((constraint.id, name))

        return result

    @api.constrains("code_get_constraint_records")
    def _check_code_get_constraint_records(self):
        """
        Check if python code to get constraint records is valid
        """
        for check in self:
            if not check.is_constrain_all_records:
                msg = test_python_expr(
                    expr=check.code_get_constraint_records.strip(), mode="exec"
                )
                if msg:
                    raise exceptions.ValidationError(msg)

    @api.constrains("code_get_apply_records")
    def _check_code_get_apply_records(self):
        """
        Check if python code to get to-apply records is valid
        """
        for check in self:
            msg = test_python_expr(
                expr=check.code_get_apply_records.strip(), mode="exec"
            )
            if msg:
                raise exceptions.ValidationError(msg)

    def _get_eval_context(self, record):
        self.ensure_one()

        model = self.env[self.model_id.model]

        eval_context = {
            "env": self.env,
            "model": model,
            # Exceptions
            "Warning": exceptions.UserError,
            # record
            "record": record,
        }

        if self.target_model_id:
            eval_context["target_model"] = self.env[self.target_model_id.model]

        return eval_context

    def get_constraint_records(self, apply_on_record):
        records = self._get_records(apply_on_record, self.code_get_constraint_records)

        if records and self.constrain_state_id.model != records[0]._name:
            raise exceptions.UserError(
                _("Comparing apples and oranges (%s <> %s)")
                % (self.constrain_state_id.model, records[0]._name)
            )
        return records

    def get_to_apply_records(self, apply_on_record):
        records = self._get_records(apply_on_record, self.code_get_apply_records)

        if records and self.apply_workflow_id.model_id.model != records[0]._name:
            raise exceptions.UserError(
                _("Comparing apples and oranges (%s <> %s)")
                % (self.apply_workflow_id.model_id.model, records[0]._name)
            )
        return records

    def _get_records(self, apply_on_record, code):
        self.ensure_one()

        if not code:
            raise exceptions.UserError(_("Please set the code to get records"))

        if not self.apply_workflow_id:
            raise exceptions.UserError(_("Please set the to-apply workflow"))

        eval_context = self._get_eval_context(apply_on_record)

        records = False
        try:
            records = safe_eval(code.strip(), eval_context)
        except Exception as e:
            raise exceptions.UserError(_("Error when evaluating the code: %s" % e))

        return records
