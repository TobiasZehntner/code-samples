# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import fields, models


class DynamicState(models.Model):
    _inherit = "dynamic.state"

    constraint_ids = fields.One2many(
        "dynamic.state.constraint",
        inverse_name="constrain_state_id",
        string="Constraints",
        domain=["|", ("active", "=", True), ("active", "=", False)],
    )
