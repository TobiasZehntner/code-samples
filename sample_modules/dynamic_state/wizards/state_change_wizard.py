# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, fields, models


class StateChangeWizard(models.TransientModel):
    _name = "state.change.wizard"
    _description = "State Change Wizard"

    res_id = fields.Integer("Record ID", required=True, readonly=True)
    model = fields.Char("Model", required=True, readonly=True)

    type = fields.Selection(
        [
            ("choose", "Choose Status"),
            ("specific", "Specific Status"),
            ("next", "Go To Next Status"),
            ("return", "Return to Previous Status"),
        ],
        "Type",
        default="choose",
        required=True,
    )

    from_state_id = fields.Many2one(
        "dynamic.state", "From", required=True, readonly=True
    )
    to_state_id = fields.Many2one("dynamic.state", "To")
    allowed_next_state_ids = fields.Many2many(
        related="from_state_id.allowed_next_state_ids", readonly=True
    )

    comment = fields.Text("Comment")
    is_comment_required = fields.Boolean(
        "Require Comment",
        compute="_compute_is_comment_required",
        store=True,
        help="Helper field for wizard view",
    )

    @api.multi
    @api.depends("from_state_id", "to_state_id")
    def _compute_is_comment_required(self):
        """
        XML view helper field: check if From or To state require comments
        """
        for wizard in self:
            wizard.is_comment_required = (
                wizard.from_state_id.is_departure_comment_required
                or wizard.to_state_id.is_arrival_comment_required
            )

    def change_state(self):
        """
        Set record to the chosen status
        """
        self.ensure_one()
        if self.is_comment_required and not self.comment:
            raise exceptions.ValidationError(
                _("Please add a comment regarding the status change")
            )

        record = self.env[self.model].browse(self.res_id)
        record.go_to_state(self.to_state_id, self.comment)
