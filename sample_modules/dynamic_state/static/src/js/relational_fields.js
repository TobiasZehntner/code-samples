odoo.define('dynamic_state.relational_fields_dynamic', function (require) {
    "use strict";

    var RelationalFields = require('web.relational_fields');
    var FieldStatusDynamic = RelationalFields.FieldStatus.include({

        // Handle rendering of statusbar buttons
        _setState: function () {
            var self = this;
            if (this.field.type === 'many2one' &&
                this.field.relation === 'dynamic.state'
            ) {
                this.status_information = _.map(this.record.specialData[this.name],
                    function (info) {
                        return _.extend({
                            selected: info.id === self.value.res_id,
                            // Add booleans to check if this state is allowed as next
                            // state and clickable
                            is_dynamic: true,
                            next: self.record.data.allowed_next_state_ids
                                .res_ids.includes(info.id),
                            is_reachable: info.is_reachable,
                        }, info);
                    });
            } else {
                return this._super();
            }
        },
        // Handle click on statusbar: pop wizard if required
        _onClickStage: function (e) {
            var self = this;
            if (this.field.type === 'many2one' &&
                this.field.relation === 'dynamic.state'
            ) {
                self._rpc({
                    model: self.model,
                    method: 'button_specific_state',
                    args: [
                        self.res_id,
                        e.currentTarget.getAttribute("data-value"),
                    ],
                    context: self.record.getContext(self.recordParams),
                }).then(function (result) {
                    if (result && result.type === 'ir.actions.act_window') {
                        self.do_action(result, {
                            on_close: function () {
                                self.trigger_up('reload');
                            },
                        });
                    }
                });
            } else {
                return this._super(e);
            }
        },
    });
    return FieldStatusDynamic;
});
