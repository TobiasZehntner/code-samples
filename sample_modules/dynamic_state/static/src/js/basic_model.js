odoo.define('dynamic_state.reachable', function (require) {
    "use strict";

    var BasicModel = require('web.BasicModel');
    var DynamicStateReachable = BasicModel.include({

        /* eslint-disable no-param-reassign */
        _fetchSpecialMany2ones: function (record, fieldName, fieldInfo, fieldsToRead) {
            if (fieldName === 'dyn_state_id') {
                if (fieldsToRead === undefined) {
                    fieldsToRead = [];
                }
                fieldsToRead.push('is_reachable');
            }
            return this._super(record, fieldName, fieldInfo, fieldsToRead);
        },
        /* eslint-enable no-param-reassign */
    });

    return DynamicStateReachable;
});
