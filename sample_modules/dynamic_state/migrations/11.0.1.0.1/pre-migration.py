# © 2021 Antoine Honinckx
# © 2021 Niboo SRL (https://www.niboo.com/)
# License Other proprietary.


def migrate(cr, version):
    cr.execute(
        """
    ALTER TABLE dynamic_state
    DROP CONSTRAINT IF EXISTS dynamic_state_unique_workflow_code;
    ALTER TABLE dynamic_state_workflow
    DROP CONSTRAINT IF EXISTS dynamic_state_workflow_unique_workflow_model;
    """
    )
