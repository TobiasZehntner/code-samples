# © 2019 Niboo SPRL (https://www.niboo.com/)
# © 2019 Okia SPRL (https://okia.be)
# License Other proprietary.

{
    "name": "Dynamic States",
    "category": "Other",
    "summary": "Improved handling of record statuses",
    "website": "https://www.niboo.com",
    "version": "11.0.1.0.1",
    "license": "Other proprietary",
    "author": "Niboo",
    "description": """
        Dynamic States allow to set which states can be followed, which are
        reachable manually (by click on Statusbar) and which aren't.

        To add dynamic states to a model:
        Python: the model needs to inherit dynamic.state.base
        Views: the form view requires dyn_state_id AND allowed_next_state_ids
        (as invisible)
    """,
    "depends": ["web"],
    "data": [
        "security/ir.model.access.csv",
        "views/dynamic_state_workflow.xml",
        "views/dynamic_state.xml",
        "views/dynamic_state_history.xml",
        "views/webclient_templates.xml",
        "wizards/state_change_wizard.xml",
    ],
    "qweb": ["static/src/xml/base.xml"],
    "installable": True,
    "application": False,
}
