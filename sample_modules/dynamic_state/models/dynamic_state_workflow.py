# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, fields, models


class DynamicStateWorkflow(models.Model):
    _name = "dynamic.state.workflow"
    _description = "Status Workflow"

    model_id = fields.Many2one(
        "ir.model", "Model", required=True, domain=lambda self: self._get_model_domain()
    )

    state_ids = fields.One2many(
        "dynamic.state", inverse_name="workflow_id", string="Statuses"
    )
    is_always_ask_comment = fields.Boolean(
        "Always Ask Comment",
        help="On state change, always open pop-up to "
        "propose a comment, not just when it is "
        "required.",
    )

    _sql_constraints = [
        (
            "unique_model_id",
            "UNIQUE(model_id)",
            "A Workflow for this model already exists",
        ),
    ]

    @api.multi
    def name_get(self):
        result = []
        for workflow in self:
            result.append((workflow.id, _("%s Workflow") % workflow.model_id.name))
        return result

    def _get_available_models(self):
        """
        Get models that have dynamic.state.base inherited, searching by
        3 specific fields
        """
        specific_fields = self.env["ir.model.fields"].search(
            [
                (
                    "name",
                    "in",
                    ["state_code", "state_history_ids", "allowed_next_state_ids"],
                )
            ]
        )
        inherited_models = specific_fields.mapped("model_id")
        available_models = inherited_models.filtered(
            lambda m: m.model
            not in ("dynamic.state", "dynamic.state.base", "state.change.wizard")
        )
        return available_models

    def _get_model_domain(self):
        """
        Show only available models in M2O selection
        """
        available = self._get_available_models()
        used = (
            self.env["dynamic.state.workflow"]
            .search([("id", "!=", self.id)])
            .mapped("model_id")
        )

        return [("id", "in", available.ids), ("id", "not in", used.ids)]

    @api.multi
    @api.constrains("model_id")
    def check_model(self):
        """
        Check if dynamic.state.base has been inherited by the chosen model
        """
        for workflow in self:
            if workflow.model_id not in self._get_available_models():
                raise exceptions.ValidationError(
                    _(
                        "Missing inheritance: make sure the model %s (%s) "
                        "inherits the Dynamic State Base Model "
                        "(dynamic.state.base)"
                        % (workflow.model_id.name, workflow.model_id.model)
                    )
                )
