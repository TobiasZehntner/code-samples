# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, fields, models


class DynamicStateHistory(models.Model):
    _name = "dynamic.state.history"
    _description = "Status History"
    _order = "date DESC"

    res_id = fields.Integer("Record ID", required=True)
    model = fields.Char("Model", required=True, readonly=True)
    from_state_id = fields.Many2one(
        "dynamic.state", "From", required=True, readonly=True
    )
    to_state_id = fields.Many2one("dynamic.state", "To", required=True, readonly=True)
    date = fields.Datetime(
        "Date", default=fields.Datetime.now, required=True, readonly=True
    )
    user_id = fields.Many2one(
        "res.users",
        "User",
        default=lambda self: self.env.user.id,
        required=True,
        readonly=True,
    )
    comment = fields.Text("Comment", readonly=True)

    # Functional fields to save information of forced status change
    auto_force_model = fields.Char(
        "Model",
        readonly=True,
        help="The Model of the record that forced " "the Status Change",
    )
    auto_force_res_id = fields.Integer(
        "Record ID",
        readonly=True,
        help="The ID of the record that forced " "the Status Change",
    )

    @api.multi
    def name_get(self):
        result = []
        for history in self:
            result.append(
                (
                    history.id,
                    _("%s to %s")
                    % (history.from_state_id.name, history.to_state_id.name),
                )
            )
        return result
