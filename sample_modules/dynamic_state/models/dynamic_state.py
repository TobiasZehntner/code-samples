# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, fields, models


class DynamicState(models.Model):
    _name = "dynamic.state"
    _description = "Status"
    _order = "sequence, id"

    name = fields.Char("Name", required=True)
    code = fields.Char(
        "Code", required=True, help="Short Code for quick identification"
    )
    sequence = fields.Integer("Sequence", default=1, index=True)

    workflow_id = fields.Many2one("dynamic.state.workflow", "Workflow", required=True)
    model = fields.Char(related="workflow_id.model_id.model", readonly=True)

    next_state_id = fields.Many2one(
        "dynamic.state",
        "Next in Sequence",
        compute="_compute_next_state_id",
        store=True,
    )
    allowed_next_state_ids = fields.Many2many(
        comodel_name="dynamic.state",
        relation="current_to_next_state_rel",
        column1="current_state_id",
        column2="next_state_id",
        string="Allowed Next Statuses",
        help="The statuses that can follow this one",
    )

    is_arrival_comment_required = fields.Boolean(
        "Require Arrival Comment",
        help="Require comment if a record arrives " "at this status",
    )
    is_departure_comment_required = fields.Boolean(
        "Require Departure Comment",
        help="Require comment if a record " "departs from this status",
    )
    is_reachable = fields.Boolean(
        "Manually Reachable", default=True, help="Status can be chosen in Statusbar"
    )
    is_hidden = fields.Boolean(
        "Hide from Statusbar",
        help="Do not show this status in the "
        "Statusbar. Use this for non-standard "
        'statuses like "Cancelled". It can not be '
        "reached manually.",
    )
    is_terminal = fields.Boolean(
        string="Terminal Status", compute="_compute_is_terminal", store=True
    )

    _sql_constraints = [
        (
            "unique_code_workflow",
            "UNIQUE(code, workflow_id)",
            "Short Code already exists in this workflow",
        ),
    ]

    @api.depends("allowed_next_state_ids")
    def _compute_is_terminal(self):
        for state in self:
            state.is_terminal = not state.allowed_next_state_ids

    @api.multi
    @api.depends("sequence")
    def _compute_next_state_id(self):
        """
        Get the next state in sequence
        """
        for state in self:
            state.next_state_id = self.env["dynamic.state"].search(
                [
                    ("id", "!=", state.id),
                    ("model", "=", state.model),
                    ("sequence", ">=", state.sequence),
                ],
                order="sequence, id",
                limit=1,
            )

    @api.multi
    @api.constrains("is_reachable", "is_hidden")
    def _check_reachable_hidden(self):
        """
        Avoid users to define states that are both supposed to be hidden and
        reachable
        """
        for state in self:
            if state.is_reachable and state.is_hidden:
                raise exceptions.ValidationError(
                    _("A status cannot be reached manually if it is hidden")
                )
