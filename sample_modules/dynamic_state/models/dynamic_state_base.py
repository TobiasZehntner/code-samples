# © 2019 Tobias Zehntner
# © 2019 Niboo SPRL (https://www.niboo.com/)
# License Other proprietary.

from odoo import _, api, exceptions, fields, models


class DynamicStateBase(models.Model):
    """
    This model should be inherited by models you would like to add
    dynamic states to. The Form View will require dyn_state_id and
    allowed_next_state_ids (as invisible)
    """

    _name = "dynamic.state.base"
    _description = "Status Base"

    dyn_state_id = fields.Many2one(
        "dynamic.state",
        "Status",
        track_visibility="onchange",
        domain=lambda self: [("model", "=", self._name)],
        default=lambda self: self.env["dynamic.state"].search(
            [("model", "=", self._name)], order="sequence", limit=1
        ),
    )

    allowed_next_state_ids = fields.Many2many(
        related="dyn_state_id.allowed_next_state_ids", readonly=True
    )
    state_code = fields.Char(related="dyn_state_id.code", store=True, readonly=True)
    state_history_ids = fields.Many2many(
        "dynamic.state.history", string="Status History", readonly=True
    )

    def write(self, vals):
        """
        Only allow writing on dynamic_state_id with context
        force_dynamic_state=True to avoid writing directly.
        The function go_to_state(state) should be used for Status change.
        Exception is made for imports
        """
        if not self._context.get("import_file", False):

            if vals.get("state_code", False):
                if vals.get("dyn_state_id", False):
                    vals.pop("state_code")
                else:
                    raise exceptions.ValidationError(
                        _(
                            "You are not allowed to write on state_code directly, "
                            "please use go_to_state_by_code(code)"
                        )
                    )
            dyn_states = self.mapped("dyn_state_id")
            if len(dyn_states) == 1:
                if dyn_states.id == vals.get("dyn_state_id"):
                    vals.pop("dyn_state_id")
            if vals.get("dyn_state_id", False) and not self._context.get(
                "force_dynamic_state", False
            ):
                raise exceptions.ValidationError(
                    _(
                        "Please use go_to_state(state) to change the dynamic "
                        "state of a record."
                    )
                )
        return super(DynamicStateBase, self).write(vals)

    def button_specific_state(self, state_id):
        """
        Form view button: change status by click on Statusbar.
        Call Wizard if a comment is required.
        """
        self.ensure_one()
        to_state = self.env["dynamic.state"].browse(int(state_id))
        if (
            to_state.workflow_id.is_always_ask_comment
            or self.dyn_state_id.is_departure_comment_required
            or to_state.is_arrival_comment_required
        ):
            return self.open_state_change_wizard("specific", to_state)
        else:
            self.go_to_state(to_state)

    def button_next_state(self):
        """
        Form view button: go to next Status in sequence.
        Call Wizard if a comment is required.
        """
        self.ensure_one()
        to_state = self.dyn_state_id.next_state_id

        if not to_state:
            raise exceptions.ValidationError(
                _("No next Status available (%s: %s)")
                % (self._description, self.dyn_state_id.name)
            )
        if to_state not in (
            self.dyn_state_id + self.dyn_state_id.allowed_next_state_ids
        ):
            raise exceptions.ValidationError(
                _("Next Status in sequence is not allowed (%s: %s)")
                % (self._description, to_state.name)
            )

        if (
            to_state.workflow_id.is_always_ask_comment
            or self.dyn_state_id.is_departure_comment_required
            or to_state.is_arrival_comment_required
        ):
            return self.open_state_change_wizard("next", to_state)
        else:
            self.go_to_state(to_state)

    def button_previous_state(self):
        """
        Form view button: go to the previous state that the record had.
        Search in history to find the last state.
        Call Wizard if a comment is required.
        """
        self.ensure_one()

        latest_history = self.env["dynamic.state.history"].search(
            [("model", "=", self._name), ("res_id", "=", self.id)],
            order="date desc",
            limit=1,
        )
        if not latest_history:
            raise exceptions.ValidationError(
                _("No previous status available (%s: %s)")
                % (self._description, self.dyn_state_id.name)
            )
        to_state = latest_history.from_state_id

        if to_state and to_state not in (
            self.dyn_state_id + self.dyn_state_id.allowed_next_state_ids
        ):
            raise exceptions.ValidationError(
                _("The previous state is not allowed (%s: %s)")
                % (self._description, to_state.name)
            )

        if (
            to_state.workflow_id.is_always_ask_comment
            or self.dyn_state_id.is_departure_comment_required
            or to_state.is_arrival_comment_required
        ):
            return self.open_state_change_wizard("return", to_state)
        else:
            self.go_to_state(to_state)

    def button_choose_state(self):
        """
        Form view button: choose state from list of available states
        """
        self.ensure_one()
        return self.open_state_change_wizard()

    @api.multi
    def go_to_state_by_code(self, code, comment=False, disable_state_check=False):
        """
        Helper function to change record state by state_id.code
        """
        to_state = self.env["dynamic.state"].search(
            [("model", "=", self._name), ("code", "=", code)], limit=1
        )

        if not to_state:
            raise exceptions.ValidationError(
                _('No Status found for code "%s" on model %s') % (code, self._name)
            )

        for record in self:
            record.go_to_state(
                to_state, comment=comment, disable_state_check=disable_state_check
            )

    def go_to_state(
        self,
        to_state,
        comment=False,
        auto_force_record=False,
        disable_state_check=False,
    ):
        """
        Main function to change the Dynamic State of a record
        Checks if state is allowed and if comment is required and provided,
        posts the comment in the chatter of the record and creates a status
        history item.
        """
        self.ensure_one()
        if to_state.model != self._name:
            raise exceptions.ValidationError(
                _("Wrong Status for Model (Record Model: %s <> Status Model: %s)")
                % (self._name, to_state.model)
            )
        if (
            to_state
            not in (self.dyn_state_id + self.dyn_state_id.allowed_next_state_ids)
            and not disable_state_check
        ):
            raise exceptions.ValidationError(
                _("The chosen status is not allowed (%s: %s)")
                % (self._description, to_state.name)
            )
        if not comment and (
            self.dyn_state_id.is_departure_comment_required
            or to_state.is_arrival_comment_required
        ):
            raise exceptions.ValidationError(
                _("This status change requires a comment (%s: %s)")
                % (self._description, to_state.name)
            )

        # If the module has chatter, post a message
        if comment and "message_ids" in self.fields_get_keys():
            self.message_post(body=comment)

        history_dict = {
            "res_id": self.id,
            "model": self._name,
            "from_state_id": self.dyn_state_id.id,
            "to_state_id": to_state.id,
            "comment": comment,
        }
        if auto_force_record:
            history_dict["auto_force_model"] = auto_force_record._name
            history_dict["auto_force_res_id"] = auto_force_record.id

        history = self.env["dynamic.state.history"].create(history_dict)

        # module track_change requires to read then write with magic key 6
        histories = self.state_history_ids + history
        self.with_context(force_dynamic_state=True).write(
            {"state_history_ids": [(6, 0, histories.ids)], "dyn_state_id": to_state.id}
        )

    def open_state_change_wizard(self, wizard_type="choose", to_state=False):
        """
        Open State Change Wizard to allow choosing a state and/or adding a
        comment.
        :param wizard_type: Wizard options:
            - choose: choose the next state of those available
            - specifc: Go to a specific state
            - next: Go to next state in sequence
            - return: Return to previous state
        :type wizard_type: str
        :param to_state: If asking to change to a specific next state
        :type to_state: dynamic.state recordset
        """
        self.ensure_one()

        if wizard_type != "choose" and not to_state:
            raise exceptions.ValidationError(
                _("Missing To State for Wizard of non-choose type")
            )
        if wizard_type == "choose":
            # Do not allow choose wizard if there is no status to choose
            if not self.dyn_state_id.allowed_next_state_ids:
                raise exceptions.ValidationError(
                    _("The status %s cannot be followed by any other. (%s)")
                    % (self.dyn_state_id.name, self._description)
                )
            elif not self.dyn_state_id.allowed_next_state_ids.filtered(
                lambda s: s.is_reachable
            ):
                raise exceptions.ValidationError(
                    _(
                        "The status %s has no following statuses that are "
                        "manually reachable."
                    )
                    % self.dyn_state_id.name
                )

        state_change_wizard = self.env["state.change.wizard"].create(
            {
                "res_id": self.id,
                "model": self._name,
                "type": wizard_type,
                "from_state_id": self.dyn_state_id.id,
                "to_state_id": to_state and to_state.id or False,
            }
        )

        if wizard_type == "next":
            title = "Go to Next Status"
        elif wizard_type == "return":
            title = "Go to Previous Status"
        else:
            title = "Change Status"

        view = self.env.ref("dynamic_state.state_change_wizard_form")

        return {
            "name": title,
            "type": "ir.actions.act_window",
            "view_type": "form",
            "view_mode": "form",
            "view_id": view.id,
            "views": [[view.id, "form"]],
            "res_model": "state.change.wizard",
            "res_id": state_change_wizard.id,
            "target": "new",
        }
