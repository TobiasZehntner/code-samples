# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import SUPERUSER_ID, api


def migrate(cr, version):
    """
    Re-compute to ignore related fields
    """
    with api.Environment.manage():
        env = api.Environment(cr, SUPERUSER_ID, {})
        models = env["ir.model"].search([])
        models._compute_field_info()
        models._compute_model_children()
