# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import SUPERUSER_ID, api


def migrate(cr, version):
    """
    Re-compute for models that have specific ignore fields
    """
    with api.Environment.manage():
        env = api.Environment(cr, SUPERUSER_ID, {})
        models_to_update = [
            "hom.contract.legal.entity",
            "product.template",
            "product.product",
        ]
        models = env["ir.model"].search([("model", "in", models_to_update)])
        models._compute_field_info()
        models._compute_model_children()
