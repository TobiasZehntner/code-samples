odoo.define('track_change_message.FormController', function (require) {
    "use strict";

    var FormController = require('web.FormController');

    FormController.include({
        _update: function () {
            const res = this._super.apply(this, arguments);
            const self = this;
            const record = self.model.get(self.handle);

            // Save loading queue in sessionStorage to know when all calls have been returned
            // Display loading icon accordingly
            const loading_id = self.handle;
            const $loading_icon = $('body').find('#load_changes_icon');
            $loading_icon.removeClass('hidden');
            const loading_url = window.sessionStorage.getItem('loading_url') || '';
            // Remove menu_id from url since it changes between calls
            const current_url = new URL(window.location.href);
            const params = new URLSearchParams(current_url.hash);
            params.delete("menu_id");
            current_url.hash = params;
            const current_url_clean = current_url.toString();

            let loading_queue = [];
            if (loading_url === current_url_clean) {
                // Existing loading queue for this url
                var loading_queue_str = window.sessionStorage.getItem('loading_queue');
                loading_queue = JSON.parse(loading_queue_str);
            } else {
                // URL has changed, use empty queue
                window.sessionStorage.setItem('loading_url', current_url_clean);
            }
            loading_queue.push(loading_id);
            window.sessionStorage.setItem('loading_queue', JSON.stringify(loading_queue));

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '/highlight_form_changes/' + self.modelName,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({'jsonrpc': "2.0", 'method': "call", "params": {'res_ids': record.res_id}}),
                success: function (response) {
                    if ('error' in response) {
                        throw new Error("Error while highlighting track changes: " + response.error.data.message );
                    } else if (Object.keys(response.result).length !== 0 &&
                            Object.keys(response.result.changes).length !== 0) {
                        const changes = response.result.changes;
                        self.apply_track_changes(changes);
                    }
                    // Loading finished, remove from loading queue
                    loading_queue_str = window.sessionStorage.getItem('loading_queue') || '[]';
                    loading_queue = JSON.parse(loading_queue_str);
                    loading_queue = loading_queue.filter((item) => item !== loading_id);
                    if (loading_queue.length === 0) {
                        // Loading queue empty, hide loading icon
                        $loading_icon.addClass('hidden');
                    }
                    window.sessionStorage.setItem('loading_queue', JSON.stringify(loading_queue));
                },
            });

            return res;
        },
        apply_track_changes: function (changes) {
            var self = this;

            if (Object.keys(changes).length === 0) {
                if (self.$buttons !== undefined) {
                    const toolbar = self.$buttons.find('.tracking_tooltip');
                    toolbar.toggleClass('o_hidden', true);
                }
                return;
            }

            var users = [];
            var has_current_changes = false;

            if ('new_record' in changes) {
                self.$('.o_form_sheet').addClass('new-record');
                has_current_changes = true;
                var vals = changes.new_record;
                users.push(vals.user);
            } else {
                $.each(changes, function (field, lines) {
                    var selector = '[name=\''+field+'\']:not(.o_invisible_modifier)';
                    var elem = self.$(selector);

                    if (elem.length === 0) {
                        return;
                    }

                    $.each(lines, function (i, val) {
                        if ($.inArray(val.user, users) < 0 && val.user !== null) {
                            users.push(val.user);
                        }
                    });

                    var is_current_changeset = false;
                    if (lines.some(function (line) {
                        return line.is_current_changeset === true;
                    })) {
                        is_current_changeset = true;
                        has_current_changes = true;
                    }
                    self.render_labels(elem, is_current_changeset);
                    var field_type = lines[0].field_type;
                    if (field_type === 'many2many' &&
                        !elem.hasClass("o_field_many2manytags")
                    ) {
                        self.apply_track_changes_many2many(
                            elem, lines, is_current_changeset,
                        );
                    } else if (field_type === 'many2many' &&
                        elem.hasClass("o_field_many2manytags")
                    ) {
                        self.apply_track_changes_many2many_tag(
                            elem, lines, is_current_changeset,
                        );
                    } else {
                        self.apply_track_changes_other(
                            elem, lines, is_current_changeset,
                        );
                    }
                });
            }

            if (self.$buttons !== undefined) {
                var change_str = '';
                if ('new_record' in changes) {
                    change_str = 'This record has been created but not commited.';
                } else {
                    change_str = 'This record has been changed but not commited.';
                }
                var change_text = change_str + ' Change by: ' + users.join(', ');

                const toolbar = self.$buttons.find('.tracking_tooltip');
                toolbar.toggleClass('o_hidden', false);
                if (has_current_changes) {
                    toolbar.removeClass('alert-warning').addClass('alert-info');
                }
                toolbar.find("#change_text").text(change_text);
            }
        },
        render_labels (elem, is_current_changeset) {
            var label = elem.closest('tr').find('label');
            var tab_id = '#' + elem.parents('div.tab-pane').attr('id');
            var tab = elem.parents('.o_notebook').find(
                'a[href="' + tab_id +'"]',
            );
            var elements = label.add(tab);
            elements.addClass('track-change');
            if (is_current_changeset) {
                elements.addClass('current-changeset');
            }
        },
        apply_track_changes_many2many (elem, lines, is_current_changeset) {
            var main_container = $("<div/>", {
                "class": "alert alert-warning",
            });

            var warning_icon = $("<i/>", {
                "class": "fa fa-exclamation-triangle",
                "style": "padding-right: 0.5em",
            });
            var users = lines.filter(function (val) {
                return val.user !== null;
            });
            var user_text = '';
            if (users.length === 1 && is_current_changeset) {
                user_text = 'You';
            } else {
                user_text = users.length + " user";
                if (users.length !== 1) {
                    user_text += 's';
                }
            }

            var warning_text = $("<span/>").text(
                user_text + " changed this field. Click for details",
            );
            var info_container = $("<div/>").append(warning_icon).append(warning_text);
            var lines_container = $("<div/>");
            main_container.click(function () {
                lines_container.toggle();
                info_container.toggle();
            });
            main_container.append(info_container);

            var current_value; // eslint-disable-line init-declarations
            lines_container.hide();
            $.each(lines, function (i, val) {
                if (val.user === null) {
                    current_value = val;
                    return;
                }

                var line_container = $("<div/>");
                line_container.append($("<span/>").text(
                    "By " + val.user + " on " + val.date,
                ));
                line_container.append($("<br/>)"));
                line_container.append(val.display_value);
                lines_container.append(line_container);
            });

            if (current_value !== undefined) {
                var display_value = current_value.display_value;
                if (typeof display_value === 'string' && !display_value.trim()) {
                    display_value = 'None';
                }
                var line_container = $("<div/>");
                line_container.append($("<span/>").text("Current value:"));
                line_container.append($("<br/>)"));
                line_container.append(display_value);

                lines_container.append(line_container);
            }
            main_container.append(lines_container);
            if (is_current_changeset) {
                warning_icon.addClass("current-changeset");
                main_container.removeClass("alert-warning").addClass("alert-info");
            }
            elem.append(main_container);
        },
        apply_track_changes_many2many_tag (elem, lines, is_current_changeset) {
            var warning_icon = $("<i/>", {
                "class": "fa fa-exclamation-triangle track-change icon",
                "title": "Click for details",
            });
            var current_value; // eslint-disable-line init-declarations
            var lines_container = $("<div/>", {
                "class": "alert alert-warning alert-dismissible",
                "role": "alert",
                "style": "margin-left: 1em; padding: 0 15px 5px 15px;",
            }).hide();

            warning_icon.click(function () {
                lines_container.toggle();
            });
            elem.append(warning_icon);

            $.each(lines, function (i, val) {
                if (val.user === null) {
                    current_value = val;
                    return;
                }

                var line = $("<span/>", {"style": "padding: 5px 0"}).append(
                    "By " + val.user + " (" + val.date + "):<br/>" + val.display_value,
                );
                lines_container.append(line);
            });

            if (current_value !== undefined) {
                var display_value = current_value.display_value;
                if (typeof display_value === 'string' && !display_value.trim()) {
                    display_value = 'None';
                }
                var line = $("<span/>", {"style": "padding-left: 0.5em;"}).append(
                    "Current value: " + display_value,
                );
                lines_container.append(line);
            }

            if (is_current_changeset) {
                warning_icon.addClass("current-changeset");
                lines_container.removeClass("alert-warning").addClass("alert-info");
            }
            elem.append(lines_container);
        },
        apply_track_changes_other (elem, lines, is_current_changeset) {
            var current_value; // eslint-disable-line init-declarations
            var changes = "";
            var index = 1;
            $.each(lines, function (i, val) {
                if (val.user === null) {
                    current_value = val;
                    return;
                }

                changes += "* By " + val.user + " (" + val.date + "): " +
                    val.display_value;

                if (index < lines.length) {
                    changes += '\n';
                }

                index++;
            });

            if (current_value !== undefined) {
                var display_value = current_value.display_value;
                var is_empty_string = typeof display_value === 'string' &&
                    !display_value.trim();
                if (is_empty_string || display_value === false) {
                    display_value = 'None';
                }
                changes += "* Current value: " + display_value;
            }

            // Strip html from changes as it can't be displayed in 'title'
            var stripped_changes =
                $('<p>' + changes + '</p>').text().replace(/^\s*[\r\n]/gm, '');
            var warning_icon = $("<i/>", {
                "class": "fa fa-exclamation-triangle track-change icon",
                "title": stripped_changes,
            });
            if (is_current_changeset) {
                warning_icon.addClass('current-changeset');
            }
            if (elem.hasClass('o_statusbar_status')) {
                elem.append(warning_icon);
            } else {
                elem.after(warning_icon);
            }
        },
    });
});
