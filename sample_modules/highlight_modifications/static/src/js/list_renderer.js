odoo.define('track_change_messages.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');

    ListRenderer.include({
        _renderRow: function (record) {

            /* Add res_id to jquery object */
            var result = this._super.apply(this, arguments);
            result.data("res_id", record.res_id);
            return result;
        },
        _renderRows: function () {

            /* Add classes to rows that contain track-changes */
            var self = this;
            var $rows = this._super();
            self.highlight_rows($rows);
            return $rows;
        },
        // eslint-disable-next-line no-unused-vars
        _renderGroups: function (data, groupLevel) {

            /* Add classes to grouped rows that contain track-changes */
            var self = this;
            var result = this._super.apply(this, arguments);
            var $rows = [];
            for (const i in result) {
                const data_rows = result[i].find('tr.o_data_row').map(function () {
                    return $(this);
                }).get();
                $rows.push(...data_rows);
            }
            self.highlight_rows($rows);
            return result;
        },
        highlight_rows: function ($rows) {

            /* Get track-changes related to record and highlight row */
            const self = this;

            var res_ids = [];
            for (const i in $rows) {
                const res_id = $rows[i].data('res_id');
                if (typeof res_id !== 'undefined') {
                    res_ids.push(res_id);
                }
            }

            if (res_ids.length) {

                // Save loading queue in sessionStorage to know when all calls have been returned
                // Display loading icon accordingly
                const loading_id = self.state.id;
                const $loading_icon = $('body').find('#load_changes_icon');
                $loading_icon.removeClass('hidden');
                const loading_url = window.sessionStorage.getItem('loading_url') || '';
                let loading_queue = [];
                // Remove menu_id from url since it changes between calls
                const current_url = new URL(window.location.href);
                const params = new URLSearchParams(current_url.hash);
                params.delete("menu_id");
                current_url.hash = params;
                const current_url_clean = current_url.toString();

                if (loading_url === current_url_clean) {
                    // Existing loading queue for this url
                    const loading_queue_str = window.sessionStorage.getItem('loading_queue');
                    loading_queue = JSON.parse(loading_queue_str);
                } else {
                    // URL has changed, use empty queue
                    window.sessionStorage.setItem('loading_url', current_url_clean);
                }
                loading_queue.push(loading_id);
                window.sessionStorage.setItem('loading_queue', JSON.stringify(loading_queue));

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: '/highlight_list_changes/' + self.state.model,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({'jsonrpc': "2.0", 'method': "call", "params": {'res_ids': res_ids, 'context': this.state.context}}),
                    success: function (response) {
                        if ('error' in response) {
                            throw new Error("Error while highlighting track changes: " + response.error.data.message);
                        } else if (Object.keys(response.result).length !== 0 &&
                                    Object.keys(response.result.changes).length !== 0) {
                            const changes = response.result.changes;
                            let is_current = false;
                            for (const i in $rows) {
                                const res_id = $rows[i].data('res_id');
                                if (res_id in changes) {
                                    $rows[i].addClass('track-change');
                                    if (changes[res_id]) {
                                        is_current = true;
                                        $rows[i].addClass('current-changeset');
                                    }
                                }
                            }
                            self.render_labels($rows[0], is_current);
                        }
                        // Loading finished, remove from loading queue
                        const loading_queue_str = window.sessionStorage.getItem('loading_queue') || '[]';
                        loading_queue = JSON.parse(loading_queue_str);
                        loading_queue = loading_queue.filter((item) => item !== loading_id);
                        if (loading_queue.length === 0) {
                            // Loading queue empty, hide loading icon
                            $loading_icon.addClass('hidden');
                        }
                        window.sessionStorage.setItem('loading_queue', JSON.stringify(loading_queue));
                    },
                });

            }
        },
        render_labels (elem, is_current_changeset) {
            var tab_id = '#' + elem.parents('div.tab-pane').attr('id');
            var tab = elem.parents('.o_notebook').find('a[href="' + tab_id +'"]');
            tab.addClass('track-change');
            if (is_current_changeset) {
                tab.addClass('current-changeset');
            } else {
                tab.removeClass('current-changeset');
            }
        },
    });
});
