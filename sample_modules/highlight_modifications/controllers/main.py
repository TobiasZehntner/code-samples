# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import http


class TrackChangeHighlightController(http.Controller):
    @http.route(
        "/highlight_form_changes/<string:model>",
        auth="user",
        type="json",
        methods=["POST"],
        csrf=False,
    )
    def highlight_form_changes(self, model, **kw):
        return http.request.env[model].browse(kw.get("res_ids")).get_track_changes()

    @http.route(
        "/highlight_list_changes/<string:model>",
        auth="user",
        type="json",
        methods=["POST"],
        csrf=False,
    )
    def highlight_list_changes(self, model, **kw):
        return (
            http.request.env[model]
            .browse(kw.get("res_ids"))
            .get_related_track_changes()
        )
