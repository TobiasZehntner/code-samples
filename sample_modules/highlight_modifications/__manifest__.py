# © 2018 Niboo SPRL (https://www.niboo.com/)
# © 2018 Okia SPRL (https://okia.be)
# License Other proprietary.

{
    "name": "Track Changes",
    "category": "Other",
    "summary": "Display messages from Track Changes on formviews",
    "website": "https://www.niboo.com",
    "version": "11.0.1.0.3",
    "license": "Other proprietary",
    "author": "Niboo",
    "depends": ["track_change"],
    "data": ["views/assets.xml"],
    "qweb": ["static/src/xml/base.xml"],
    "installable": True,
    "application": False,
}
