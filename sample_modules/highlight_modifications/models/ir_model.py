# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

import json

from odoo import api, fields, models

# Used for O2Ms where the highlight is more confusing than helpful to the user
SPECIFIC_IGNORE_FIELDS_BY_MODEL = {
    "hom.contract.legal.entity": ["mts_settlement_ids"],
    "product.template": ["corridor_ids"],
    "product.product": ["corridor_ids"],
    "hom.service": ["corridor_ids"],
}


def _get_ignored_fields(model):
    """
    Return computed and other fields that are irrelevant for highlighting
    """
    computed_and_inversed_fields = [
        cp_field.name for cp_field in list(model._field_computed.keys())
    ]
    computed_fields = []
    for computed_field in computed_and_inversed_fields:
        field = model._fields.get(computed_field)
        if field.related or not field.inverse:
            computed_fields.append(computed_field)
    ignored_fields = (
        ["id"] + computed_fields + SPECIFIC_IGNORE_FIELDS_BY_MODEL.get(model._name, [])
    )
    return ignored_fields


class IrModel(models.Model):
    _inherit = "ir.model"

    field_info = fields.Char(
        compute="_compute_field_info",
        store=True,
        help="Stored information on Model Fields for efficient TrackChange highlighting",
    )
    model_children = fields.Char(
        compute="_compute_model_children",
        store=True,
        help="Stored information on Model Fields for efficient TrackChange highlighting",
    )

    @api.depends("field_id")
    def _compute_field_info(self):
        models_to_ignore = self.get_models_to_ignore()
        for ir_model in self:
            model_name = ir_model.model
            if ir_model.transient or model_name in models_to_ignore:
                ir_model.field_info = "ignored"
                continue
            try:
                model = self.env[model_name]
            except KeyError:
                # Model is not yet loaded. Will have to compute on first use
                ir_model.field_info = None
                continue

            ignore_fields = _get_ignored_fields(model)

            o2m_fields = []
            model_fields = model.fields_get(
                attributes=["type", "relation", "relation_field"]
            )
            direct_children = set()
            for field in model_fields:
                if field in ignore_fields:
                    continue
                elif model_fields[field]["type"] == "one2many":
                    related_model = model_fields[field]["relation"]
                    if related_model not in models_to_ignore:
                        inverse_field = model_fields[field].get("relation_field", False)
                        o2m_fields.append((field, related_model, inverse_field))
                        if related_model != model._name:
                            direct_children.add(related_model)

            field_info = {
                "o2m_fields": o2m_fields,
                "ignore_fields": ignore_fields,
                "direct_children": list(direct_children),
            }
            ir_model.field_info = json.dumps(field_info)

    @api.depends("field_info")
    def _compute_model_children(self):
        all_model_field_info = self.get_all_model_field_info()

        def _get_children(model, children):
            child_model_field_info = all_model_field_info[model]
            if not child_model_field_info or child_model_field_info == "ignored":
                return children
            for child in child_model_field_info["direct_children"]:
                if child != model and child not in children:
                    children.add(child)
                    children.update(_get_children(child, children))
            return children

        parent_models = set()
        for ir_model in self:
            model = ir_model.model
            model_field_info = all_model_field_info[model]
            if not model_field_info:
                ir_model.model_children = False
                continue
            elif model_field_info == "ignored":
                ir_model.model_children = "ignored"
                continue
            model_children = _get_children(model, set())
            ir_model.model_children = json.dumps(list(model_children))
            for other_model in all_model_field_info.keys():
                if (
                    model != other_model
                    and other_model not in self.mapped("model")
                    and all_model_field_info[other_model]
                    and all_model_field_info[other_model] != "ignored"
                    and other_model not in model_children
                    and model
                    in all_model_field_info[other_model].get("direct_children", set())
                ):
                    parent_models.add(other_model)

        if parent_models:
            parent_ir_models = self.env["ir.model"].search(
                [("model", "in", list(parent_models))]
            )
            parent_ir_models._compute_model_children()
