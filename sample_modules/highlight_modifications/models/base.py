# © 2018 Niboo SPRL (https://www.niboo.com/)
# © 2018 Okia SPRL (https://okia.be)
# License Other proprietary.

import json
import logging

from odoo import _, api, exceptions, models
from odoo.addons.track_change.constants import CONTEXT_IGNORE_CHANGESET

_logger = logging.getLogger(__name__)


class Base(models.AbstractModel):
    _inherit = "base"

    def get_all_model_field_info(self):
        """
        Get static field info (O2M relations and ignored fields) of all models
        :return: { model: field_info_dict, ... }
        :rtype: dict
        """
        self._cr.execute("SELECT model, field_info, model_children FROM ir_model;")
        model_field_info_raw = self._cr.fetchall()
        all_model_field_info = {}
        for res in model_field_info_raw:
            if res[1] == "ignored":
                all_model_field_info[res[0]] = res[1]
            elif res[1]:
                all_model_field_info[res[0]] = json.loads(res[1])
                if res[2] and res[2] != "ignored":
                    all_model_field_info[res[0]].update(
                        {"model_children": json.loads(res[2])}
                    )
            else:
                all_model_field_info[res[0]] = False
        return all_model_field_info

    def get_model_field_info(self, all_model_field_info, model):
        """
        Get field info of specific model. Compute if it's not yet stored.

        :return: {
            "o2m_fields": [
                (field_name, related_model, inverse_field),
                ...
            ],
            "ignore_fields": [
                ignored_field_name_1,
                ...
            ],
            "direct_children": [
                'sale.order.line',
                ...
            ]
        }
        :rtype: dict
        """
        model_field_info = all_model_field_info.get(model, False)
        if not model_field_info:
            # Not computed yet (model was not loaded at time of computation)
            ir_model = self.env["ir.model"].search([("model", "=", model)])
            ir_model.sudo()._compute_field_info()
            model_field_info = ir_model.sudo().field_info
            if model_field_info != "ignored":
                model_field_info = json.loads(model_field_info)
        return model_field_info

    @api.multi
    def get_track_changes(self):
        response = {"changes": {}}
        model = self._name

        all_model_field_info = self.get_all_model_field_info()
        model_field_info = self.get_model_field_info(all_model_field_info, model)

        if (
            not self
            or model in self.get_models_to_ignore()
            or model_field_info == "ignored"
        ):
            return response

        fields_to_ignore = model_field_info["ignore_fields"]
        current_changeset_id = self.get_changeset_id()[0]

        # Current changeset: show any create/write changes
        # Others: show 'write'-changes in pending_approval/provisioning_ready/processing
        action_query = (
            "(tchange.action = 'write' AND tchangeset.state in "
            "('pending_approval', 'provisioning_ready', 'processing'))"
        )
        if current_changeset_id:
            action_query = f"""
                        ({action_query} OR (
                            tchange.action in ('create', 'write')
                            AND tchange.changeset_id = {current_changeset_id})
                        )
                    """

        change_commit_query = f"""
            SELECT DISTINCT ON (tchange.key, users.id, tchangeset.id)
              tchangeset.id,
              users.id,
              partner.name,
              tchange.key,
              tchange.value,
              tchange.date,
              tchangeset.name
            FROM track_change tchange
              INNER JOIN track_changeset tchangeset
                ON tchange.changeset_id = tchangeset.id
              INNER JOIN res_users users
                on tchangeset.user_id = users.id
              INNER JOIN res_partner partner
                ON users.partner_id = partner.id
            WHERE tchange.res_id = %s
            AND tchange.model = %s
            AND (tchange.key NOT IN %s OR tchange.key ISNULL)
            AND {action_query}
            AND tchange.is_applied = FALSE
            ORDER BY tchangeset.id, tchange.key, users.id, tchange.id DESC;
        """
        self._cr.execute(
            change_commit_query, (self.id, self._name, tuple(fields_to_ignore))
        )
        changes = self._cr.fetchall()

        IrQwebFieldDatetime = self.env["ir.qweb.field.datetime"]

        fields_to_read = set()
        result = {}
        TrackChangeset = self.env["track.changeset"]
        for change in changes:
            changeset = TrackChangeset.sudo().browse(change[0])
            field_str = change[3]
            value = change[4]

            try:
                self.check_field_access_rights("read", [field_str])
            except exceptions.AccessError:
                continue

            if change[1] == self._uid:
                user = _("You (%s)") % change[6].replace("\n", "")
            else:
                user = change[2]

            # Change the timezone according the user
            date = change[5]
            tz_date = IrQwebFieldDatetime.value_to_html(date, {})

            if field_str is None and value is None:
                result["new_record"] = {"date": tz_date, "user": user}
                continue

            if field_str not in self._fields:
                continue

            if current_changeset_id == changeset.id:
                fields_to_read.add(field_str)

            field = self._fields[field_str]
            display_value = changeset.pretty_print_track_change(field, value, self.id)

            field_values = result.get(field_str, [])
            field_values.append(
                {
                    "value": value,
                    "date": tz_date,
                    "display_value": display_value,
                    "user": user,
                    "field_type": field.type,
                }
            )
            result[field_str] = field_values

        # Add default value if required
        if fields_to_read:
            values = self.with_context(**{CONTEXT_IGNORE_CHANGESET: True}).read(
                fields=list(fields_to_read)
            )[0]

            for key, value in values.items():
                field_values = result.get(key, [])

                field = self._fields[key]

                display_value = TrackChangeset._pprint_tchange(field, value, self.id)

                field_values.append(
                    {
                        "value": value,
                        "date": None,
                        "display_value": display_value,
                        "user": None,
                        "field_type": field.type,
                        "is_current_changeset": True,
                    }
                )
                result[key] = field_values
        response["changes"] = result
        return response

    def get_related_track_changes(self):  # noqa: C901
        """
        Highlights records in list view that have been modified in a changeset.
        From all records in self, it returns a dict with the IDs that contain changes
        {res_id: bool}. The boolean is true if the change belongs to the current
        changeset.
        Considered are changes to fields that are not computed, on the record and on all
        its nested One2Many connected records. (i.e. an Opportunity shows change if
        opportunity > corridor > cost.structure > cost.structure.line >
        non-computed-field has a change).
        :return: {"changes": {res_id: True, res_id: False}}
        :rtype: dict
        """
        result = {"changes": {}}
        ignored_models = self.get_models_to_ignore()
        if self._name in ignored_models:
            return result

        all_model_field_info = self.get_all_model_field_info()

        current_changeset_id = self.get_changeset_id()[0]

        # Get all non-applied track-changes
        # Other changesets: show write in pending_approval/provisioning_ready
        action_query = (
            "(tchange.action = 'write' AND tchangeset.state IN "
            "('pending_approval', 'provisioning_ready', 'processing'))"
        )
        if current_changeset_id:
            # Current changeset: show create/write in any changeset state
            action_query = f"""
                ({action_query} OR (
                    tchange.action IN ('create', 'write')
                    AND tchange.changeset_id = {current_changeset_id})
                )
            """
        track_change_query = f"""
                    SELECT tchange.model, tchange.key, tchange.res_id, array_to_string(array_agg(distinct
                    tchange.changeset_id),',') AS res_ids FROM track_change tchange
                    INNER JOIN track_changeset tchangeset ON tchange.changeset_id = tchangeset.id
                    WHERE tchange.is_applied = FALSE
                    AND {action_query}
                    GROUP BY tchange.model, tchange.res_id, tchange.key
                    ;
                """
        self._cr.execute(track_change_query)
        track_change_query_raw = self._cr.fetchall()

        # Create dict by model with list of all track-changes: {model: [(res_id, field, [changeset_ids])]}
        valid_track_changes = {}
        valid_models = set()
        for tc_res in track_change_query_raw:
            model = tc_res[0]
            if not valid_track_changes.get(model):
                valid_models.add(model)
                valid_track_changes[model] = []
            valid_track_changes[model].append(
                (
                    tc_res[1],
                    tc_res[2],
                    [int(changeset_id) for changeset_id in tc_res[3].split(",")],
                )
            )

        def _find_related_changes(records, ignore_field=None):
            """
            If a record or its related records (recursive)
            - has been changed in this changeset: return 'current'
            - has been changed in another changeset: return 'other'
            - has not been changed: return False
            - If the record's model or the related O2M models are not present in valid track changes: return 'none'

            :param records: Records to be checked for changes
            :type records: recordset
            :param ignore_field: inverse field of O2M field that should be ignored on related model
            :type ignore_field: str
            :return: 'current', 'other', 'none', False
            :rtype: str
            """

            has_valid_relations = False
            search_result = False

            # 1. Get track changes that are relevant for this model
            model = records[0]._name
            model_field_info = self.get_model_field_info(all_model_field_info, model)
            if model_field_info == "ignored":
                # For good measure.
                # We return at the start of main function if model is ignored.
                # On recursive, we only search if model has track-changes, so the model shouldn't be ignored by default
                return search_result

            if not all_model_field_info.get(model):
                # Model info has been newly computed, add it
                all_model_field_info[model] = model_field_info

            # 2. Check if records are in track changes
            model_track_changes = valid_track_changes.get(model, [])
            if model_track_changes:
                has_valid_relations = True
                model_ignore_fields = model_field_info["ignore_fields"]
                if ignore_field:
                    model_ignore_fields.append(ignore_field)
                for model_track_change in model_track_changes:
                    if model_track_change[0] in model_ignore_fields:
                        continue
                    elif model_track_change[1] in records.ids:
                        if current_changeset_id in model_track_change[2]:
                            search_result = "current"
                            break
                        else:
                            search_result = "other"

            # 3. Check if record has O2M relations with any model in the relevant track changes
            if search_result != "current":
                for o2m_field_tuple in model_field_info.get("o2m_fields", []):
                    o2m_field = o2m_field_tuple[0]
                    related_model = o2m_field_tuple[1]
                    inverse_field = o2m_field_tuple[2]
                    related_model_field_info = all_model_field_info[related_model]
                    if not related_model_field_info:
                        related_model_field_info = self.get_model_field_info(
                            all_model_field_info, related_model
                        )
                        all_model_field_info[related_model] = related_model_field_info
                    children_models = related_model_field_info.get("model_children", [])

                    if related_model in valid_models or any(
                        [child_model in valid_models for child_model in children_models]
                    ):
                        # Only continue if any children models are in valid track changes
                        has_valid_relations = True
                        related_records = records.mapped(o2m_field)
                        if related_records:
                            if model == related_model:
                                # O2M pointing at same model: check IDs but do not search recursively
                                for model_track_change in model_track_changes:
                                    if model_track_change[0] in model_ignore_fields:
                                        continue
                                    elif model_track_change[1] in related_records.ids:
                                        if (
                                            current_changeset_id
                                            in model_track_change[2]
                                        ):
                                            # Found current changeset change, search no further
                                            search_result = "current"
                                            break
                                        else:
                                            search_result = "other"
                                if search_result == "current":
                                    break
                            else:
                                # Recursive search on related records
                                search_related_result = _find_related_changes(
                                    related_records, ignore_field=inverse_field
                                )
                                if search_related_result:
                                    if search_related_result == "current":
                                        # Found current changeset change, overwrite search_result and search no further
                                        search_result = search_related_result
                                        break
                                    elif (
                                        search_related_result == "other"
                                        and not search_result
                                    ):
                                        # Found related changes in other changeset
                                        search_result = search_related_result
            if not has_valid_relations:
                search_result = "none"
            return search_result

        changes = {}
        for record in self:
            related_result = _find_related_changes(record)
            if related_result == "none":
                # No valid relations, no need to check every record
                break
            elif related_result == "current":
                changes[record.id] = True
            elif related_result == "other":
                changes[record.id] = False
        result["changes"] = changes
        return result

    def get_related_records(
        self, all_model_field_info, related_records_by_model, ignore_field=None
    ):
        """
        Similar method than above, but here we read all related records, while the other we stop when found
        """

        def _add_record_ids(records, related_records_by_model):
            if not records.ids:
                # Records are newIDs and will not yet feature in track change
                return related_records_by_model
            record_model = records._name
            if not related_records_by_model.get(record_model):
                related_records_by_model[record_model] = []
            related_records_by_model[record_model] += records.ids
            return related_records_by_model

        model = self._name
        model_field_info = self.get_model_field_info(all_model_field_info, model)
        if model_field_info == "ignored":
            return

        related_records_by_model = _add_record_ids(self, related_records_by_model)
        if not all_model_field_info.get(model):
            # Model info has been newly computed, add it
            all_model_field_info[model] = model_field_info

        for o2m_field_tuple in model_field_info.get("o2m_fields", []):
            o2m_field = o2m_field_tuple[0]
            if o2m_field == ignore_field:
                continue
            related_model = o2m_field_tuple[1]
            inverse_field = o2m_field_tuple[2]
            related_model_field_info = all_model_field_info[related_model]
            if not related_model_field_info:
                related_model_field_info = self.get_model_field_info(
                    all_model_field_info, related_model
                )
                all_model_field_info[related_model] = related_model_field_info

            related_records = self.mapped(o2m_field)
            if related_records:
                if model == related_model:
                    # O2M pointing at same model: add IDs but do not search recursively
                    related_records_by_model = _add_record_ids(
                        related_records, related_records_by_model
                    )
                else:
                    # Recursive search on related records
                    related_records_by_model = related_records.get_related_records(
                        all_model_field_info,
                        related_records_by_model,
                        ignore_field=inverse_field,
                    )
        return related_records_by_model

    def get_related_changeset_ids(self):
        """
        Find changesets that have modified the current or its related records.
        Used in compute on partner, product and corridor
        """
        self.ensure_one()
        all_model_field_info = self.get_all_model_field_info()
        related_records_by_model = self.get_related_records(all_model_field_info, {})

        where_clauses = []
        for key, value in related_records_by_model.items():
            if len(value) == 1:
                value_clause = f"= {value[0]}"
            else:
                value_clause = f"IN {str(tuple(value))}"
            where_clauses.append(f"(tc.model = '{key}' AND tc.res_id {value_clause})")

        query = f"""
                            SELECT tc.changeset_id
                            FROM track_change tc
                            LEFT JOIN track_changeset tcset ON tc.changeset_id = tcset.id
                            WHERE tcset.state NOT IN ('cancelled', 'failed', 'not_approved', 'new', 'draft') AND
                            ({" OR ".join(where_clauses)})
                            ORDER BY tcset.approval_date DESC NULLS FIRST, tcset.date_end DESC NULLS FIRST
                            """
        self._cr.execute(query)
        changeset_ids = [x[0] for x in self._cr.fetchall()]
        return changeset_ids
