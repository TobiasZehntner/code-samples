# © 2024 Tobias Zehntner
# © 2024 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

import odoo


def onwrite(*args):
    """
    Decorates functions that should be run after a field is written on.
    Invoked on the records on which one of the named fields has been modified.
    Called after constrains checks
    Note: not called on create. Use @api.oncreatewrite to ensure also call on create

    Example:
        @api.onwrite('partner_id', 'currency_id')
        def reset_amount(self):
            for record in self:
                record.amount = 0

    `@onwrite` only supports simple field names, dotted names
        (fields of relational fields e.g. ``partner_id.customer``) are not
        supported and will be ignored
    """
    return odoo.api.attrsetter("_onwrite", args)


def oncreate(*args):
    """
    Decorates functions that should be run after a record is created.
    If fields are passed in the decorator arguments, it is only triggered if that field is in the create values.
    If no fields declared, the function is called after every create.
    Note: only fields present in a view will be written on when record is created

    Example:
        # Call function if partner_id is present in create values
        @api.oncreate('partner_id')
        def set_amount(self):
            for record in self:
                record.amount = 100

        # Call function at any create of this model
        @api.oncreate()
        def set_amount(self):
            for record in self:
                record.amount = 100

    `@oncreate` only supports simple field names, dotted names
        (fields of relational fields e.g. ``partner_id.customer``) are not
        supported and will be ignored
    """
    return odoo.api.attrsetter("_oncreate", args)


def oncreatewrite(*args):
    """
    Decorates functions that should be run after a record is created AND is written on.
    Invoked on the records on which one of the named fields has been modified or used in create values.
    Note: only fields present in a view will be written on when record is created, and only fields in create values
    will trigger the function

    Example:
        @api.oncreatewrite('partner_id', 'currency_id')
        def reset_amount(self):
            for record in self:
                record.amount = 0

    `@oncreatewrite` only supports simple field names, dotted names
        (fields of relational fields e.g. ``partner_id.customer``) are not
        supported and will be ignored
    """
    return odoo.api.attrsetter("_oncreatewrite", args)


odoo.api.onwrite = onwrite
odoo.api.oncreate = oncreate
odoo.api.oncreatewrite = oncreatewrite
