# © 2024 Tobias Zehntner
# © 2024 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

import logging
from inspect import getmembers

from odoo import api, models

_logger = logging.getLogger(__name__)


class Base(models.AbstractModel):
    _inherit = "base"

    @api.model_create_multi
    def create(self, vals_list):
        res = super().create(vals_list)
        res.run_oncreate_methods(vals_list)
        res.run_oncreatewrite_methods(vals_list)
        return res

    def write(self, vals):
        res = super().write(vals)
        self.run_onwrite_methods(vals)
        self.run_oncreatewrite_methods(vals)
        return res

    def run_oncreate_methods(self, vals_list):
        field_names = set().union(*vals_list)
        for method in self._oncreate_methods:
            # Field arguments on oncreate decorator are optional
            if not method._oncreate or set(method._oncreate) & field_names:
                method(self)

    def run_onwrite_methods(self, vals):
        field_names = vals.keys()
        for method in self._onwrite_methods:
            if set(method._onwrite) & field_names:
                method(self)

    def run_oncreatewrite_methods(self, vals_list):
        field_names = set().union(*vals_list)
        for method in self._oncreatewrite_methods:
            if set(method._oncreatewrite) & field_names:
                method(self)

    @property
    def _oncreate_methods(self):
        """
        Return a list of methods implementing Python oncreate functions.
        """

        def is_oncreate(func):
            return callable(func) and hasattr(func, "_oncreate")

        cls = type(self)
        methods = []
        for attr, func in getmembers(cls, is_oncreate):
            for name in func._oncreate:
                field = cls._fields.get(name)
                if not field:
                    _logger.warning(
                        f"method : @oncreate parameter {name} is not a field name"
                    )
                elif not (field.store or field.inverse or field.inherited):
                    _logger.warning(
                        f"method {cls._name}.{attr}: @oncreate parameter {name} is not createable"
                    )
            methods.append(func)

        cls._oncreate_methods = methods
        return methods

    @property
    def _onwrite_methods(self):
        """
        Return a list of methods implementing Python onwrite functions.
        """

        def is_onwrite(func):
            return callable(func) and hasattr(func, "_onwrite")

        cls = type(self)
        methods = []
        for attr, func in getmembers(cls, is_onwrite):
            for name in func._onwrite:
                field = cls._fields.get(name)
                if not field:
                    _logger.warning(
                        f"method : @onwrite parameter {name} is not a field name"
                    )
                elif not (field.store or field.inverse or field.inherited):
                    _logger.warning(
                        f"method {cls._name}.{attr}: @onwrite parameter {name} is not writeable"
                    )
            methods.append(func)

        cls._onwrite_methods = methods
        return methods

    @property
    def _oncreatewrite_methods(self):
        """
        Return a list of methods implementing Python oncreatewrite functions.
        """

        def is_oncreatewrite(func):
            return callable(func) and hasattr(func, "_oncreatewrite")

        cls = type(self)
        methods = []
        for attr, func in getmembers(cls, is_oncreatewrite):
            for name in func._oncreatewrite:
                field = cls._fields.get(name)
                if not field:
                    _logger.warning(
                        f"method : @oncreatewrite parameter {name} is not a field name"
                    )
                elif not (field.store or field.inverse or field.inherited):
                    _logger.warning(
                        f"method {cls._name}.{attr}: @oncreatewrite parameter {name} is not createwriteable"
                    )
            methods.append(func)

        cls._oncreatewrite_methods = methods
        return methods
