# © 2024 Tobias Zehntner
# © 2024 Niboo SRL (https://www.niboo.com/)
# License Other proprietary.

{
    "name": "API Create/Write Decorator",
    "category": "base",
    "summary": "Adds extra decorators for functions",
    "website": "https://www.niboo.com/",
    "version": "17.0.1.0.0",
    "license": "Other proprietary",
    "description": """
Enables extra decorator on functions

@api.onwrite(<field_name>)
The function will be called after the field(s) is written on
Similar to a constrains but called entirely after the write.

@api.oncreate(optional <field_name>)
The function will be called if the field_name is present in the create values, or if no field is passed.
Called after the creation of the record.

@api.oncreatewrite(<field_name>)
Similar as above. Called both after create and write.

Note: Needs to be loaded server-wide. etc/odoo.cfg: server_wide_modules = web,api_onwrite_decorator
    """,
    "author": "Niboo",
    "installable": True,
    "application": False,
}
