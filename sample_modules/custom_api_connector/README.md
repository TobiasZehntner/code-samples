# Dun & Bradstreet Connector
## Configuration
### Manage your licences
- General Settings > Contacts > Dun & Bradstreet Data > License


## Features:
### Clean contacts
- General Settings > Contacts > Dun & Bradstreet Data > Clean my Contacts

Send all your company contacts to D&B and have them matched to their data. Your contacts will be updated with the latest
data and duplicates will be archived. A report with the affected contacts will be sent to the Administrator's email once the process is finished.
