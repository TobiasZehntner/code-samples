# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from . import dnb_api_connector  # pylint: disable=no-name-in-module


class NibooConnector:
    # TODO replace direct calls to dnb_api with calls to Niboo
    dnb_api = False

    def __new__(cls, *args, **kw):  # pylint: disable=unused-argument
        # Python singleton
        if not hasattr(cls, "instance"):
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self, user, password, limit):
        self.dnb_api = dnb_api_connector.DnbApiConnector(user, password, limit)

    def set_token(self):
        return self.dnb_api.set_token()

    def search_match(self, vals):
        return self.dnb_api.search_match(vals)

    def search_leads(self, vals):
        return self.dnb_api.search_leads(vals)

    def get_industries(self):
        return self.dnb_api.get_industry_codes()

    def get_datablock_info(self, duns, block_ids):
        return self.dnb_api.get_datablock_info(duns, block_ids)
