# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

import base64
import json
import logging

import requests

STANDALONE_PARAMETERS = [
    "duns",
    "name",
    "phone",
    "website",
    "street",
    "vat",
    "company_registry",
    "dnb_industry_ids",
    "dnb_industry_codes",
]

COMMON_SEARCH_FIELD_MAP = {
    "duns": "duns",
    "country_code": "countryISOAlpha2Code",
    "phone": "telephoneNumber",
    "city": "addressLocality",
    "state_name": "addressRegion",
    "street": "streetAddressLine1",
    "zip": "postalCode",
    "zip_list": "postalCodesList",
    "pageSize": "pageSize",
    "pageNumber": "pageNumber",
}
# Docs: https://directplus.documentation.dnb.com/openAPI.html?apiID=IDRCleanseMatch
LEAD_SEARCH_FIELD_MAP = {
    **COMMON_SEARCH_FIELD_MAP,
    "name": "searchTerm",
    "website": "domain",
    "is_fortune_1000": "isFortune1000Listed",
}
# Docs: https://directplus.documentation.dnb.com/openAPI.html?apiID=searchCriteria
MATCH_SEARCH_FIELD_MAP = {
    **COMMON_SEARCH_FIELD_MAP,
    "name": "name",
    "street2": "streetAddressLine2",
    "email": "email",
    "website": "url",
    "limit": "candidateMaximumQuantity",
    "vat": "registrationNumber",
    "company_registry": "registrationNumber",
}

# TODO Add support for any expected languages
# TODO add dnb language code in order to get info in correct language
LANGUAGE_CODE_MAP = {
    "Belgian Dutch": "nl_BE",
    "Belgian German": "de",
    "Belgian French": "fr_BE",
    "Dutch": "nl_NL",
    "French": "fr_FR",
    "US English": "en_US",
    "English": "en_US",
    "Canadian English": "en_CA",
}

API_ENDPOINTS = {
    "access_token": ("POST", "https://plus.dnb.com/v2/token"),
    "search_leads": ("POST", "https://plus.dnb.com/v1/search/criteria"),
    "search_match": ("GET", "https://plus.dnb.com/v1/match/cleanseMatch"),
    "company_info": ("GET", "https://plus.dnb.com/v1/data/duns"),
    "industry_codes": ("GET", "https://plus.dnb.com/v1/referenceData/category?id=3599"),
}
# TODO calls using these data blocks should be in the corresponding modules (for we need to check access for client)
DATA_BLOCKS = {
    "company_info": "companyinfo_L2_v1",
    "contact_info": "principalscontacts_L1_v2",
    "credit_rating": "financialstrengthinsight_L2_v1",
}
EMPTY_VALUES = {
    "street": ["Adresse non disponible dans le Banque Carrefour 0"],
    "city": ["Inconnu"],
    "zip": ["0000"],
}
REGISTRATION_NUMBER_TYPE_CODES = {
    # Available registration number types via API call https://plus.dnb.com/v1/referenceData/category?id=7
    # Currently we allow NL, BE, LU
    "company_registry": [6256, 800, 3827],
    "vat": [6273, 99, 480],
}


def translate_to_dnb_vals(odoo_vals, endpoint):
    dnb_values = {}
    field_map = (
        MATCH_SEARCH_FIELD_MAP if endpoint == "search_match" else LEAD_SEARCH_FIELD_MAP
    )

    def add_range_value(odoo_key):
        dnb_key = "yearlyRevenue" if "revenue" in odoo_key else "numberOfEmployees"
        value_key = "minimumValue" if "min_" in odoo_key else "maximumValue"
        if not dnb_values.get(dnb_key):
            dnb_values[dnb_key] = {}
        dnb_values[dnb_key][value_key] = odoo_vals[odoo_key]

    for key in odoo_vals.keys():
        if key in ["min_revenue", "max_revenue", "min_employees", "max_employees"]:
            add_range_value(key)
        elif endpoint == "search_leads" and key in ("vat", "company_registry"):
            if not dnb_values.get("registrationNumbers"):
                dnb_values["registrationNumbers"] = []
            dnb_values["registrationNumbers"].append(odoo_vals[key])
        elif key == "dnb_industry_codes":
            dnb_values["industryCodes"] = [
                {"typeDnbCode": 3599, "code": odoo_vals[key]}
            ]
        elif key in field_map:
            dnb_values[field_map[key]] = odoo_vals[key]
    return dnb_values


def format_amount(value):
    value = f"{int(value):,}"
    return value


def get_industry_codes(dnb_vals):
    industry_codes = []
    for industry_code in dnb_vals.get("industryCodes", []):
        # Inconsistent spelling with capital letters in lead vs match api results
        dnb_code = industry_code.get("typeDnBCode") or industry_code.get("typeDnbCode")
        if dnb_code == 3599:
            # Use DNB codes (Standard Industrial Classification), full 8-digit size (fill with zeroes)
            industry_codes.append(industry_code["code"].ljust(8, "0"))
    if dnb_vals.get("primaryIndustryCode"):
        # Primary industry code is in US SIC but should match DNB Industries
        industry_codes.append(dnb_vals["primaryIndustryCode"]["usSicV4"].ljust(8, "0"))
    return industry_codes


def get_odoo_vals(dnb_vals):
    address = dnb_vals.get("primaryAddress", {})
    vals = {
        "duns": dnb_vals["duns"],
        "name": dnb_vals.get("primaryName"),
        "is_fortune_1000": dnb_vals.get("isFortune1000Listed"),
        "street": address.get("streetAddress", {}).get("line1"),
        "street2": address.get("streetAddress", {}).get("line2"),
        "country_code": address.get("addressCountry", {}).get("isoAlpha2Code"),
        "state_code": address.get("addressRegion", {}).get("abbreviatedName"),
        "state_name": address.get("addressRegion", {}).get("name"),
        "city": address.get("addressLocality", {}).get("name"),
        "zip": address.get("postalCode"),
        "creation_date": dnb_vals.get("startDate"),
        "linked_duns": [],
        # TODO only to use in wizard results, not data
    }
    assessment = dnb_vals.get("dnbAssessment")
    if assessment:
        if assessment.get("creditLimitRecommendation"):
            max_limit = assessment.get("creditLimitRecommendation").get(
                "maximumRecommendedLimit"
            )
            vals["maximum_credit_recommendation_currency_code"] = max_limit.get(
                "currency"
            )
            vals["maximum_credit_recommendation"] = max_limit.get("value")

        if assessment.get("standardRating"):
            vals["rating"] = assessment.get("standardRating").get("rating")
            vals["financial_rating_date"] = assessment.get("standardRating").get(
                "scoreDate"
            )
            vals["financial_strength"] = assessment.get("standardRating").get(
                "financialStrength"
            )
            vals["risk_segment"] = assessment.get("standardRating").get("riskSegment")
            vals["rating_reason"] = {}
            for reason in assessment.get("standardRating", {}).get("ratingReason", []):
                vals["rating_reason"][reason.get("dnbCode")] = reason.get("description")
        if assessment.get("failureScore"):
            vals["national_percentile"] = assessment.get("failureScore").get(
                "nationalPercentile"
            )
    return vals


def get_website_address(dnb_vals):
    value = False
    if dnb_vals.get("websiteAddress"):
        # Company search
        for website in dnb_vals.get("websiteAddress", []):
            if isinstance(website, str):
                # Match search
                value = website
            elif isinstance(website, dict):
                # Company search
                value = website.get("url") or website.get("domainName")
            # Get first website found
            break
    elif dnb_vals.get("domain"):
        value = dnb_vals.get("domain")
    return value


def get_phone(dnb_vals):
    value = False
    for phone_data in dnb_vals.get("telephone", []):
        # Take first phone number if several
        phone = phone_data.get("telephoneNumber")
        if phone_data.get("isdCode"):
            phone = f"+{phone_data.get('isdCode')}{phone}"
        value = phone
        break
    return value


def get_email(dnb_vals):
    if dnb_vals.get("certifiedEmail"):
        return dnb_vals.get("certifiedEmail")
    if dnb_vals.get("email"):
        # Use first email available
        return dnb_vals["email"][0]


def get_reg_numbers(dnb_vals, odoo_vals):
    for reg_num in dnb_vals.get("registrationNumbers", []):
        if (
            reg_num.get("typeDnBCode")
            in REGISTRATION_NUMBER_TYPE_CODES["company_registry"]
        ):
            odoo_vals["company_registry"] = reg_num.get("registrationNumber")
        elif reg_num.get("typeDnBCode") in REGISTRATION_NUMBER_TYPE_CODES["vat"]:
            odoo_vals["vat"] = reg_num.get("registrationNumber")
    return odoo_vals


def get_yearly_revenues(dnb_vals, odoo_vals):
    for revenue_data in dnb_vals.get("financials", []):
        for values in revenue_data.get("yearlyRevenue", []):
            if values.get("currency") == "USD":
                odoo_vals["yearly_revenue"] = format_amount(values.get("value"))
                break

    return odoo_vals


def get_linked_duns(dnb_vals):
    company_link_duns = []
    if dnb_vals.get("corporateLinkage", {}).get("globalUltimate"):
        company_link_duns.append(
            {
                "duns": dnb_vals["corporateLinkage"]["globalUltimate"]["duns"],
                "name": dnb_vals["corporateLinkage"]["globalUltimate"]["primaryName"],
                "is_incomplete": True,
            }
        )
    if dnb_vals.get("corporateLinkage", {}).get("parent"):
        company_link_duns.append(
            {
                "duns": dnb_vals["corporateLinkage"]["parent"]["duns"],
                "name": dnb_vals["corporateLinkage"]["parent"]["primaryName"],
                "is_incomplete": True,
            }
        )
    return company_link_duns


def get_local_language_address(dnb_vals):
    local_lang_addresses = []
    for local_lang_address in dnb_vals.get("multilingualPrimaryAddress", []):
        local_lang_addresses.append(
            {
                "lang_description": local_lang_address.get("language", {}).get(
                    "description"
                ),
                "writing_script": local_lang_address.get("writingScript", {}).get(
                    "name"
                ),
                "street": local_lang_address.get("streetAddress", {}).get("line1"),
                "street2": local_lang_address.get("streetAddress", {}).get("line2"),
                "street_name": local_lang_address.get("streetName"),
                "street_number": local_lang_address.get("streetNumber"),
                "zipcode": local_lang_address.get("postalCode"),
                "locality": local_lang_address.get("addressLocality", {}).get("name"),
                "region": local_lang_address.get("addressRegion", {}).get("name"),
                "continental_region": local_lang_address.get(
                    "continentalRegion", {}
                ).get("name"),
                "minor_town_name": local_lang_address.get("minorTownName"),
                "country_code": local_lang_address.get("addressCountry", {}).get(
                    "code"
                ),
            }
        )

    return local_lang_addresses


def get_registration_numbers(dnb_vals):
    registration_numbers = []

    for reg_number_data in dnb_vals.get("registrationNumbers", []):
        class_info = reg_number_data.get("registrationNumberClass", {})
        registration_numbers.append(
            {
                "registration_class_dnb_code": class_info.get("dnbCode"),
                "registration_class_description": class_info.get("description"),
                "registration_type_dnb_code": reg_number_data.get("typeDnBCode"),
                "registration_type_description": reg_number_data.get("typeDescription"),
                "is_preferred": reg_number_data.get("isPreferredRegistrationNumber"),
                "number": reg_number_data.get("registrationNumber"),
                "location": reg_number_data.get("registrationLocation"),
            }
        )

    return registration_numbers


def get_activities(dnb_vals):
    activities = []
    for activity_data in dnb_vals.get("unspscCodes", []):
        activities.append(
            {
                "code": activity_data.get("code"),
                "description": activity_data.get("description"),
            }
        )
    return activities


def get_is_oob(dnb_vals):
    # 403 seems to be the code "out of business". TODO: check if there are more code for that
    return (
        dnb_vals.get("dunsControlStatus", {}).get("operatingStatus", {}).get("dnbCode")
        == 403
    )


def translate_to_odoo_vals(dnb_vals, get_grade=False):
    odoo_vals = get_odoo_vals(dnb_vals)

    if get_grade:
        odoo_vals.update(
            {
                "confidence_code": dnb_vals.get("confidence_code"),
                "match_grade": dnb_vals.get("match_grade"),
                "name_match_score": dnb_vals.get("name_match_score"),
            }
        )

    odoo_vals["website"] = get_website_address(dnb_vals)
    odoo_vals["phone"] = get_phone(dnb_vals)
    odoo_vals["email"] = get_email(dnb_vals)
    odoo_vals["industry_codes"] = get_industry_codes(dnb_vals)
    odoo_vals["local_language_address"] = get_local_language_address(dnb_vals)
    odoo_vals["registration_numbers"] = get_registration_numbers(dnb_vals)
    odoo_vals["activities"] = get_activities(dnb_vals)
    odoo_vals["is_out_of_business"] = get_is_oob(dnb_vals)

    # There are 6 types of employee numbers (Entire group, employees total, employees here etc), take the max
    if dnb_vals.get("numberOfEmployees"):
        odoo_vals["num_employees"] = max(
            data["value"] for data in dnb_vals.get("numberOfEmployees", [])
        )

    odoo_vals = get_reg_numbers(dnb_vals, odoo_vals)
    odoo_vals = get_yearly_revenues(dnb_vals, odoo_vals)
    odoo_vals["linked_duns"] = get_linked_duns(dnb_vals)

    if dnb_vals.get("preferredLanguage"):
        lang_description = dnb_vals.get("preferredLanguage", {}).get("description")
        odoo_vals["lang_code"] = LANGUAGE_CODE_MAP.get(lang_description)

    # Clean empty vals
    for key, value in odoo_vals.items():
        if value in EMPTY_VALUES.get(key, []):
            # D&B values can be placeholder texts like "unknown": Replace them with False.
            odoo_vals[key] = False
    return odoo_vals


class DnbApiConnector:
    user = False
    password = False
    limit = 0
    token = False

    def __new__(cls, *args, **kw):  # pylint: disable=unused-argument
        # Python singleton
        if not hasattr(cls, "instance"):
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self, user, password, limit):
        logging.basicConfig(
            filename="api_connector.log", encoding="utf-8", level=logging.DEBUG
        )
        self.user = user
        self.password = password
        self.limit = limit
        if not self.token:
            self.set_token()

    def do_request(
        self, endpoint, data=None, headers=None, path_parameter=None, renew_token=True
    ):
        if data is None:
            data = {}
        if not headers:
            headers = {
                "Authorization": f"Bearer {self.token}",
                "Content-Type": "application/json",
            }
        try:
            path_endpoint = API_ENDPOINTS[endpoint][1]
            if path_parameter:
                path_endpoint = f"{path_endpoint}/{path_parameter}"
            if API_ENDPOINTS[endpoint][0] == "GET":
                result = requests.get(path_endpoint, headers=headers, params=data)
            else:
                result = requests.post(path_endpoint, headers=headers, json=data)
            content = json.loads(result.content)

            if result.status_code == 200:
                return {
                    "status": "ok",
                    "content": content,
                }
            error = content.get("error", {})
            message = error.get("errorMessage", "")
            if error.get("errorDetails"):
                message += f" {', '.join(msg for e in error.get('errorDetails') for msg in e['errors'].values())}"

            if "Access token expired" in message and renew_token:
                self.set_token()
                return self.do_request(
                    endpoint, data, headers, path_parameter, renew_token=False
                )
            logging.error(message)
            logging.debug(result, content)
            return {
                "status": "error",
                "message": message,
            }
        except Exception as error:  # pylint: disable=broad-except
            logging.error(error)
            return {"status": "error", "message": error}

    def set_token(self):
        login = f"{self.user}:{self.password}"
        login_base64 = base64.b64encode(login.encode("ascii")).decode("ascii")
        headers = {
            "Authorization": f"Basic {login_base64}",
            "Content-Type": "application/json",
        }
        data = {"grant_type": "client_credentials"}
        result = self.do_request("access_token", data, headers=headers)
        if result["status"] == "ok":
            self.token = result["content"].get("access_token")
            return {"status": "ok"}
        message = f"Error while optaining token: {result.get('message')}"
        return {"status": "error", "message": message}

    def search(self, vals, endpoint):
        logging.info("Performing search: %s", vals)
        data = translate_to_dnb_vals(vals, endpoint)
        return self.do_request(endpoint, data)

    def search_match(self, vals):
        if self.limit:
            vals.update({"limit": self.limit})
        result = self.search(vals, "search_match")
        if result["status"] == "ok":
            company_results = []
            for match_candidate in result["content"]["matchCandidates"]:
                company_values = match_candidate["organization"]
                company_values["confidence_code"] = match_candidate[
                    "matchQualityInformation"
                ]["confidenceCode"]
                company_values["match_grade"] = match_candidate[
                    "matchQualityInformation"
                ]["matchGrade"]
                company_values["name_match_score"] = match_candidate[
                    "matchQualityInformation"
                ]["nameMatchScore"]

                company_results.append(company_values)
            return {
                "status": "ok",
                "companies": [
                    translate_to_odoo_vals(company_res, get_grade=True)
                    for company_res in company_results
                ],
            }

        message = result.get("message")
        if "No Match found" in message:
            return {"status": "ok", "companies": []}

        message = f"Error while performing search: {message}"
        return {"status": "error", "message": message}

    def search_leads(self, vals):
        if not any(key in STANDALONE_PARAMETERS for key in vals.keys()):
            return {
                "status": "error",
                "message": "Please include at least one standalone parameter in your search (DUNS, Name, Phone, "
                "Website, Enterprise registry, VAT, Street Address or Industry Codes)",
            }
        result = self.search(vals, "search_leads")
        if result["status"] == "ok":
            company_results = [
                res["organization"] for res in result["content"]["searchCandidates"]
            ]
            return {
                "status": "ok",
                "number_of_match": result["content"].get("candidatesMatchedQuantity"),
                "companies": [
                    translate_to_odoo_vals(company_res)
                    for company_res in company_results
                ],
            }

        message = result.get("message")
        if "No match found" not in message:
            message = f"Error while performing search: {message}"
        return {"status": "error", "message": message}

    def get_datablock_info(self, duns, block_ids):
        logging.info("Get company info DUNS %s", duns)
        # create string and avoid duplicates in it
        block_ids = ",".join(
            [DATA_BLOCKS[block_id] for block_id in list(set(block_ids))]
        )
        result = self.do_request(
            endpoint="company_info",
            data={"blockIDs": block_ids},
            path_parameter=duns,
        )
        if result["status"] == "ok":
            company_vals = translate_to_odoo_vals(
                result.get("content").get("organization", {})
            )
            return {"status": "ok", "company": company_vals}

        return {"status": "error", "message": result.get("message")}

    def get_industry_codes(self):
        logging.info("Updating D&B Industries")
        result = self.do_request("industry_codes")
        # TODO get category in all installed languages with
        #  https://plus.dnb.com/v1/referenceData/category?id=1015&languageCode=41
        if result["status"] == "ok":
            # Skip parent classifications to avoid duplicates (0111, 011100, 01110000 are all 'Wheat')
            industries = [
                {"code": res["code"], "name": res["description"]}
                for res in result["content"]["codeTables"][0]["codeLists"]
                if len(res["code"]) == 8
            ]
            return {"status": "ok", "content": industries}

        message = f"Error while retrieving industries: {result.get('message')}"
        return {"status": "error", "message": message}
