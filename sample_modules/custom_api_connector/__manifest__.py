# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

{
    "name": "D&B Connector",
    "category": "Contact",
    "summary": "Odoo Connector for Dun & Bradstreet Data",
    "website": "https://www.niboo.com/",
    "version": "17.0.1.0.0",
    "license": "Other proprietary",
    "description": """
## Configuration
- Set your D&B API credentials: When setting the credentials for the first time, D&B Industries will be loaded.
This can take up to 1 minute.
- Choose features to activate

## Features (require D&B contract)
- Match Contacts
- Clean Contact Database
- Contact Monitoring
- Contact Risk Insight
- Lead Generation

    """,
    "author": "Niboo",
    "depends": [
        "base_setup",
        "contacts",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/dnb_data.xml",
        "views/dnb_industry.xml",
        "views/res_config_settings.xml",
        "views/res_partner.xml",
    ],
    "price": 0.00,
    "installable": True,
    "application": False,
}
