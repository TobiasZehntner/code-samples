# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, models
from odoo.osv import expression

from ..lib.niboo_connector import NibooConnector


class DnbApi(models.AbstractModel):
    """
    Abstract model without records. For functions connecting to D&B requiring Odoo env
    """

    _name = "dnb.api"
    _description = "D&B API"

    @api.model
    def get_api(self):
        """
        Return API object with User/Password from config.settings
        """
        api_credentials = self.env["res.config.settings"].get_values()
        user = api_credentials.get("dnb_api_user")
        password = api_credentials.get("dnb_api_password")
        limit = api_credentials.get("dnb_num_results")

        if user and password:
            return NibooConnector(user, password, limit)

        raise exceptions.ValidationError(
            _("Please set User and Password for the D&B API in Settings > Contacts")
        )

    @api.model
    def set_token(self):
        """
        Set token on the API that is used to do requests
        """
        niboo_api = self.get_api()
        result = niboo_api.set_token()
        if result["status"] != "ok":
            raise exceptions.ValidationError(result["message"])

    @api.model
    def dnb_search(self, vals, mode):
        """
        Basic search
        @param vals: Search values
        @type vals: dict
        @return: List of matching company data
        @rtype: list
        """
        niboo_api = self.get_api()

        if vals.get("country_id"):
            country = self.env["res.country"].browse(vals["country_id"])
            vals["country_code"] = country.code
            del vals["country_id"]

        if vals.get("state_id"):
            state = self.env["res.country.state"].browse(vals["state_id"])
            vals["state_name"] = state.name
            del vals["state_id"]
        if vals.get("dnb_industry_ids"):
            industries = self.env["dnb.industry"].browse(vals["dnb_industry_ids"])
            vals["dnb_industry_codes"] = industries.mapped("code")
            del vals["dnb_industry_ids"]

        search_method = (
            niboo_api.search_match if mode == "match" else niboo_api.search_leads
        )
        result = search_method(vals)
        if result["status"] == "ok":
            if mode == "leads":
                return result
            else:
                return result["companies"]

        raise exceptions.ValidationError(result["message"])

    def dnb_search_match(self, vals):
        if "country_id" not in vals:
            # Country is mandatory
            raise exceptions.ValidationError(_("Please select a country"))
        return self.dnb_search(vals, "match")

    def dnb_search_leads(self, vals):
        return self.dnb_search(vals, "leads")

    def get_datablock_info(self, duns, block_ids):
        niboo_api = self.get_api()
        result = niboo_api.get_datablock_info(duns, block_ids)
        if result["status"] == "ok":
            return result["company"]
        raise exceptions.ValidationError(result["message"])

    def domain_to_vals(self, domain):
        """
        Translate a search domain into a search vals dict
        """
        vals = {}
        for element in domain:
            # Remove AND/OR conditions and check for negative operators
            if expression.is_leaf(element):
                key, operator, value = element
                if key in ["is_incomplete"]:
                    # Skip non-DNB related filters
                    continue
                if operator in expression.NEGATIVE_TERM_OPERATORS or not value:
                    raise exceptions.ValidationError(
                        _("Negative search operators are not supported")
                    )
                if operator in ["<", "<=", ">", ">="] and key in [
                    "yearly_revenue",
                    "num_employees",
                ]:
                    if key == "yearly_revenue":
                        if operator in [">", ">="]:
                            vals["min_revenue"] = value
                        else:
                            vals["max_revenue"] = value
                    elif key == "num_employees":
                        if operator in [">", ">="]:
                            vals["min_employees"] = value
                        else:
                            vals["max_employees"] = value
                elif operator in ["=", "like", "ilike"]:
                    if key == "zip" and ("zip" in vals or "zip_list" in vals):
                        if vals.get("zip_list"):
                            vals["zip_list"].append(value)
                        else:
                            vals["zip_list"] = [vals.get("zip"), value]
                            del vals[key]
                    elif key == "dnb_industry_ids":
                        industries = self.env["dnb.industry"].search(
                            [("name", operator, value)]
                        )
                        vals["dnb_industry_codes"] = industries.mapped("code")
                    else:
                        vals[key] = value
                else:
                    return {
                        "status": "error",
                        "message": f"Unsupported Operator '{operator}' for '{key}'",
                    }
        return vals

    @api.model
    def dnb_search_domain(self, domain):
        """
        Basic search with a search domain
        @param domain: [(key, operator, value)]
        @type domain: list
        @return: List of company data
        @rtype: list
        """
        vals = self.domain_to_vals(domain)
        return self.dnb_search(vals, "match")

    @api.model
    def get_industries(self):
        """
        Get D&B Industry categories
        @return: Industry data
        @rtype: list
        """
        niboo_api = self.get_api()
        result = niboo_api.get_industries()

        if result["status"] == "ok":
            return result["content"]
        raise exceptions.ValidationError(result["message"])

    @api.model
    def get_country(self, code):
        return self.env["res.country"].search([("code", "=", code)], limit=1)

    @api.model
    def get_state(self, country, state_code, state_name):
        # TODO create state if not existing? (e.g. states/regions in France are missing)
        return self.env["res.country.state"].search(
            [
                ("country_id", "=", country.id),
                "|",
                ("code", "=", state_code),
                ("name", "=", state_name),
            ],
            limit=1,
        )

    @api.model
    def get_values(self, company_data, value_type="wizard"):
        dnb_industry_obj = self.env["dnb.industry"]
        country = self.get_country(company_data.get("country_code"))
        state = self.get_state(
            country, company_data.get("state_code"), company_data.get("state_name")
        )
        values = {
            "duns": company_data.get("duns"),
            "name": company_data.get("name"),
            "is_fortune_1000": company_data.get("is_fortune_1000"),
            "street": company_data.get("street"),
            "street2": company_data.get("street2"),
            "city": company_data.get("city"),
            "country_id": country.id,
            "state_id": state.id,
            "website": company_data.get("website"),
            "phone": company_data.get("phone"),
            "vat": company_data.get("vat"),
            "zip": company_data.get("zip"),
            "num_employees": company_data.get("num_employees"),
            "yearly_revenue": company_data.get("yearly_revenue"),
        }

        if value_type == "person":
            values.update(
                {
                    "is_incomplete": company_data.get("is_incomplete"),
                    "company_registry": company_data.get("company_registry"),
                    "lang_code": company_data.get("lang_code"),
                    "maximum_credit_recommendation_currency_id": company_data.get(
                        "maximum_credit_recommendation_currency_id"
                    ),
                }
            )
            related_company_ids = []
            for related_company in company_data.get("linked_duns", []):
                existing_linked_record = self.env["dnb.data"].search(
                    [("duns", "=", related_company["duns"])]
                )
                if not existing_linked_record:
                    existing_linked_record = self.env["dnb.data"].create(
                        related_company
                    )
                related_company_ids.append(existing_linked_record.id)
            values["linked_company_ids"] = [(6, 0, related_company_ids)]
        elif value_type == "wizard":
            values.update(
                {
                    "confidence_code": int(company_data.get("confidence_code")),
                    "match_grade": company_data.get("match_grade"),
                    "name_match_score": company_data.get("name_match_score"),
                }
            )
        if company_data.get("industry_codes", []):
            industries = dnb_industry_obj.search(
                [("code", "in", company_data.get("industry_codes", []))]
            )
            if not industries:
                # Received industry data that does not exist in DB. Update table
                dnb_industry_obj.sudo().update()
                industries = dnb_industry_obj.search(
                    [("code", "in", company_data.get("industry_codes", []))]
                )
            values["dnb_industry_ids"] = [(6, 0, industries.ids)]

        # return only not "None" values
        return {k: v for k, v in values.items() if v is not None and v is not False}
