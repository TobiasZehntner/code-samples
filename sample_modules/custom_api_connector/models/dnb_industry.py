# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import api, fields, models


class DnbIndustry(models.Model):
    _name = "dnb.industry"
    _description = "D&B Industry Categorization"
    _rec_name = "name"
    _order = "code"

    code = fields.Char("Code", required=True)
    name = fields.Char("Description", required=True)

    parent_industry_id = fields.Many2one("dnb.industry")
    child_industry_ids = fields.One2many("dnb.industry", "parent_industry_id")
    is_parent = fields.Boolean()

    _sql_constraints = [
        ("code_unique", "unique(code)", "Industry Code is required to be unique.")
    ]

    def update(self):
        """
        Update industry data: get all, update or create if not existing
        """
        # TODO get industry in all installed languages
        industries = self.env["dnb.api"].get_industries()

        for industry in industries:
            existing_industry = self.search([("code", "=", industry["code"])])
            if existing_industry:
                existing_industry.write({"name": industry["name"]})
            else:
                self.create(industry)

        self.env.cr.execute("SELECT id FROM dnb_industry WHERE code like '%0000';")
        parent_ids = [result[0] for result in self.env.cr.fetchall()]
        parent_industries = self.browse(parent_ids)
        parent_industries.write({"is_parent": True})
        for parent_industry in parent_industries:
            self.env.cr.execute(
                f"SELECT id FROM dnb_industry "
                f"WHERE code LIKE '{parent_industry.code[:4]}%' AND code != '{parent_industry.code[:4]}0000';"
            )
            child_ids = [result[0] for result in self.env.cr.fetchall()]
            child_industries = self.browse(child_ids)
            child_industries.parent_industry_id = parent_industry

    @api.model
    def name_search(self, name, args=None, operator="ilike", limit=100):
        args = args or []
        domain = []
        if name:
            domain = [
                "|",
                ("name", operator, name),
                ("child_industry_ids.name", operator, name),
            ]

        industries = self.search(domain + args, limit=limit)
        return industries.name_get()
