# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import api, fields, models

DNB_FEATURE_MODULES = [
    "module_dnb_contact_match",
    "module_dnb_contact_clean",
    "module_dnb_contact_track",
    "module_dnb_contact_risk",
    "module_dnb_crm",
]


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    dnb_api_user = fields.Char("D&B API User", config_parameter="dnb.api_user")
    dnb_api_password = fields.Char(
        "D&B API Password", config_parameter="dnb.api_password"
    )
    dnb_license = fields.Char(
        "Niboo D&B License", help="Your Niboo license for the Dun & Bradsreet Connector"
    )
    dnb_num_results = fields.Integer(
        "Max Number of Results for Contact Match",
        config_parameter="dnb.num_results",
        default=10,
        help="Limit the number of results per request. If set to 0, default of 10 will be used. Max: 25",
    )
    module_dnb_contact_match = fields.Boolean("Match Contacts")
    module_dnb_contact_clean = fields.Boolean(
        "Clean Contact Database", readonly=True, help="The feature is not yet available"
    )
    module_dnb_contact_track = fields.Boolean(
        "Contact Monitoring", readonly=True, help="The feature is not yet available"
    )
    module_dnb_contact_risk = fields.Boolean("Contact Risk Insight")
    module_dnb_crm = fields.Boolean("Lead Generation")

    @api.onchange(*DNB_FEATURE_MODULES)
    def _onchange_dnb_features(self):
        self.ensure_one()
        if (not self.dnb_api_user or not self.dnb_api_password) and any(
            self[module] for module in DNB_FEATURE_MODULES
        ):
            return {
                "value": {module: False for module in DNB_FEATURE_MODULES},
                "warning": {
                    "title": "Missing API credentials",
                    "message": "Please fill in API User and Password information before activating features",
                },
            }

    @api.onchange("module_dnb_contact_match")
    def _onchange_module_dnb_contact_match(self):
        """
        The modules module_dnb_contact_match and Odoo's module_partner_autocomplete exclude each other
        """
        self.ensure_one()
        if self.module_dnb_contact_match:
            self.module_partner_autocomplete = False

    @api.onchange("module_partner_autocomplete")
    def _onchange_module_partner_autocomplete(self):
        """
        The modules module_dnb_contact_match and Odoo's module_partner_autocomplete exclude each other
        """
        self.ensure_one()
        if self.module_partner_autocomplete:
            if self.module_dnb_contact_match:
                self.module_dnb_contact_match = False
                return {
                    "warning": {
                        "title": "Conflicting Modules",
                        "message": "D&B Match Contacts and Odoo's Partner Autocomplete exclude each other. "
                        "D&B Match Contact feature will be uninstalled.",
                    }
                }

    @api.onchange("dnb_num_results")
    def _onchange_dnb_num_results(self):
        self.ensure_one()
        self.dnb_num_results = min(self.dnb_num_results, 25)

    @api.model
    def get_values(self):
        res = super().get_values()
        params = self.env["ir.config_parameter"].sudo()
        res.update(dnb_api_user=params.get_param("dnb.api_user"))
        res.update(dnb_api_password=params.get_param("dnb.api_password"))
        res.update(dnb_num_results=int(params.get_param("dnb.num_results")))
        return res

    def set_values(self):
        super().set_values()
        ir_config_param = self.env["ir.config_parameter"].sudo()
        if ir_config_param.get_param("dnb.num_results") != self.dnb_num_results:
            ir_config_param.set_param("dnb.num_results", str(self.dnb_num_results))
        if ir_config_param.get_param("dnb.api_user") != self.dnb_api_user:
            ir_config_param.set_param("dnb.api_user", self.dnb_api_user)
        if ir_config_param.get_param("dnb.api_password") != self.dnb_api_password:
            ir_config_param.set_param("dnb.api_password", self.dnb_api_password)

        if self.dnb_api_user and self.dnb_api_password:
            if not self.env["dnb.industry"].search([]):
                # When setting API credentials for the first time, we load D&B Industries
                self.env["dnb.industry"].sudo().update()

            if not self.env["dnb.industry"].search([]):
                # When setting API credentials for the first time, we load D&B Industries
                self.env["dnb.industry"].sudo().update()

    def reset_token(self):
        self.env["dnb.api"].set_token()
