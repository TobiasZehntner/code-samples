# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    dnb_id = fields.Many2one("dnb.data", "DUNS", help="Dun & Bradstreet identifier")
    duns = fields.Char(related="dnb_id.duns", store=True, string="Partner DUNS")
    dnb_industry_ids = fields.Many2many("dnb.industry", string="D&B Industries")
    num_employees = fields.Integer("Number of Employees")
    yearly_revenue = fields.Char("Yearly Revenue")
    is_fortune_1000 = fields.Boolean("Fortune 1000 Listed")

    # TODO unique(dnb_id,company_id)?
    _sql_constraints = [
        (
            "dnb_id_unique",
            "unique(dnb_id)",
            "The DUNS number is already assigned to a contact.",
        )
    ]

    # BUSINESS NAME
    business_name_user = fields.Char()
    tradestyle = fields.Char()

    # ADDRESS FIELDS
    latitude = fields.Char()
    longitude = fields.Char()

    # COMPANY PROFILE
    operating_status = fields.Char()
    start_date = fields.Date()
    nb_employee = fields.Integer()
    nb_employee_date = fields.Date()
    nb_employee_scope = fields.Char()
    nb_employee_code_scope = fields.Char()
    nb_employee_reliability = fields.Char()
    nb_employee_code_reliability = fields.Char()

    # LEGAL FORM # TODO check which info is useful and only use that
    business_entity_type_code = fields.Char()
    global_legal_form = fields.Char()
    global_legal_form_code = fields.Char()
    global_legal_form_start_date = fields.Date()
    charter_type = fields.Char()
    charter_type_code = fields.Char()
    control_type = fields.Char()
    control_type_code = fields.Char()
    local_legal_form = fields.Char()
    local_legal_form_code = fields.Char()

    # FINANCIALS # FIXME only yearly_revenue is used, duplicated from above)
    fin_statement_to_date = fields.Date()
    yearly_revenue_currency = fields.Many2one("res.currency")
    fin_statement_reliability = fields.Char()
    # yearly_revenue = fields.Integer()
    fin_statement_scope = fields.Char()
    yearly_revenue_unit = fields.Char()

    # COMPANY CONTACT # FIXME duplicate with above
    phone_country_code = fields.Char()
    phone_number = fields.Char()
    website_url = fields.Char()
    contact_email = fields.Char()
    vat = fields.Char(string="Registration Number")

    def button_update_company_info(self):
        """Reset data on partner with data of linked dnb.data"""
        self.ensure_one()
        if self.dnb_id:
            self.dnb_id.update_company_info(["company_info"])
            self._onchange_dnb_id()
        else:
            exceptions.UserError(_("This contact is not assigned to a DUNS Number"))

    @api.onchange("dnb_id")
    @api.constrains("dnb_id")
    def _onchange_dnb_id(self):
        for partner in self:
            if partner.dnb_id:
                partner.is_company = True
                partner.name = partner.dnb_id.name
                partner.website = partner.dnb_id.website
                partner.phone = partner.dnb_id.phone
                partner.dnb_industry_ids = [(6, 0, partner.dnb_id.dnb_industry_ids.ids)]
                partner.street = partner.dnb_id.street
                partner.street2 = partner.dnb_id.street2
                partner.country_id = partner.dnb_id.country_id.id
                partner.state_id = partner.dnb_id.state_id.id
                partner.city = partner.dnb_id.city
                partner.zip = partner.dnb_id.zip
                partner.vat = partner.dnb_id.vat
                partner.company_registry = partner.dnb_id.company_registry
                partner.num_employees = partner.dnb_id.num_employees
                partner.is_fortune_1000 = partner.dnb_id.is_fortune_1000
                partner.yearly_revenue = partner.dnb_id.yearly_revenue

                if any(
                    partner.dnb_id.lang_code == lang_tuple[0]
                    for lang_tuple in self.env["res.lang"].get_installed()
                ):
                    # Only set partner language if installed
                    # TODO if not direct match, set similar (fr_FR instead of fr_BE)
                    partner.lang = partner.dnb_id.lang_code

    @api.constrains("dnb_id", "is_company")
    def _constrains_dnb_id(self):
        for partner in self:
            if partner.dnb_id and not partner.is_company:
                raise exceptions.ValidationError(
                    _("DUNS Number can only be set for Companies")
                )
