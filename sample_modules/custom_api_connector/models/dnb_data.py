# © 2023 Tobias Zehntner, Jerome Guerriat
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import api, fields, models

# Selection values imported by feature modules
EMPLOYEE_SELECTION = [
    ("num_employees_0_10", "Up to 10 Employees"),
    ("num_employees_10_50", "10-100 Employees"),
    ("num_employees_100_500", "100-500 Employees"),
    ("num_employees_100_500", "500-5000 Employees"),
    ("num_employees_5000", "Above 5000 Employees"),
]
REVENUE_SELECTION = [
    ("yearly_revenue_below_500000", "Revenue below 500k"),
    ("yearly_revenue_500000_1000000", "Revenue 500k-1m"),
    ("yearly_revenue_1000000_10000000", "Revenue 1-10m"),
    ("yearly_revenue_above_10000000", "Revenue above 10m"),
]
SELECT_DOMAINS = {
    "num_employees_0_10": [("num_employees", "<=", 10)],
    "num_employees_10_50": [("num_employees", ">=", 10), ("num_employees", "<=", 100)],
    "num_employees_100_500": [
        ("num_employees", ">=", 100),
        ("num_employees", "<=", 500),
    ],
    "num_employees_500_5000": [
        ("num_employees", ">=", 500),
        ("num_employees", "<=", 5000),
    ],
    "num_employees_5000": [("num_employees", ">=", 5000)],
    "yearly_revenue_below_500000": [("yearly_revenue", "<=", 500000)],
    "yearly_revenue_500000_1000000": [
        ("yearly_revenue", ">=", 500000),
        ("yearly_revenue", "<=", 1000000),
    ],
    "yearly_revenue_1000000_10000000": [
        ("yearly_revenue", ">=", 1000000),
        ("yearly_revenue", "<=", 10000000),
    ],
    "yearly_revenue_above_10000000": [("yearly_revenue", ">=", 10000000)],
}
# FIXME remove or make to list
COMPANY_VALUE_MAP = {
    "name": "name",
    "is_fortune_1000": "is_fortune_1000",
    "street": "street",
    "street2": "street2",
    "city": "city",
    "website": "website",
    "phone": "phone",
    "vat": "vat",
    "zip": "zip",
    "company_registry": "company_registry",
    "num_employees": "num_employees",
    "yearly_revenue": "yearly_revenue",
    "is_incomplete": "is_incomplete",
}


class DnbData(models.Model):
    _name = "dnb.data"
    _description = "D&B Data"
    _rec_name = "duns"

    duns = fields.Char("DUNS", required=True)
    partner_id = fields.Many2one("res.partner", "dnb_id")
    name = fields.Char("Name")
    website = fields.Char("Website")
    phone = fields.Char("Phone")
    dnb_industry_ids = fields.Many2many(
        "dnb.industry",
        string="Industries",
        help="Industry Codes according to D&B Standard",
    )
    street = fields.Char("Street")
    street2 = fields.Char("Street 2")
    zip = fields.Char("ZIP")
    city = fields.Char("City")
    state_id = fields.Many2one("res.country.state", string="Region")
    country_code = fields.Char("Country Code", related="country_id.code", store=True)
    country_id = fields.Many2one("res.country", "Country")
    vat = fields.Char("VAT")
    company_registry = fields.Char("Company ID")
    lang_code = fields.Char(
        "Language Code",
        help="Odoo's Language Code. Will only be set on partner if the language is installed.",
    )
    linked_company_ids = fields.Many2many(
        "dnb.data",
        relation="company_to_linked_company_rel",
        column1="company_id",
        column2="linked_company_id",
        string="Linked Companies",
    )
    num_employees = fields.Integer("Number of Employees")
    is_fortune_1000 = fields.Boolean("Fortune 1000 Listed")
    yearly_revenue = fields.Char("Yearly Revenue")
    creation_date = fields.Date()

    is_incomplete = fields.Boolean("Incomplete")

    # BUSINESS NAME
    business_name_user = fields.Char()
    tradestyle = fields.Char()

    # ADDRESS FIELDS
    latitude = fields.Char()
    longitude = fields.Char()

    # COMPANY PROFILE
    operating_status = fields.Char()
    start_date = fields.Date()
    nb_employee = fields.Integer()
    nb_employee_date = fields.Date()
    nb_employee_scope = fields.Char()
    nb_employee_code_scope = fields.Char()
    nb_employee_reliability = fields.Char()
    nb_employee_code_reliability = fields.Char()

    # LEGAL FORM FIXME only save info that we actually use
    business_entity_type_code = fields.Char()
    global_legal_form = fields.Char()
    global_legal_form_code = fields.Char()
    global_legal_form_start_date = fields.Date()
    charter_type = fields.Char()
    charter_type_code = fields.Char()
    control_type = fields.Char()
    control_type_code = fields.Char()
    local_legal_form = fields.Char()
    local_legal_form_code = fields.Char()

    # FINANCIALS FIXME probably not useful
    fin_statement_to_date = fields.Date()
    yearly_revenue_currency = fields.Many2one("res.currency")
    fin_statement_reliability = fields.Char()
    fin_statement_scope = fields.Char()
    yearly_revenue_unit = fields.Char()

    # COMPANY CONTACT FIXME duplicate
    phone_country_code = fields.Char()
    phone_number = fields.Char()
    website_url = fields.Char()
    contact_email = fields.Char()

    _sql_constraints = [
        (
            "duns_unique",
            "unique(duns)",
            "DUNS number is required to be unique.",
        )  # TODO make unique by company_id
    ]

    def name_get(self):
        """
        If specified in view, display company name instead of DUNS number
        """
        if self.env.context.get("display_name"):
            return [(record.id, record.name) for record in self]
        return super().name_get()

    def update_company_info(self, block_ids=None):
        self.ensure_one()
        if block_ids is None:
            block_ids = ["company_info"]
        company_values = self.env["dnb.api"].get_datablock_info(self.duns, block_ids)
        self.sudo().update_table([company_values])

    def open_contact(self):
        self.ensure_one()
        action = {
            "view_type": "form",
            "view_mode": "form",
            "res_model": "res.partner",
            "type": "ir.actions.act_window",
        }
        partner = self.env["res.partner"].search([("dnb_id", "=", self.id)], limit=1)
        if partner:
            action.update(
                {
                    "res_id": partner.id,
                    "target": "current",
                }
            )
        else:
            action.update(
                {
                    "target": "new",
                    "context": {
                        "default_dnb_id": self.id,
                        "default_is_company": True,
                    },
                }
            )
        return action

    @api.model
    def update_table(self, companies):
        records = self.env["dnb.data"]
        for company_data in companies:
            values = self.env["dnb.api"].get_values(company_data, value_type="person")
            existing_record = self.search([("duns", "=", company_data["duns"])])
            if existing_record:
                existing_record.write(values)
                records |= existing_record
            else:
                values.update({"duns": company_data["duns"]})
                records += self.create(values)
        return records

    def select_dnb_data(self):
        self.ensure_one()
        active_model = self.env.context.get("active_model")
        active_id = self.env.context.get("active_id")
        if active_id and active_model:
            calling_record = self.env[active_model].browse(active_id)
            calling_record.dnb_id = self

    @api.model
    def update_records_by_domain(self, domain):
        companies = self.env["dnb.api"].dnb_search_domain(domain)
        self.sudo().update_table(companies)
