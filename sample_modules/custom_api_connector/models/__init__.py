# © 2023 Tobias Zehntner
# © 2023 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from . import dnb_api
from . import dnb_data
from . import res_config_settings
from . import res_partner
from . import dnb_industry
