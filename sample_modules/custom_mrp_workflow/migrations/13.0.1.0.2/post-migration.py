# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import SUPERUSER_ID, api


def migrate(cr, version):
    with api.Environment.manage():
        # add user of super user group into the new created group
        env = api.Environment(cr, SUPERUSER_ID, {})
        gantt_menu = env.ref(
            "studio_customization.manufacturing_gantt__3ffd624b-dc3a-48bf-9369-b1369f2ea07f"
        )
        mrp_to_print_menu = env.ref("any_mrp.menu_mrp_production_available_to_print")

        cr.execute(
            """
            -- Change sequence of menu created in studio
            UPDATE ir_ui_menu SET sequence=40 WHERE id=%s;
            UPDATE ir_ui_menu SET sequence=50 WHERE id=%s;
            -- Set existing job's users to False so it's not set to 'OdooBot'
            UPDATE mrp_job SET job_prep_user_id = NULL;
            """,
            (gantt_menu.id, mrp_to_print_menu.id),
        )
