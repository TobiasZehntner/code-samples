# © 2020 Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary


def migrate(cr, version):
    cr.execute(
        """
        -- Removed duplicate field on stock.move. Merge data.
        UPDATE stock_move
            SET sale_line_id = order_line_id
            WHERE sale_line_id IS NULL AND order_line_id IS NOT NULL;
        """
    )
