# © 2020 Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary


def migrate(cr, version):
    cr.execute(
        """
        -- Uninstall old modules
        UPDATE ir_module_module SET state='uninstalled'
        WHERE name in (
            'any_email', 'any_product', 'any_report', 'any_accounting');
        -- Delete all views from module so they can be updated
        -- (old views with updated IDs cause errors)
        DELETE from ir_ui_view WHERE arch_fs ILIKE 'any_mrp/%';
        -- Make any_mrp module data updateable (marked as noupdate by Studio)
        UPDATE ir_model_data SET noupdate=FALSE WHERE module='any_mrp';

        -- Odoo Studio
        -- Delete Studio views that are inactive
        DELETE FROM ir_ui_view WHERE name ILIKE '%Odoo Studio: %' AND active=FALSE;
        -- Delete studio view that inherits our own model since it has changed
        DELETE FROM ir_ui_view WHERE name ILIKE '%Odoo Studio: %' AND model='mrp.job';
        -- Update Odoo Studio views referring to old field name
        UPDATE ir_ui_view
            SET arch_db = REPLACE(arch_db, 'name="file"', 'name="file_name"')
            WHERE name ILIKE '%Odoo Studio%' AND arch_db ILIKE '%name="file"%';
        UPDATE ir_ui_view
            SET arch_db = REPLACE(arch_db, 'file', 'file_name')
            WHERE name ILIKE '%Odoo Studio%' AND arch_db ILIKE '%file''%';
        -- Delete a studio view that has no effect on the view
        DELETE FROM ir_ui_view WHERE id=2562;
        -- Delete studio action and menu that are now part of module
        DELETE FROM ir_ui_menu where name in (
            SELECT name FROM ir_act_window WHERE res_model = 'mrp.job'
        );
        DELETE FROM ir_act_window where res_model = 'mrp.job';

        -- Changes due to refactor
        -- Update state name change
        UPDATE mrp_job SET state='draft' WHERE state='new';
        UPDATE mrp_job SET state='failed' WHERE state='crashed';

        -- Update field names
        ALTER TABLE mrp_job
            RENAME serial_num TO name;
        ALTER TABLE mrp_job
            RENAME category_id TO product_id;
        ALTER TABLE mrp_job
            RENAME started_date TO datetime_start;
        ALTER TABLE mrp_job
            RENAME ending_date TO datetime_end;
        ALTER TABLE mrp_production
            RENAME file TO file_name;
        ALTER TABLE mrp_production
            RENAME line_order_id TO order_line_id;
        ALTER TABLE sale_order_line
            RENAME file TO file_name;
        ALTER TABLE stock_move
            RENAME sale_line_id TO order_line_id;
        ALTER TABLE stock_move
            RENAME file TO file_name;

        -- Fix report attachment file name (change of field type in v13)
        UPDATE ir_act_report_xml
            SET attachment = REPLACE(
                attachment, '.write_date', '.write_date.strftime("%Y-%m-%d %H:%M:%S")'
            )
            WHERE model='sale.order' and name='Quotation / Order'
                and attachment ilike '%.write_date%';
        """
    )
