# © 2020 PINTON KEVIN, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.
from datetime import datetime

from odoo import _, api, exceptions, fields, models

JOB_USER_FIELDS = [
    "job_prep_user_id",
    "machine_prep_user_id",
    "machine_unpack_user_id",
    "depowder_user_id",
]


class MrpJob(models.Model):
    _name = "mrp.job"
    _description = "MRP Job"
    _order = "name DESC"
    _inherit = ["mail.thread", "mail.activity.mixin"]

    name = fields.Char(
        string="Reference", readonly=True, copy=False, default=lambda self: _("New")
    )
    company_id = fields.Many2one(
        "res.company", string="Company", default=lambda self: self.env.company
    )
    equipment_category_id = fields.Many2one(
        "maintenance.equipment.category", "Equipment Category", required=True
    )
    equipment_id = fields.Many2one("maintenance.equipment", "Equipment")
    product_id = fields.Many2one(
        "product.template",
        "Powder",
        required=True,
        domain=[("categ_id.is_powder", "=", True)],
    )
    product_variant_ids = fields.Many2many(
        "product.product",
        string="Product Variants",
        compute="_compute_product_variant_ids",
        help="Used in view for domain",
    )
    lot_id = fields.Many2one("stock.production.lot", "Powder Serial Number")
    datetime_start = fields.Datetime("Start")
    datetime_end = fields.Datetime("End")

    qty_total = fields.Float("Total Quantity", compute="_compute_qty_total")

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("ready", "Ready"),
            ("progress", "In Progress"),
            ("cooling", "Cooling"),
            ("depowder", "Depowdering"),
            ("done", "Done"),
            ("cancel", "Cancelled"),
            ("failed", "Failed"),
        ],
        "Status",
        default="draft",
        tracking=True,
    )
    job_line_ids = fields.One2many(
        "mrp.job.line",
        string="Manufacturing Orders",
        inverse_name="job_id",
        required=True,
    )
    finished_move_ids = fields.Many2many(
        "stock.move.line",
        string="Stock Move Line",
        compute="_compute_finished_move_ids",
    )

    job_prep_user_id = fields.Many2one(
        "res.users",
        "Job Preparation User",
        default=lambda self: self._uid,
        tracking=True,
    )
    machine_prep_user_id = fields.Many2one(
        "res.users", "Machine Preparation User", tracking=True
    )
    machine_unpack_user_id = fields.Many2one(
        "res.users", "Machine Unpacking User", tracking=True
    )
    depowder_user_id = fields.Many2one("res.users", "Depowdering User", tracking=True)
    is_require_depowder = fields.Boolean(
        related="equipment_category_id.is_require_depowder", store=True, readonly=True
    )

    def name_get(self):
        result = []
        for job in self:
            equipment = f" [{job.equipment_id.name}]" if job.equipment_id else ""
            name = (
                f"{job.name} / {job.equipment_category_id.name}{equipment} - "
                f"{job.product_id.name}"
            )
            result.append((job.id, name))
        return result

    def _compute_finished_move_ids(self):
        """
        Get all the move lines of related finished Manufacturing Orders
        """
        for job in self:
            job.finished_move_ids = (
                job.job_line_ids.mapped("production_id")
                .filtered(lambda mo: mo.state == "done")
                .mapped("finished_move_line_ids")
            )

    def _compute_qty_total(self):
        for job in self:
            job.qty_total = sum(job.job_line_ids.mapped("quantity"))

    @api.depends("product_id")
    def _compute_product_variant_ids(self):
        """
        Compute variants for domain in form view
        """
        for job in self:
            job.product_variant_ids = (
                self.env["product.product"].search(
                    [("product_tmpl_id", "=", self.product_id.id)]
                )
                if job.product_id
                else False
            )

    @api.model
    def create(self, vals):
        """
        Set Job sequence on create
        """
        if "name" not in vals:
            vals["name"] = self.env["ir.sequence"].next_by_code(
                "mrp_job_sequence"
            ) or _("New")
        return super(MrpJob, self).create(vals)

    def button_ready(self):
        """
        Check if job lines
        Check if batch powder and equipment are set
        Check if quantity set is lower than qty available for each WO
        """
        self.ensure_one()
        if not self.job_line_ids:
            raise exceptions.UserError(_("Please add job lines with work orders"))
        for jobline in self.job_line_ids:
            wo = jobline.work_order_id
            available = wo.qty_production - wo.qty_produced
            if jobline.quantity <= 0:
                raise exceptions.UserError(
                    _("Quantity on job line is required to be above 0")
                )
            elif jobline.quantity > available:
                raise exceptions.ValidationError(
                    _(
                        f"Not enough quantity available for "
                        f"{jobline.production_id.name}. Max available: {available}."
                    )
                )
        self.state = "ready"

    def button_reset_to_draft(self):
        """
        Set the Job to Draft state.
        """
        self.ensure_one()
        self.state = "draft"

    def button_start(self):
        """
        Set job and all workorders in state progress
        """
        self.ensure_one()
        for jobline in self.job_line_ids:
            if jobline.work_order_id.state == "pending":
                raise exceptions.ValidationError(
                    _(
                        "The Printing Workorder is waiting for another workorder to be "
                        "completed."
                    )
                )
            elif jobline.work_order_id.state == "done":
                raise exceptions.ValidationError(
                    _("The Printing Workorder has already been processed manually.")
                )
            jobline.work_order_id.qty_producing = jobline.quantity
            jobline.work_order_id.button_start()
        self.write({"state": "progress", "datetime_start": datetime.now()})

    def button_finish(self):
        """
        Set job in state cooling
        """
        self.ensure_one()
        self.state = "cooling"

    def button_depowder(self):
        """
        Set job in state depowdering
        """
        self.ensure_one()
        self.state = "depowder"

    def button_cancel(self):
        """
        Put all the Work Orders in Cancel state
        """
        self.ensure_one()
        self.write({"state": "cancel", "datetime_end": datetime.now()})

    def button_success(self):
        """
        Create lots and process all workorders.
        Set job to state "done"
        """
        self.ensure_one()
        serial_counter = 1  # unique over multiple lines/products
        for job_line in self.job_line_ids:
            if job_line.quantity != 0:
                job_line.check_quantity()
                tracking = job_line.work_order_id.product_id.tracking
                if tracking == "serial":
                    for _i in range(0, int(job_line.quantity)):
                        job_line.process_work_order(
                            tracking="serial", counter=serial_counter
                        )
                        serial_counter += 1
                elif tracking == "lot":
                    job_line.process_work_order(tracking="lot")
                else:
                    job_line.process_work_order()
            else:
                job_line.state = "failed"

        self.write({"state": "done", "datetime_end": datetime.now()})

    def button_partial_fail(self):
        """
        Partial fail: open wizard to set successful quantity
        This will modify the qty of the job and process the successful qty with
        button_success()
        """
        self.ensure_one()
        wizard = self.env["mrp.job.partial_fail_wizard"].create({"job_id": self.id})
        return wizard.open()

    def button_fail(self):
        """
        Set job in state failed
        """
        self.ensure_one()
        self.write({"state": "failed", "datetime_end": datetime.now()})

    @api.constrains("state")
    def _constrains_state(self):
        for job in self:
            if job.state in ["done", "failed"]:
                if (
                    not job.job_prep_user_id
                    or not job.machine_prep_user_id
                    or not job.machine_unpack_user_id
                    or (job.is_require_depowder and not job.depowder_user_id)
                ):
                    raise exceptions.ValidationError(
                        _("Please assign all tasks before finishing a job")
                    )

    @api.constrains(*JOB_USER_FIELDS)
    def _constrains_job_user_fields(self):
        for job in self:
            if job.state in ["done", "failed"]:
                raise exceptions.UserError(
                    _("You cannot change a user for a job that is done")
                )

    def open_user_assign_wizard(self):
        vals = {"job_ids": self.ids}
        for user_field in JOB_USER_FIELDS:
            # If all records are assigned to the same user, we pre-fill the field with that user
            # If any record has field empty, we leave it empty
            field_values = {
                job[user_field] for job in self
            }  # Set includess 'empty' value
            if len(field_values) == 1:
                vals.update({user_field: self.mapped(user_field).id})
        wizard = self.env["mrp.job.user.assign.wizard"].create(vals)
        return {
            "name": "Assign Tasks",
            "type": "ir.actions.act_window",
            "view_type": "form",
            "view_mode": "form",
            "res_model": "mrp.job.user.assign.wizard",
            "res_id": wizard.id,
            "target": "new",
        }


class MrpJobLine(models.Model):
    _name = "mrp.job.line"
    _description = "MRP Job Line"

    job_id = fields.Many2one(
        "mrp.job", "Job", ondelete="cascade", readonly=True, required=True
    )
    quantity = fields.Float("Quantity", default=1.00, required=True)
    production_id = fields.Many2one(
        "mrp.production", "Manufacturing Order", required=True
    )
    work_order_id = fields.Many2one("mrp.workorder", "Work Order")

    # Related
    product_id = fields.Many2one(related="work_order_id.product_id", readonly=True)
    state = fields.Selection(related="job_id.state", readonly=True)
    file_name = fields.Char(related="production_id.file_name", readonly=True)

    _sql_constraints = [
        (
            "Job_mo_unique",
            "UNIQUE(job_id, production_id)",
            _("There can only be one job line per Manufacturing Order"),
        )
    ]

    @api.onchange("production_id")
    def _onchange_production_id(self):
        """
        Set domain on production and workorder.
        If production set, set workorder if only one available.
        """
        self.ensure_one()
        self.work_order_id = False

        # Consider only MOs that use the Job's powder as component
        available_mo = self.get_available_productions().filtered(
            lambda mo: mo not in self.job_id.job_line_ids.mapped("production_id")
        )
        allowed_mo = (
            self.env["stock.move"]
            .search(
                [
                    (
                        "product_id",
                        "in",
                        self.job_id.product_id.product_variant_ids.ids,
                    ),
                    ("raw_material_production_id", "in", available_mo.ids),
                ]
            )
            .mapped("raw_material_production_id")
        )
        domain = {"production_id": [("id", "in", allowed_mo.ids)]}
        warning = {}
        if self.production_id:
            workorders = self.get_available_workorders(self.production_id)
            domain["work_order_id"] = [("id", "in", workorders.ids)]
            if len(workorders) == 1:
                self.work_order_id = workorders.id

            if self.production_id.product_id.tracking not in ["serial", "lot"]:
                warning["title"] = _("Warning: Non tracked Product")
                warning["message"] = _(
                    f"The product {self.production_id.product_id.name} is not "
                    f"tracked and the finished pieces won't have a Serial/Lot number "
                    f"assigned."
                )

        return {"domain": domain, "warning": warning}

    def get_available_productions(self):
        mo_domain = [("state", "in", ["planned", "progress"])]
        # qty_available_to_print is non-stored computed and cannot be searched
        available_mo = (
            self.env["mrp.production"]
            .search(mo_domain)
            .filtered(lambda mo: mo.qty_available_to_print > 0)
        )
        return available_mo

    def get_available_workorders(self, production):
        return self.env["mrp.workorder"].search(
            [
                ("production_id", "=", production.id),
                ("workcenter_id.is_printing_type", "=", True),
                ("state", "!=", "done"),
            ]
        )

    @api.onchange("work_order_id")
    def _onchange_work_order_id(self):
        self.quantity = 0
        if self.work_order_id:
            self.quantity = self.work_order_id.qty_available_to_print

    def get_qty_jobline_wo(self, workorder, current_jobline=False):
        """
        From workorder, compute sum of job line quantities, excluding current job line
        """
        domain = [
            ("work_order_id", "=", workorder.id),
            ("state", "in", ["draft", "ready", "progress", "cooling", "depowder"]),
        ]
        if current_jobline:
            domain.append(("id", "!=", current_jobline.id))
        joblines = self.env["mrp.job.line"].search(domain)
        return sum(joblines.mapped("quantity"))

    def check_quantity(self):
        for line in self:
            if line.quantity > (
                line.work_order_id.qty_production - line.work_order_id.qty_produced
            ):
                raise exceptions.ValidationError(
                    _(
                        "You can't produce more items than specified in the "
                        "Manufacturing Order."
                    )
                )

    def process_work_order(self, tracking=None, counter=None):
        self.ensure_one()
        StockProductionLot = self.env["stock.production.lot"]

        job = self.job_id
        work_order = self.work_order_id
        qty = self.quantity

        if tracking:
            lot = False
            lot_name = job.name
            if tracking == "serial":
                if not counter:
                    raise exceptions.ValidationError(
                        _("Missing identifier while creating unique lot")
                    )
                lot_name = f"{lot_name}-{counter}"
                qty = 1
            else:
                # Multiple job lines with same product and lot-tracking: lot might exist
                lot = StockProductionLot.search(
                    [
                        ("name", "=", lot_name),
                        ("product_id", "=", work_order.product_id.id),
                        ("company_id", "=", job.company_id.id),
                    ]
                )
            if not lot:
                lot = StockProductionLot.create(
                    {
                        "name": lot_name,
                        "product_id": work_order.product_id.id,
                        "company_id": job.company_id.id,
                    }
                )
            work_order.finished_lot_id = lot

        work_order.lot_id = job.lot_id  # Consume the powder defined on the job
        work_order.qty_producing = qty
        work_order._onchange_qty_producing()
        work_order.with_context(no_redirect=True)._next()
        if work_order.is_last_step:
            work_order.record_production()
