# © 2020 Pierre Gillissen
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

from odoo import api, fields, models


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    file_name = fields.Char(
        "File Name", compute="_compute_file_name", store=True, readonly=True
    )

    @api.depends("sale_line_ids")
    def _compute_file_name(self):
        for line in self:
            line.file_name = ", ".join(
                {
                    sale_line.file_name
                    for sale_line in line.sale_line_ids
                    if sale_line.file_name
                }
            )
