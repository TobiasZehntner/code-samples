# © 2021 Tobias Zehntner
# © 2021 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, models


class MrpBom(models.Model):
    _inherit = "mrp.bom"

    @api.constrains("bom_line_ids")
    def _constrains_bom_line_ids(self):
        for bom in self:
            powder_components = bom.bom_line_ids.filtered(
                lambda l: l.product_id.categ_id.is_powder
            )
            if len(powder_components) > 1:
                raise exceptions.ValidationError(
                    _("A Bill of Material can only contain one powder component")
                )
