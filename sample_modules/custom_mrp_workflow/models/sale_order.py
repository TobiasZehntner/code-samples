# © 2020 Meriam Mzoughi, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    manufacture_ids = fields.One2many(
        "mrp.production",
        "order_id",
        string="Related Manufacturing Orders",
        readonly=True,
    )


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    file_name = fields.Char(string="File Name")

    def name_get(self):
        res = []
        for line in self:
            name = f"{line.order_id.name}: {line.product_id.name}"
            if line.file_name:
                name = f"{name} - {line.file_name}"
            res.append((line.id, name))
        return res
