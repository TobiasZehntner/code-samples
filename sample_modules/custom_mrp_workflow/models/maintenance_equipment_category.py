# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import fields, models


class MaintenanceEquipmentCategory(models.Model):
    _inherit = "maintenance.equipment.category"

    is_require_depowder = fields.Boolean("Requires Depowdering")
