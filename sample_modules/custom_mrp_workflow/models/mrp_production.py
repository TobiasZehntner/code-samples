# © 2020 Meriam Mzoughi, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

from odoo import _, api, exceptions, fields, models


class MrpProduction(models.Model):
    _inherit = "mrp.production"

    order_line_id = fields.Many2one("sale.order.line", "Sale Order Line")
    order_id = fields.Many2one("sale.order", "Sale Order")
    file_name = fields.Char("File Name")
    job_line_ids = fields.One2many(
        "mrp.job.line", "production_id", "Linked Job Lines", readonly=True
    )
    sale_analytic_account_id = fields.Many2one(
        related="order_id.analytic_account_id", readonly=True
    )
    qty_available_to_print = fields.Float(
        "Quantity Available to Print", compute="_compute_qty_available_to_print"
    )

    @api.constrains("product_id", "order_id", "order_line_id")
    def _constrains_order_values(self):
        """
        On save, force fields to correspond or raise if user needs to fix it
        """
        for mo in self:
            if mo.order_line_id:
                if mo.order_line_id.product_id != mo.product_id:
                    raise exceptions.UserError(
                        _(
                            "The product of the Sale Order Line does not correspond "
                            "with the product of the Manufacturing Order. Please "
                            "change one. (%s)"
                        )
                        % mo.name
                    )
                if mo.order_line_id.file_name != mo.file_name:
                    mo.file_name = mo.order_line_id.file_name
                if mo.order_line_id.order_id != mo.order_id:
                    mo.order_id = mo.order_line_id.order_id

    @api.onchange("order_line_id")
    def _onchange_order_line_id(self):
        """
        If sale.order.line is changed, update the order_id and file_name if needed
        """
        self.ensure_one()
        if self.order_line_id:
            if self.product_id != self.order_line_id.product_id:
                self.product_id = self.order_line_id.product_id
            if self.order_id != self.order_line_id.order_id:
                self.order_id = self.order_line_id.order_id
            if self.file_name != self.order_line_id.file_name:
                self.file_name = self.order_line_id.file_name

    @api.onchange("product_id")
    def _onchange_product_id(self):
        """
        If product is changed, reset sale order line, order and file_name
        """
        self.ensure_one()
        if self.order_line_id and self.product_id != self.order_line_id.product_id:
            self.order_line_id = False
            self.order_id = False
            self.file_name = False

    @api.onchange("order_id")
    def _onchange_order_id(self):
        """
        If SO changes, remove SO line and file_name, set domain on SO line
        """
        self.ensure_one()
        if self.order_id:
            if self.order_line_id and self.order_line_id.order_id != self.order_id:
                self.order_line_id = False
                self.file_name = False
            return {"domain": {"order_line_id": [("order_id", "=", self.order_id.id)]}}

    @api.constrains("move_raw_ids")
    def _constrains_move_raw_ids(self):
        for mo in self:
            powder_components = mo.move_raw_ids.filtered(
                lambda m: m.product_id.categ_id.is_powder
            )
            if len(powder_components) > 1:
                raise exceptions.ValidationError(
                    _("A Manufacturing Order can only contain one powder component")
                )

    def _compute_qty_available_to_print(self):
        for mo in self:
            mo.qty_available_to_print = sum(
                mo.workorder_ids.mapped("qty_available_to_print")
            )

    def open_mo_view(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_window",
            "name": "Manufacturing Order",
            "view_type": "form",
            "view_mode": "form",
            "res_model": self._name,
            "res_id": self.id,
            "target": "current",
        }

    def open_production_available_to_print(self):
        ready_productions = self.env["mrp.job.line"].get_available_productions()
        return {
            "type": "ir.actions.act_window",
            "name": "Manufacturing Order to Print",
            "view_mode": "tree,form",
            "res_model": self._name,
            "target": "current",
            "domain": [("id", "in", ready_productions.ids)],
        }

    def assign_to_job(self):
        if not self.filtered(lambda mo: mo.qty_available_to_print > 0):
            raise exceptions.UserError(
                _(
                    "The selected Manufacturing Orders have no available quantities to "
                    "print."
                )
            )
        wizard = self.env["mrp.job.assign_wizard"].create(
            {"production_ids": [(6, 0, self.ids)]}
        )
        return wizard.open()
