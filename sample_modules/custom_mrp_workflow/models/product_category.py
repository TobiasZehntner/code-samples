# © 2020 Meriam Mzoughi, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (<https://www.niboo.com/>)
# License Other Proprietary.

from odoo import fields, models


class ProductCategory(models.Model):
    _inherit = "product.category"

    is_powder = fields.Boolean(string="Powder Batch")
