# © 2020 PINTON KEVIN, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

from collections import defaultdict

from odoo import _, api, exceptions, fields, models
from odoo.tools import float_compare


class MrpWorkorder(models.Model):
    _inherit = "mrp.workorder"

    job_line_ids = fields.One2many(
        "mrp.job.line", string="Ref Job Line", inverse_name="work_order_id"
    )
    is_workcenter_printing_type = fields.Boolean(
        "Printing Type", related="workcenter_id.is_printing_type", readonly=True
    )
    file_name = fields.Char(related="production_id.file_name", readonly=True)
    mo_analytic_account_id = fields.Many2one(
        related="production_id.sale_analytic_account_id", readonly=True
    )
    qty_available_to_print = fields.Float(
        "Quantity Available to Print", compute="_compute_qty_available_to_print"
    )
    product_category_id = fields.Many2one(
        related="product_id.categ_id", string="Product Category", store=True
    )
    assign_user_id = fields.Many2one("res.users", "Assigned")

    def _compute_qty_available_to_print(self):
        job_line_obj = self.env["mrp.job.line"]
        for wo in self:
            qty_available = 0
            if wo.workcenter_id.is_printing_type:
                to_produce = wo.qty_production - wo.qty_produced
                unavailable = job_line_obj.get_qty_jobline_wo(wo)
                qty_available = to_produce - unavailable
            wo.qty_available_to_print = qty_available

    def record_production(self):
        """
        - Do not allow to produce without a lot if the product is tracked
        - Do not allow to produce more qty per lot that was produced in the previous WO
        """
        self.ensure_one()
        prev_work_order = self.search([("next_work_order_id", "=", self.id)])
        if prev_work_order and self.product_tracking != "none":
            if not self.finished_lot_id:
                raise exceptions.ValidationError(_("Please assign a Lot Number"))

            prev_qty_done = sum(
                prev_work_order.finished_workorder_line_ids.filtered(
                    lambda line: line.lot_id == self.finished_lot_id
                ).mapped("qty_done")
            )
            current_qty_done = sum(
                self.finished_workorder_line_ids.filtered(
                    lambda line: line.lot_id == self.finished_lot_id
                ).mapped("qty_done")
            )
            rounding = self.product_uom_id.rounding
            if (
                float_compare(
                    prev_qty_done,
                    current_qty_done + self.qty_producing,
                    precision_rounding=rounding,
                )
                < 0
            ):
                raise exceptions.ValidationError(
                    _(
                        f"You have produced {prev_qty_done} "
                        f"{self.product_id.uom_id.name} of lot "
                        f"{self.finished_lot_id.name} in the previous workorder. You "
                        f"are trying to produce {current_qty_done + self.qty_producing}"
                        f" in this one"
                    )
                )
        return super().record_production()

    # fmt: off
    @api.depends('production_id.workorder_ids.finished_workorder_line_ids',
                 'production_id.workorder_ids.finished_workorder_line_ids.qty_done',
                 'production_id.workorder_ids.finished_workorder_line_ids.lot_id')
    def _compute_allowed_lots_domain(self):
        """
        OVERWRITE:
        Odoo: if previous workorder hasn't produced all qty, Odoo allows any lot. If
        it produced all, it only allows the ones that were produced.
        We change it so it ALWAYS only allows the ones produced previously
        """
        productions = self.mapped('production_id')
        treated = self.browse()
        for production in productions:
            if production.product_id.tracking == 'none':
                continue

            # rounding = production.product_uom_id.rounding
            finished_workorder_lines = production.workorder_ids.mapped('finished_workorder_line_ids').filtered(lambda wl: wl.product_id == production.product_id)  # noqa: B950
            qties_done_per_lot = defaultdict(list)
            for finished_workorder_line in finished_workorder_lines:
                # It is possible to have finished workorder lines without a lot (eg using the dummy  # noqa: B950
                # test type). Ignore them when computing the allowed lots.
                if finished_workorder_line.lot_id:
                    qties_done_per_lot[finished_workorder_line.lot_id.id].append(finished_workorder_line.qty_done)  # noqa: B950

            # START OVERWRITE: remove 'if' and only apply 'else' condition
            # qty_to_produce = production.product_qty
            # if production.product_id.tracking == 'serial':
            #     qty_to_produce = production.product_uom_id._compute_quantity(
            #         production.product_qty,
            #         production.product_id.uom_id,
            #         round=False
            #     )
            # allowed_lot_ids = self.env['stock.production.lot']
            # qty_produced = sum([max(qty_dones) for qty_dones in qties_done_per_lot.values()])  # noqa: B950
            # if float_compare(qty_produced, qty_to_produce, precision_rounding=rounding) < 0:  # noqa: B950
            #     # If we haven't produced enough, all lots are available
            #     allowed_lot_ids = self.env['stock.production.lot'].search([
            #         ('product_id', '=', production.product_id.id),
            #         ('company_id', '=', production.company_id.id),
            #     ])
            # else:
            #     # If we produced enough, only the already produced lots are available
            #     allowed_lot_ids = self.env['stock.production.lot'].browse(qties_done_per_lot.keys())  # noqa: B950
            #
            allowed_lot_ids = self.env['stock.production.lot'].browse(
                qties_done_per_lot.keys())
            # END OVERWRITE

            workorders = production.workorder_ids.filtered(lambda wo: wo.state not in ('done', 'cancel'))  # noqa: B950
            for workorder in workorders:
                if workorder.product_tracking == 'serial':
                    workorder.allowed_lots_domain = allowed_lot_ids - workorder.finished_workorder_line_ids.filtered(lambda wl: wl.product_id == production.product_id).mapped('lot_id')  # noqa: B950
                else:
                    workorder.allowed_lots_domain = allowed_lot_ids
                treated |= workorder
        (self - treated).allowed_lots_domain = False
    # fmt: on
