# © 2020 PINTON KEVIN, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

from odoo import fields, models


class MrpWorkCenter(models.Model):
    _inherit = "mrp.workcenter"

    is_printing_type = fields.Boolean("Job Printing Work Center")
    workorder_to_produce_count = fields.Integer(
        "Total To Produce Orders",
        compute="_compute_workorder_count",
        help="Work Orders that are to launch or in progress",
    )

    def _compute_workorder_count(self):
        super()._compute_workorder_count()
        for wo in self:
            wo.workorder_to_produce_count = (
                wo.workorder_ready_count + wo.workorder_progress_count
            )
