# © 2018 Tobias Zehntner
# © 2018 Niboo SPRL (<https://www.niboo.be/>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    # Make date fields editable
    date_done = fields.Datetime(readonly=False)
    scheduled_date = fields.Datetime(states={})

    @api.depends("move_lines.date_expected")
    def _compute_scheduled_date(self):
        """
        Planning an MO moves the delivery forward if the MO is finished ahead of time.
        We avoid this so the delivery stays scheduled as to what the SO was committed to
        """
        super()._compute_scheduled_date()
        for picking in self:
            commit_date = picking.sale_id.commitment_date
            if (
                commit_date
                and commit_date > picking.scheduled_date
                and picking.picking_type_code == "outgoing"
            ):
                picking.scheduled_date = commit_date


class StockMove(models.Model):
    _inherit = "stock.move"

    file_name = fields.Char(
        "File Name",
        compute="_compute_file_name",
        inverse="_inverse_file_name",
        store=True,
    )

    @api.depends("sale_line_id", "raw_material_production_id", "production_id")
    def _compute_file_name(self):
        """
        File Name on stock.move is required both on the sale side
        (picking, delivery) and manufacturing (raw, finished moves). It will always be
        the same but the moves are linked to different records.
        """
        for move in self:
            if move.sale_line_id and move.sale_line_id.file_name:
                move.file_name = move.sale_line_id.file_name
            elif (
                move.raw_material_production_id
                and move.raw_material_production_id.file_name
            ):
                move.file_name = move.raw_material_production_id.file_name
            elif move.production_id and move.production_id.file_name:
                move.file_name = move.production_id.file_name
            else:
                move.file_name = False

    def _inverse_file_name(self):
        """
        Allow user to change file_name without consequences to other records
        """
        pass

    def _prepare_procurement_values(self):
        """
        Pass sale.order.line and file_name to mrp.production
        """
        res = super()._prepare_procurement_values()
        res.update(
            {
                "order_line_id": self.sale_line_id.id,
                "file_name": self.sale_line_id.file_name,
            }
        )
        return res


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    file_name = fields.Char("File Name", related="move_id.file_name", readonly=True)


class StockRule(models.Model):
    _inherit = "stock.rule"

    def _prepare_mo_vals(
        self,
        product_id,
        product_qty,
        product_uom,
        location_id,
        name,
        origin,
        company_id,
        values,
        bom,
    ):
        """
        Pass sale.order.line and file_name to mrp.production
        """
        res = super()._prepare_mo_vals(
            product_id,
            product_qty,
            product_uom,
            location_id,
            name,
            origin,
            company_id,
            values,
            bom,
        )
        res.update(
            {
                "order_line_id": values.get("order_line_id", False),
                "file_name": values.get("file_name", False),
            }
        )
        return res


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    name = fields.Char(default=False)  # Do not set a default sequence
