# © 2020 Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models


class JobAssignWizard(models.TransientModel):
    _name = "mrp.job.assign_wizard"
    _description = "MRP Job: Assign Wizard"

    production_ids = fields.Many2many(
        "mrp.production", string="Manufacturing Orders", required=True, readonly=True
    )
    wizard_line_ids = fields.One2many(
        "mrp.job.assign_wizard.line", inverse_name="wizard_id", string="Wizard Lines"
    )
    job_id = fields.Many2one("mrp.job", "Job")
    powder_product_id = fields.Many2one(
        "product.template", "Powder Product", compute="_compute_powder_product_id"
    )

    @api.depends("production_ids")
    def _compute_powder_product_id(self):
        """
        Get the powder product from the selected MOs and check there is only one
        """
        for wizard in self:
            powder = wizard.production_ids.mapped(
                "move_raw_ids.product_id.product_tmpl_id"
            ).filtered(lambda p: p.categ_id.is_powder)

            if len(powder) > 1:
                raise exceptions.ValidationError(
                    _(
                        "The selected Manufacturing Orders use different powders and "
                        "cannot be assigned to one job"
                    )
                )

            wizard.powder_product_id = powder

    def open(self):
        """
        Create a wizard line per manufacturing order and open the wizard
        """
        wizard_line_obj = self.env["mrp.job.assign_wizard.line"]
        for mo in self.production_ids:
            workorders = self.env["mrp.job.line"].get_available_workorders(mo)
            vals = {
                "wizard_id": self.id,
                "production_id": mo.id,
                "available_work_order_ids": workorders.ids,
                "qty_original": mo.product_qty,
            }
            if len(workorders) == 1:
                vals["work_order_id"] = workorders.id
                vals["qty_available"] = workorders.qty_available_to_print
                vals["qty_assign"] = workorders.qty_available_to_print
            wizard_line_obj.create(vals)

        return {
            "name": "Assign to Job",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "mrp.job.assign_wizard",
            "res_id": self.id,
            "target": "new",
        }

    def button_confirm(self):
        """
        Create job lines on assigned job
        """
        self.ensure_one()
        job_line_obj = self.env["mrp.job.line"]
        for wizard_line in self.wizard_line_ids:
            existing_line = self.job_id.job_line_ids.filtered(
                lambda line: line.production_id == wizard_line.production_id
            )
            if existing_line:
                existing_line.quantity += wizard_line.qty_assign
            else:
                job_line_obj.create(
                    {
                        "job_id": self.job_id.id,
                        "quantity": wizard_line.qty_assign,
                        "production_id": wizard_line.production_id.id,
                        "work_order_id": wizard_line.work_order_id.id,
                    }
                )

    def button_confirm_open_job(self):
        self.button_confirm()
        return {
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "mrp.job",
            "res_id": self.job_id.id,
            "target": "current",
        }


class JobAssignWizardLine(models.TransientModel):
    _name = "mrp.job.assign_wizard.line"
    _description = "MRP Job: Assign Wizard Line"

    wizard_id = fields.Many2one(
        "mrp.job.assign_wizard", "Wizard", required=True, ondelete="cascade"
    )
    production_id = fields.Many2one(
        "mrp.production", "Manufacturing Order", required=True, readonly=True
    )
    work_order_id = fields.Many2one("mrp.workorder", "Work Order")
    available_work_order_ids = fields.Many2many(
        "mrp.workorder", string="Available Work Orders", readonly=True
    )
    qty_assign = fields.Integer("Quantity to Assign")

    qty_original = fields.Integer("Original Quantity", readonly=True)
    qty_available = fields.Integer("Available Quantity", readonly=True)
    # Related
    product_id = fields.Many2one(related="work_order_id.product_id")
    file_name = fields.Char(related="production_id.file_name")

    @api.onchange("qty_assign")
    def _onchange_qty_assign(self):
        """
        Check that set qty is valid
        """
        self.ensure_one()
        if not 0 <= self.qty_assign <= self.qty_available:
            raise exceptions.UserError(
                _(
                    f"You can assign a maximum of {self.qty_available} from "
                    f"{self.production_id.name}"
                )
            )

    @api.onchange("work_order_id")
    def _onchange_work_order_id(self):
        self.ensure_one()
        self.qty_original = 0
        self.qty_available = 0
        self.qty_assign = 0
        if self.work_order_id:
            self.qty_original = self.work_order_id.qty_production
            self.qty_available = self.work_order_id.qty_available_to_print
            self.qty_assign = self.work_order_id.qty_available_to_print
