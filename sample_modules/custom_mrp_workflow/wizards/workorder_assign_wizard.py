# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import fields, models


class MrpWorkorderAssignWizard(models.TransientModel):
    _name = "mrp.workorder.assign.wizard"
    _description = "Workorder Assign Wizard"

    workorder_ids = fields.Many2many(
        "mrp.workorder", string="Workorders", required=True
    )
    assign_user_id = fields.Many2one("res.users", "Assign To", required=True)

    def button_confirm(self):
        self.ensure_one()
        self.workorder_ids.write({"assign_user_id": self.assign_user_id.id})
