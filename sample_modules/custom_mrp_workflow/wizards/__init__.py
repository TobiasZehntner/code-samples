# © 2020 Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary

from . import job_assign_wizard
from . import job_partial_fail_wizard
from . import job_user_assign_wizard
from . import workorder_assign_wizard
