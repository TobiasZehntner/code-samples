# © 2020 Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models


class JobPartialFailWizard(models.TransientModel):
    _name = "mrp.job.partial_fail_wizard"
    _description = "MRP Job: Partial Fail Wizard"

    job_id = fields.Many2one("mrp.job", "Job", required=True)
    wizard_line_ids = fields.One2many(
        "mrp.job.partial_fail_wizard.line",
        inverse_name="wizard_id",
        string="Wizard Lines",
    )

    def open(self):
        """
        Create a wizard line per job line and open the wizard
        """
        WizardLine = self.env["mrp.job.partial_fail_wizard.line"]
        for line in self.job_id.job_line_ids:
            WizardLine.create(
                {
                    "wizard_id": self.id,
                    "job_line_id": line.id,
                    "qty_original": int(line.quantity),
                    "qty_success": int(line.quantity),
                }
            )

        return {
            "name": "Partial Fail - Adjust quantities",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "mrp.job.partial_fail_wizard",
            "res_id": self.id,
            "target": "new",
        }

    def button_confirm(self):
        """
        If all quantities have failed: Set job to failed.
        If all quantities have succeeded: Set job to success.
        If partial: adapt produced quantity on the line and set job to success,
        then duplicate the job, set failed qty and set copied job to failed
        """
        self.ensure_one()
        if all([line.qty_fail == line.qty_original for line in self.wizard_line_ids]):
            self.job_id.button_fail()
        elif all(
            [line.qty_success == line.qty_original for line in self.wizard_line_ids]
        ):
            self.job_id.button_success()
        else:
            successful_job = self.job_id
            failed_job = successful_job.copy({"name": f"{successful_job.name}-f"})
            for wizard_line in self.wizard_line_ids:
                if wizard_line.qty_success != wizard_line.qty_original:
                    wizard_line.job_line_id.quantity = wizard_line.qty_success
                    failed_line = wizard_line.job_line_id.copy(
                        {"job_id": failed_job.id}
                    )
                    failed_line.quantity = wizard_line.qty_fail
            successful_job.button_success()
            failed_job.button_fail()
            successful_job.message_post(
                body=f'The job failed partially. See <a href="#" data-oe-model="'
                f'{failed_job._name}" data-oe-id="{failed_job.id}">'
                f"{failed_job.name}</a> for failed quantities."
            )
            failed_job.message_post(
                body=f'This job is the failed part of <a href="#" data-oe-model="'
                f'{successful_job._name}" data-oe-id="{successful_job.id}">'
                f"{successful_job.name}</a>"
            )


class JobPartialFailWizardLine(models.TransientModel):
    _name = "mrp.job.partial_fail_wizard.line"
    _description = "MRP Job: Partial Fail Wizard Line"

    wizard_id = fields.Many2one(
        "mrp.job.partial_fail_wizard", "Wizard", required=True, ondelete="cascade"
    )
    job_line_id = fields.Many2one("mrp.job.line", "Job Line", required=True)
    qty_original = fields.Integer("Original Quantity", readonly=True)
    qty_success = fields.Integer("Successful Quantity")
    qty_fail = fields.Integer("Failed Quantity")

    # Related
    production_id = fields.Many2one(related="job_line_id.production_id", readonly=True)
    file_name = fields.Char(related="job_line_id.file_name", readonly=True)
    product_id = fields.Many2one(related="job_line_id.product_id", readonly=True)

    @api.onchange("qty_success")
    def _onchange_qty_success(self):
        """
        Check that set qty is valid and adjust corresponding failed qty
        """
        self.ensure_one()
        if not 0 <= self.qty_success <= self.qty_original:
            raise exceptions.UserError(
                _(
                    f"The successful quantity is required to be between 0 and "
                    f"{self.qty_original}"
                )
            )
        elif not self.env.context.get("skip"):
            self.with_context(skip=True).qty_fail = self.qty_original - self.qty_success

    @api.onchange("qty_fail")
    def _onchange_qty_fail(self):
        """
        Check that set qty is valid and adjust corresponding success qty
        """
        self.ensure_one()
        if not 0 <= self.qty_success <= self.qty_original:
            raise exceptions.UserError(
                _(
                    f"The failed quantity is required to be between 0 and "
                    f"{self.qty_original}"
                )
            )
        elif not self.env.context.get("skip"):
            self.with_context(skip=True).qty_success = self.qty_original - self.qty_fail
