# © 2022 Tobias Zehntner
# © 2022 Niboo SRL (https://www.niboo.com/)
# License Other Proprietary

from odoo import _, api, exceptions, fields, models

from ..models.mrp_job import JOB_USER_FIELDS


class JobUserAssignWizard(models.TransientModel):
    _name = "mrp.job.user.assign.wizard"
    _description = "Job User Assign Wizard"

    job_ids = fields.Many2many("mrp.job", string="Jobs", required=True)
    is_require_depowder = fields.Boolean(compute="_compute_is_require_depowder")

    job_prep_user_id = fields.Many2one("res.users", "Job Preparation User")
    machine_prep_user_id = fields.Many2one("res.users", "Machine Preparation User")
    machine_unpack_user_id = fields.Many2one("res.users", "Machine Unpacking User")
    depowder_user_id = fields.Many2one("res.users", "Depowdering User")

    @api.depends("job_ids")
    def _compute_is_require_depowder(self):
        for wizard in self:
            wizard.is_require_depowder = any(
                job.is_require_depowder for job in wizard.job_ids
            )

    def button_confirm(self):
        self.ensure_one()
        vals = {}
        for user_field in JOB_USER_FIELDS:
            if self[user_field]:
                vals.update({user_field: self[user_field].id})

        if not vals:
            raise exceptions.UserError(_("Please assign at least one task to a user"))

        depowder_jobs = self.job_ids.filtered(lambda j: j.is_require_depowder)
        other_jobs = self.job_ids.filtered(lambda j: not j.is_require_depowder)
        if depowder_jobs:
            depowder_jobs.write(vals)
        if other_jobs:
            if self.is_require_depowder:
                del vals["depowder_user_id"]
            other_jobs.write(vals)
