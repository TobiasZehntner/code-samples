# © 2020 PINTON KEVIN, Pierre Gillissen, Tobias Zehntner
# © 2020 Niboo SPRL (https://www.niboo.com/)
# License Other Proprietary.

{
    "name": "ANY - MRP",
    "category": "Sales-Mrp",
    "summary": "Customized MRP Module for ANY",
    "website": "https://www.niboo.com",
    "version": "13.0.1.0.2",
    "license": "Other proprietary",
    "description": """
    - Add Job model
        - Serial Number (Job <year> <5 digits>)
        - Category equipment Id
        - Equipment id (of this category)
        - Powder category (product template used)
        - stock.production.lot Id (of this category)
        - Job Line Ids
    - Add Job Line model
        - Job ID
        - Quant id
        - Workorder ID
        - Quantity
    - Allow the possibility to add/display a file name on:
        - Account Invoice
        - Sale Order Line
        - Manufacturing Order
        - Workorder
        - Stock Move
        - Stock Move Line
        - Delivery Slip
        - Sale Order report
    - Link SO to MO by using procurement.rule
    - Add boolean on product.category for powder batch
    - Add report for Manufacturing Order
    """,
    "author": "Niboo",
    "depends": ["account", "product", "sale_stock", "mrp_workorder", "maintenance"],
    "data": [
        "data/ir_sequence_job.xml",
        "report/report_account_invoice.xml",
        "report/report_mrp_production.xml",
        "report/report_sale_order.xml",
        "report/report_stock_delivery_slip.xml",
        "views/account_invoice.xml",
        "views/maintenance_equipment_category.xml",
        "views/mrp_job.xml",
        "views/mrp_production.xml",
        "views/mrp_workcenter.xml",
        "views/mrp_workorder.xml",
        "views/product_category.xml",
        "views/sale_order.xml",
        "views/stock_picking.xml",
        "wizards/job_assign_wizard.xml",
        "wizards/job_partial_fail_wizard.xml",
        "wizards/job_user_assign_wizard.xml",
        "wizards/workorder_assign_wizard.xml",
        "security/ir.model.access.csv",
    ],
    "installable": True,
    "application": False,
}
